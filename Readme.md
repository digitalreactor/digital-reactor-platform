
 _____  _       _ _        _ _____                 _             _____                 _____  _       _    __
|  __ \(_)     (_) |      | |  __ \               | |           |_   _|               |  __ \| |     | |  / _|
| |  | |_  __ _ _| |_ __ _| | |__) |___  __ _  ___| |_ ___  _ __  | |  ___    ______  | |__) | | __ _| |_| |_ ___  _ __ _ __ ___
| |  | | |/ _` | | __/ _` | |  _  // _ \/ _` |/ __| __/ _ \| '__| | | / _ \  |______| |  ___/| |/ _` | __|  _/ _ \| '__| '_ ` _ \
| |__| | | (_| | | || (_| | | | \ \  __/ (_| | (__| || (_) | | _ _| || (_) |          | |    | | (_| | |_| || (_) | |  | | | | | |
|_____/|_|\__, |_|\__\__,_|_|_|  \_\___|\__,_|\___|\__\___/|_|(_)_____\___/           |_|    |_|\__,_|\__|_| \___/|_|  |_| |_| |_|
           __/ |
          |___/

## Terms

OpenMonth - It is interval of time between first and last day of month.
MetricsForThreeMonth - It is interval which consist of two month and openMonth;



https://api-metrika.yandex.ru/stat/v1/data.csv?dimensions=ym:s:directSearchPhrase,ym:s:directClickOrder,ym:s:directBannerGroup,ym:s:directClickBanner,ym:s:directPhraseOrCond,ym:s:UTMSource,ym:s:UTMMedium,ym:s:UTMCampaign,ym:s:UTMContent&metrics=ym:s:visits,ym:s:pageviews,ym:s:bounceRate,ym:s:avgVisitDurationSeconds,ym:s:sumGoalReachesAny&filters=ym:s:directPlatformType=='search'&limit=10000&offset=1&ids=34322390&oauth_token=AQAEA7qgoVoVAAK_6aPQ1lZ_JEQ8tbtwjJcGY7k&include_undefined=true

https://api-metrika.yandex.ru/stat/v1/data/bytime?group=day&dimensions=ym:s:directSearchPhrase,ym:s:directClickOrder,ym:s:directBannerGroup,ym:s:directClickBanner,ym:s:directPhraseOrCond,ym:s:UTMSource,ym:s:UTMMedium,ym:s:UTMCampaign,ym:s:UTMContent&metrics=ym:s:visits,ym:s:pageviews,ym:s:bounceRate,ym:s:avgVisitDurationSeconds,ym:s:sumGoalReachesAny&filters=ym:s:directPlatformType=='search'&limit=10000&offset=1&ids=34322390&oauth_token=AQAEA7qgoVoVAAK_6aPQ1lZ_JEQ8tbtwjJcGY7k&include_undefined=true


  https://api-metrika.yandex.ru/stat/v1/data?date1=2016-08-01
&date2=2016-09-20
&limit=10000&offset=1
&ids=34322390
&oauth_token=AQAEA7qgoVoVAAK_6aPQ1lZ_JEQ8tbtwjJcGY7k
&filters=ym:s:LastDirectClickOrder!n
&metrics=ym:s:visits,ym:s:bounceRate,ym:s:pageDepth,ym:s:avgVisitDurationSeconds
&dimensions=ym:s:lastDirectClickOrder,ym:s:lastDirectClickBanner,ym:s:lastDirectPhraseOrCond,ym:s:lastDirectSearchPhrase
&include_undefined=true

## Database
### Common info
Tool (http://ondras.zarovi.cz/sql/demo/) for online loading scheme.xml form ***resources.documentation.db***
### Install
Required version: 9.6
1. https://www.postgresql.org/download/linux/ubuntu/
2. https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04
3. sudo -i -u postgres
4. createuser --interactive
5. psql -d template1 -U postgres
6. ALTER USER "platform" WITH PASSWORD 'platform';
7. GRANT ALL privileges ON DATABASE platform TO platform;

### Encoding
Current the project encoding is UTF-8.
platform      | postgres | UTF8      | ru_RU.UTF-8 | ru_RU.UTF-8 | =Tc/postgres         +
1. https://www.sinyawskiy.ru/invalid_locale.html
2. How to check your encoding: https://evileg.com/en/post/2/

## UI
Inject ui for dev: ln -sf ~/gitProject/digital-reactor-platform-ui/dist/dev ~/gitProject/digital-reactor-platform/build/resources/main/static