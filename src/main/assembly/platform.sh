#!/bin/bash
#
### BEGIN INIT INFO
# Provides:          platform
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: platform
# Description:       DigitalReactor - platform
### END INIT INFO
#
[[ -n "$DEBUG" ]] && set -x
export LANG="ru_RU.UTF-8" LC_ALL="ru_RU.UTF-8"

#
# config
#
APP_NAME=platform
RUN_USER=platform
ROOTDIR=/opt/digitalreactor/platform
#JAVA_HOME=

JMX_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.host=127.0.0.1 -Dcom.sun.management.jmxremote.port=5000 -Dcom.sun.management.jmxremote.authenticate=false -Dfile.encoding=utf-8 -Dcom.sun.management.jmxremote.ssl=false"
JAVA_OPTS="-Xmx4096M -Xms1000M -Xss256k -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Djava.net.preferIPv4Stack=true -XX:+HeapDumpOnOutOfMemoryError $JMX_OPTS"
RUN_ARGS="--spring.profiles.active=prod"

JARFILE=$ROOTDIR/platform.jar
STOP_WAIT_TIME=60
PID_FILE=$ROOTDIR/$APP_NAME.pid
LOGDIR=$ROOTDIR/logs

CURRENT_LOG=$LOGDIR/$APP_NAME.log
LOG_PATTERN=$LOGDIR/$APP_NAME.%Y-%m-%d.log

#
# script
#
cd $ROOTDIR
mkdir -p $LOGDIR

# ANSI Colors
echoRed() { echo $'\e[0;31m'"$1"$'\e[0m'; }
echoGreen() { echo $'\e[0;32m'"$1"$'\e[0m'; }
echoYellow() { echo $'\e[0;33m'"$1"$'\e[0m'; }


checkPermissions() {
  touch "$PID_FILE" &> /dev/null || { echoRed "Operation not permitted (cannot access pid file)"; return 4; }
}

isRunning() {
  ps -p "$1" &> /dev/null
}


# Determine the script mode
action="$1"
shift

# Find Java
if [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
    javaexe="$JAVA_HOME/bin/java"
elif type -p java > /dev/null 2>&1; then
    javaexe=$(type -p java)
elif [[ -x "/usr/bin/java" ]];  then
    javaexe="/usr/bin/java"
else
    echo "Unable to find Java"
    exit 1
fi

arguments=($JAVA_OPTS -jar "$JARFILE" $RUN_ARGS "$@")

# Action functions
start() {
  if [[ -f "$PID_FILE" ]]; then
    pid=$(cat "$PID_FILE")
    isRunning "$pid" && { echoYellow "Already running [$pid]"; return 0; }
  fi
  do_start "$@"
}

do_start() {
  working_dir=$(dirname "$JARFILE")
  pushd "$working_dir" > /dev/null
  if [[ $(id -u) == "0" ]]; then
    checkPermissions || return $?
    chown "$RUN_USER" "$PID_FILE"

    su -s /bin/sh -c "($javaexe $(printf "\"%s\" " "${arguments[@]}") 2>&1 & echo \$! > $PID_FILE) | cronolog --link \"$CURRENT_LOG\" \"$LOG_PATTERN\" &" "$RUN_USER"
    pid=$(cat "$PID_FILE")
  else
    checkPermissions || return $?
    ("$javaexe" "${arguments[@]}" 2>&1 & echo "$!" > "$PID_FILE") | cronolog --link "$CURRENT_LOG" "$LOG_PATTERN" 2>&1 &
    pid=`cat $PID_FILE`
    disown -a
  fi
  [[ -z $pid ]] && { echoRed "Failed to start"; return 1; }
  echoGreen "Started [$pid]"
}

stop() {
  working_dir=$(dirname "$JARFILE")
  pushd "$working_dir" > /dev/null
  [[ -f $PID_FILE ]] || { echoYellow "Not running (pidfile not found)"; return 0; }
  pid=$(cat "$PID_FILE")
  isRunning "$pid" || { echoYellow "Not running (process ${pid}). Removing stale pid file."; rm -f "$PID_FILE"; return 0; }
  do_stop "$pid" "$PID_FILE"
}

do_stop() {
  kill "$1" &> /dev/null || { echoRed "Unable to kill process $1"; return 1; }
  for i in $(seq 1 $STOP_WAIT_TIME); do
    isRunning "$1" || { echoGreen "Stopped [$1]"; rm -f "$2"; return 0; }
    [[ $i -eq STOP_WAIT_TIME/2 ]] && kill "$1" &> /dev/null
    sleep 1
  done
  echoRed "Unable to kill process $1";
  return 1;
}

force_stop() {
  [[ -f $PID_FILE ]] || { echoYellow "Not running (pidfile not found)"; return 0; }
  pid=$(cat "$PID_FILE")
  isRunning "$pid" || { echoYellow "Not running (process ${pid}). Removing stale pid file."; rm -f "$PID_FILE"; return 0; }
  do_force_stop "$pid" "$PID_FILE"
}

do_force_stop() {
  kill -9 "$1" &> /dev/null || { echoRed "Unable to kill process $1"; return 1; }
  for i in $(seq 1 $STOP_WAIT_TIME); do
    isRunning "$1" || { echoGreen "Stopped [$1]"; rm -f "$2"; return 0; }
    [[ $i -eq STOP_WAIT_TIME/2 ]] && kill -9 "$1" &> /dev/null
    sleep 1
  done
  echoRed "Unable to kill process $1";
  return 1;
}

restart() {
  stop && start
}

force_reload() {
  working_dir=$(dirname "$JARFILE")
  pushd "$working_dir" > /dev/null
  [[ -f $PID_FILE ]] || { echoRed "Not running (pidfile not found)"; return 7; }
  pid=$(cat "$PID_FILE")
  rm -f "$PID_FILE"
  isRunning "$pid" || { echoRed "Not running (process ${pid} not found)"; return 7; }
  do_stop "$pid" "$PID_FILE"
  do_start
}

status() {
  working_dir=$(dirname "$JARFILE")
  pushd "$working_dir" > /dev/null
  [[ -f "$PID_FILE" ]] || { echoRed "Not running"; return 3; }
  pid=$(cat "$PID_FILE")
  isRunning "$pid" || { echoRed "Not running (process ${pid} not found)"; return 1; }
  echoGreen "Running [$pid]"
  return 0
}

run() {
  pushd "$(dirname "$JARFILE")" > /dev/null
  "$javaexe" "${arguments[@]}"
  result=$?
  popd > /dev/null
  return "$result"
}

# Call the appropriate action function
case "$action" in
start)
  start "$@"; exit $?;;
stop)
  stop "$@"; exit $?;;
force-stop)
  force_stop "$@"; exit $?;;
restart)
  restart "$@"; exit $?;;
force-reload)
  force_reload "$@"; exit $?;;
status)
  status "$@"; exit $?;;
run)
  run "$@"; exit $?;;
*)
  echo "Usage: $0 {start|stop|force-stop|restart|force-reload|status|run}"; exit 1;
esac

exit 0