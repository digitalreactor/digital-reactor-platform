CREATE TABLE users (
  id       SERIAL PRIMARY KEY,
  email    VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  reg_date TIMESTAMP   NOT NULL,
  UNIQUE (email)
);

CREATE TABLE projects (
  id      SERIAL PRIMARY KEY,
  user_id INT         NOT NULL REFERENCES users (id) ON DELETE CASCADE NOT NULL,
  name    VARCHAR(25) NOT NULL
);

CREATE TYPE metrika_token_status AS ENUM ('ACTIVE', 'EXPIRED');

CREATE TABLE metrika_tokens (
  id         SERIAL PRIMARY KEY,
  token      VARCHAR(255)         NOT NULL,
  date_added TIMESTAMP            NOT NULL,
  user_id    INT                  NOT NULL REFERENCES users (id) ON DELETE CASCADE NOT NULL,
  status     metrika_token_status NOT NULL
);

CREATE TABLE metrika_counters (
  id          SERIAL PRIMARY KEY,
  project_id  INT NOT NULL REFERENCES projects (id) ON DELETE CASCADE NOT NULL,
  -- код счётчика в метрике
  external_id INT NOT NULL,
  token_id    INT REFERENCES metrika_tokens (id)
);

CREATE TYPE task_status AS ENUM ('NEW', 'RUNNING', 'COMPLETED', 'ERROR', 'INTERRUPTED', 'WAITING');
CREATE TYPE task_scope AS ENUM ('ALL', 'PARTIAL');

CREATE TABLE project_tasks (
  id                 SERIAL PRIMARY KEY,
  project_id         INT         NOT NULL REFERENCES projects (id) ON DELETE CASCADE NOT NULL,
  task_type          VARCHAR(25) NOT NULL,
  payload            TEXT        NOT NULL,
  -- статус выполнения задачи
  status             task_status NOT NULL,
  -- дата перехода в состояние
  date_of_transition TIMESTAMP   NOT NULL
);

/**
Summary - составная еденица модуля отчётов.
*/

CREATE TYPE summary_status AS ENUM ('NEW', 'LOADING', 'COMPLETED', 'BROKEN');

CREATE TABLE summaries (
  id         SERIAL PRIMARY KEY,
  project_id INT            NOT NULL REFERENCES projects (id) ON DELETE CASCADE NOT NULL,
  status     summary_status NOT NULL,
  load_date  TIMESTAMP      NOT NULL
);

CREATE TABLE reports (
  id         SERIAL PRIMARY KEY,
  summary_id INT         NOT NULL REFERENCES summaries (id) ON DELETE CASCADE NOT NULL,
  -- Тип отчёта
  type       VARCHAR(65) NOT NULL,
  payload    TEXT        NOT NULL
);

CREATE TABLE feedbacks (
  id         SERIAL PRIMARY KEY,
  user_name VARCHAR(255) NOT NULL,
  message TEXT NOT NULL
);