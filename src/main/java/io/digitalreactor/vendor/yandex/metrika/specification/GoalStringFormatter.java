package io.digitalreactor.vendor.yandex.metrika.specification;

import io.digitalreactor.vendor.yandex.metrika.model.Goal;

/**
 * digital-reactor
 * Created by igor on 08.05.17.
 */
public class GoalStringFormatter {
    public static final String GOAL_PARAMETER_TEMPLATE = ",ym:s:goal%susers";

    public static String makeGoal(final String goalId) {
        if (goalId.length() > 0) {
            return String.format(GOAL_PARAMETER_TEMPLATE, goalId);
        } else {
            return "";
        }
    }

    public static String getStringMetrikaGoal(final Goal goal) {
        return goal.getId() == 0 ? "" : String.valueOf(goal.getId());
    }
}
