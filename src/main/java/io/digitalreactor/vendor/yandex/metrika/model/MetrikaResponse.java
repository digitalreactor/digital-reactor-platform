package io.digitalreactor.vendor.yandex.metrika.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * digital-reactor
 * Created by igor on 05.04.17.
 */

@Data
@AllArgsConstructor
public class MetrikaResponse {
    private List<Map<String, String>> dimensions = new ArrayList<>();
    private List<List<Double>> metrics = new ArrayList<>();
}
