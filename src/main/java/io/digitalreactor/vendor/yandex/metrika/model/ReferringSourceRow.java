package io.digitalreactor.vendor.yandex.metrika.model;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.stream.Collectors;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */

@Data
@Builder
public class ReferringSourceRow {
    private String name;
    private List<List<Integer>> metrics;
    private List<Pair<Integer,Integer>> sums;
    private Pair<Integer,Integer> fullSum;

    public void evaluateSums() {
        sums = metrics
                        .stream()
                        .map(list -> Pair.of(
                                list.subList(0,list.size()/2).stream()
                                        .mapToInt(Integer::intValue).sum(),
                                list.subList(list.size()/2,list.size()).stream()
                                        .mapToInt(Integer::intValue).sum()
                                )
                        )
                        .collect(Collectors.toList());
    }
}
