package io.digitalreactor.vendor.yandex.metrika.domain;

import io.digitalreactor.vendor.yandex.metrika.DataTransformer;

/**
 * digital-reactor
 * Created by ingvard on 06.01.17.
 */
public interface CustomRequest<I, R> extends DataTransformer<I, R>, Request {
}
