package io.digitalreactor.vendor.yandex.metrika.model;

import lombok.*;

/**
 * digital-reactor
 * Created by igor on 27.03.17.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PageRow {
    private String siteName;
    private String favicon;

    private int visits;
    private double bounceRate;
    private int pageViews;
    private double avgVisitDurationSeconds;

    String goal;
    Double conversion;

    public int compareTo(final PageRow pageRow) {
        return Integer.compare(pageRow.visits,visits);
    }
}
