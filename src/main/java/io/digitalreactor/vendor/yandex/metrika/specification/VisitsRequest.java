package io.digitalreactor.vendor.yandex.metrika.specification;


import com.jayway.jsonpath.JsonPath;
import io.digitalreactor.vendor.yandex.metrika.domain.CustomRequest;
import io.digitalreactor.vendor.yandex.metrika.model.VisitWithGoalRow;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.digitalreactor.analytic.metrika.common.report.object.Visit.evalDay;
import static io.digitalreactor.vendor.yandex.metrika.specification.GoalStringFormatter.makeGoal;

/**
 * digital-reactor
 * Created by MStepachev on 22.09.2016.
 */
public class VisitsRequest implements CustomRequest<String, List<VisitWithGoalRow>> {
    private final static String QUERY_TEMPLATE = "stat/v1/data/bytime?" +
            "group=day&" +
            "ids=%s&" +
            "metrics=ym:s:users%s&" +
            "date1=%s&" +
            "date2=%s&" +
            "oauth_token=%s";

    private final String oauthToken;
    private final long counterId;
    private final String endIntervalDate;
    private final String startIntervalDate;
    private final String goalId;

    public VisitsRequest(
            final String oauthToken,
            final long counterId,
            final LocalDate startIntervalDate,
            final LocalDate endIntervalDate,
            final String goalId) {
        this.oauthToken = oauthToken;
        this.counterId = counterId;
        this.endIntervalDate = endIntervalDate.toString();
        this.startIntervalDate = startIntervalDate.toString();
        this.goalId = goalId;
    }

    @Override
    public String toQuery() {
        return String.format(
                QUERY_TEMPLATE,
                counterId,
                makeGoal(goalId),
                startIntervalDate,
                endIntervalDate,
                oauthToken
        );
    }

    @Override
    public List<VisitWithGoalRow> transform(final String json) {
        final net.minidev.json.JSONArray metrics = JsonPath.read(json, "$.data[0].metrics[0][*]");
        final net.minidev.json.JSONArray goalMetrics = JsonPath.read(json, "$.data[0].metrics[1][*]");
        final LocalDate startDate = LocalDate.parse(JsonPath.read(json, "$.query.date1"));

        if (goalMetrics.size() > 0) {
            return IntStream.range(0, metrics.size())
                    .mapToObj(i ->
                            VisitWithGoalRow.builder()
                                    .date(startDate.plusDays(i))
                                    .dayType(evalDay(startDate.plusDays(i)))
                                    .number(((Double) metrics.get(i)).intValue())
                                    .goal(goalId)
                                    .conversion((Double) goalMetrics.get(i))
                                    .build()
                    )
                    .collect(Collectors.toList());
        } else {
            return IntStream.range(0, metrics.size())
                    .mapToObj(i ->
                            VisitWithGoalRow.builder()
                                    .date(startDate.plusDays(i))
                                    .dayType(evalDay(startDate.plusDays(i)))
                                    .number(((Double) metrics.get(i)).intValue())
                                    .goal(goalId)
                                    .conversion(0.0)
                                    .build()
                    )
                    .collect(Collectors.toList());
        }
    }
}
