package io.digitalreactor.vendor.yandex.metrika.serivce;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Service
public class ClassicHttpClient implements JsonClientWrapper {
    private final CloseableHttpClient httpClient;
    private final ObjectMapper mapper = new ObjectMapper();

    public ClassicHttpClient(final CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public String getJsonResponse(@NonNull final String uri) {
        try {
            return httpClient.execute(request(uri), response -> {
                final int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    return IOUtils.toString(response.getEntity().getContent(), "UTF-8");
                } else {
                    throw new RuntimeException("Gotten: " + statusCode + IOUtils.toString(response.getEntity().getContent(), "UTF-8"));
                }
            });

        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpGet request(final String query) throws UnsupportedEncodingException {
        return new HttpGet("https://api-metrika.yandex.ru/" + query);
    }
}
