package io.digitalreactor.vendor.yandex.metrika.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.digitalreactor.analytic.metrika.common.report.enumeration.DayType;
import lombok.*;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * digital-reactor
 * Created by igor on 08.05.17.
 */

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class VisitWithGoalRow {
    private int number;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate date;
    private DayType dayType;

    private String goal;
    private Double conversion;

    public static DayType evalDay(@NonNull final LocalDate localDate) {
        return localDate.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                || localDate.getDayOfWeek().equals(DayOfWeek.SUNDAY) ?
                DayType.HOLIDAY : DayType.WEEKDAY;
    }
}

