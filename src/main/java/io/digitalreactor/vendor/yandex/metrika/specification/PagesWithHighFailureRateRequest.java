package io.digitalreactor.vendor.yandex.metrika.specification;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.vendor.yandex.metrika.domain.CustomRequest;
import io.digitalreactor.vendor.yandex.metrika.model.PageRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.digitalreactor.vendor.yandex.metrika.specification.GoalStringFormatter.makeGoal;

/**
 * digital-reactor
 * Created by igor on 27.03.17.
 */

public class PagesWithHighFailureRateRequest implements CustomRequest<String, List<PageRow>> {
    private static final Logger logger = LoggerFactory.getLogger(PagesWithHighFailureRateRequest.class);

    private static final Integer METRICS_START_INDEX = 4;

    private static final String QUERY_TEMPLATE = "/stat/v1/data?" +
            "metrics=ym:s:visits,ym:s:bounceRate,ym:s:pageviews,ym:s:avgVisitDurationSeconds%s&" + //<-- goal
            "dimensions=ym:s:startURL&" +
            "ids=%s&" +
            "date1=%s&" +
            "date2=%s&" +
            "limit=10000&" +
            "offset=1&" +
            "oauth_token=%s";

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final String oauthToken;
    private final long counterId;
    private final String endIntervalDate;
    private final String startIntervalDate;
    private final String goalId;

    public PagesWithHighFailureRateRequest(final String oauthToken, final long counterId,
                                           final LocalDate startIntervalDate, final LocalDate endIntervalDate,
                                           final String goalId
    ) {
        this.oauthToken = oauthToken;
        this.counterId = counterId;
        this.endIntervalDate = endIntervalDate.toString();
        this.startIntervalDate = startIntervalDate.toString();
        this.goalId = goalId;
    }

    @Override
    public List<PageRow> transform(final String json) {
        final JsonNode jsonNode;
        try {
            jsonNode = (objectMapper.readValue(json, JsonNode.class)).get("data");

            return StreamSupport
                    .stream(
                            Spliterators.spliteratorUnknownSize(
                                    jsonNode.iterator(),
                                    Spliterator.SIZED
                            ),
                            false
                    )
                    .map(
                            node -> {
                                final JsonNode metrics = node.get("metrics");
                                final JsonNode dimensions = node.get("dimensions").get(0);
                                PageRow.PageRowBuilder pageBuilder = PageRow.builder()
                                        .siteName(dimensions.get("name").asText())
                                        .favicon(dimensions.get("favicon").asText())
                                        .visits(metrics.get(0).asInt())
                                        .bounceRate(metrics.get(1).asDouble())
                                        .pageViews(metrics.get(2).asInt())
                                        .avgVisitDurationSeconds(metrics.get(3).asDouble());

                                Double conversion = 0.0;
                                if (metrics.size() > METRICS_START_INDEX) {
                                    conversion = metrics.get(METRICS_START_INDEX).asDouble();
                                }

                                return pageBuilder
                                        .goal(goalId)
                                        .conversion(conversion)
                                        .build();
                            }
                    )
                    .collect(Collectors.toList());
        } catch (final IOException e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public String toQuery() {
        return String.format(
                QUERY_TEMPLATE,
                makeGoal(goalId),
                counterId,
                startIntervalDate,
                endIntervalDate,
                oauthToken);
    }
}
