package io.digitalreactor.vendor.yandex.metrika.serivce;

import io.digitalreactor.vendor.yandex.metrika.model.*;
import io.digitalreactor.vendor.yandex.metrika.model.PageRow;
import io.digitalreactor.vendor.yandex.metrika.domain.CustomRequest;
import io.digitalreactor.vendor.yandex.metrika.specification.PagesWithHighFailureRateRequest;
import io.digitalreactor.vendor.yandex.metrika.specification.ReferringSourceWithGoalsRequest;
import io.digitalreactor.vendor.yandex.metrika.specification.SearchPhraseRequest;
import io.digitalreactor.vendor.yandex.metrika.specification.VisitsRequest;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;

import static io.digitalreactor.vendor.yandex.metrika.specification.GoalStringFormatter.getStringMetrikaGoal;

public class ReportApiService {
    private static final Logger logger = LoggerFactory.getLogger(ReportApiService.class);

    private final JsonClientWrapper jsonClientWrapper;

    public ReportApiService(final JsonClientWrapper jsonClientWrapper) {
        this.jsonClientWrapper = jsonClientWrapper;
    }

    public <R> R findAllBy(@NonNull final CustomRequest<String, R> request) {
        final String response = jsonClientWrapper.getJsonResponse(request.toQuery());

        return request.transform(response);
    }

    public List<VisitWithGoalRow> getVisits(
            final String token,
            final long externalId,
            final LocalDate startDate,
            final LocalDate endDate,
            final Goal goal
    ) {
        return findAllBy(
                new VisitsRequest(
                        token,
                        externalId,
                        startDate,
                        endDate,
                        getStringMetrikaGoal(goal)
                )
        );
    }

    public List<PageRow> getPages(
            final String token,
            final long externalId,
            final LocalDate startDate,
            final LocalDate endDate,
            final Goal goal
    ) {
        return findAllBy(
                new PagesWithHighFailureRateRequest(
                        token,
                        externalId,
                        startDate,
                        endDate,
                        getStringMetrikaGoal(goal)
                )
        );
    }

    public List<PhraseRow> getPhrases(
            final String token,
            final long externalId,
            final LocalDate startDate,
            final LocalDate endDate,
            final Goal goal
    ) {
        return findAllBy(
                new SearchPhraseRequest(
                        token,
                        externalId,
                        startDate,
                        endDate,
                        getStringMetrikaGoal(goal)
                )
        );
    }

    public List<ReferringSourceRow> getSources(
            final String token,
            final long externalId,
            final LocalDate startDate,
            final LocalDate endDate,
            final Goal goal
    ) {
        return findAllBy(
                new ReferringSourceWithGoalsRequest(
                        token,
                        externalId,
                        startDate,
                        endDate,
                        getStringMetrikaGoal(goal)
                )
        );
    }
}
