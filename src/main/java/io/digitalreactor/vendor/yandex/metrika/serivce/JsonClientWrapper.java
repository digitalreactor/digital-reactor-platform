package io.digitalreactor.vendor.yandex.metrika.serivce;

public interface JsonClientWrapper {
    String getJsonResponse(String uri);
}
