package io.digitalreactor.vendor.yandex.metrika.model;

import lombok.*;

import java.util.Map;


/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(exclude = "goalAndConversion")
@AllArgsConstructor
public class PhraseRow {
    private String searchPhrase;
    private int visits;
    private double bounceRate;
    private double pageDepth;
    private double avgVisitDurationSeconds;
    private Map<String, Integer> goalAndConversion;
}

