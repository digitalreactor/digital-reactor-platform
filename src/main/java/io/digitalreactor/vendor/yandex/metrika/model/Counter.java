package io.digitalreactor.vendor.yandex.metrika.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * digital-reactor
 * Created by MStepachev on 09.09.2016.
 */
@Data
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Counter {
    private long id;
    private String name;
}
