package io.digitalreactor.vendor.yandex.metrika.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */

@Data
@Builder
@AllArgsConstructor
public class GoalResponse {
    private List<Goal> goals;

    public List<String> getGoalsIds() {
        return goals.stream()
                .map(goal -> String.valueOf(goal.getId()))
                .collect(Collectors.toList());
    }
}
