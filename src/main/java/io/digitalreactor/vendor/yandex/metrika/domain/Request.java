package io.digitalreactor.vendor.yandex.metrika.domain;

/**
 * Created by MStepachev on 22.09.2016.
 */
public interface Request {
    String toQuery();
}
