package io.digitalreactor.vendor.yandex.metrika.specification;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.vendor.yandex.metrika.domain.CustomRequest;
import io.digitalreactor.vendor.yandex.metrika.model.PhraseRow;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.digitalreactor.vendor.yandex.metrika.specification.GoalStringFormatter.makeGoal;

/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */
public class SearchPhraseRequest implements CustomRequest<String, List<PhraseRow>> {
    private static final String QUERY_TEMPLATE = "/stat/v1/data" +
            "?ids=%s" +
            "&oauth_token=%s" +
            "&date1=%s" +
            "&date2=%s" +
            "&limit=10000&offset=1" +
            "&filters=ym:s:LastDirectClickOrder!n" +
            "&metrics=ym:s:visits,ym:s:bounceRate,ym:s:pageDepth,ym:s:avgVisitDurationSeconds%s" + // <-- goals
            "&dimensions=ym:s:lastDirectClickOrder,ym:s:lastDirectClickBanner,ym:s:lastDirectPhraseOrCond,ym:s:lastDirectSearchPhrase" +
            "&include_undefined=true";

    private static final Integer METRICS_START_INDEX = 4;

    private static final List<String> goals = new ArrayList<>();
    private static final Function<JsonNode, PhraseRow> SEARCH_PHRASE_MAPPER = jsonNode -> {
        final String searchPhrase = jsonNode.get("dimensions").get(3).get("name").asText();
        final int visits = jsonNode.get("metrics").get(0).asInt();
        final double bounceRate = jsonNode.get("metrics").get(1).asDouble();
        final double pageDepth = jsonNode.get("metrics").get(2).asDouble();
        final double avgVisitDurationSeconds = jsonNode.get("metrics").get(3).asDouble();

        final Map<String, Integer> goalsAndConversion = new HashMap<>();
        if (jsonNode.get("metrics").size() > METRICS_START_INDEX) {
            for (int i = METRICS_START_INDEX; i < jsonNode.get("metrics").size(); i++) {
                final String goal = goals.get(i - METRICS_START_INDEX);
                final int goalUsers = jsonNode.get("metrics").get(i).asInt();

                goalsAndConversion.put(goal, goalUsers);
            }
        }

        return PhraseRow.builder()
                .searchPhrase(searchPhrase)
                .visits(visits)
                .bounceRate(bounceRate)
                .pageDepth(pageDepth)
                .avgVisitDurationSeconds(avgVisitDurationSeconds)
                .goalAndConversion(goalsAndConversion)
                .build();
    };
    private final String oauthToken;
    private final long counterId;
    private final String endIntervalDate;
    private final String startIntervalDate;
    private final String goalId;

    public SearchPhraseRequest(final String oauthToken,
                               final long counterId,
                               final LocalDate startIntervalDate,
                               final LocalDate endIntervalDate,
                               final String goalId) {
        this.oauthToken = oauthToken;
        this.counterId = counterId;
        this.endIntervalDate = endIntervalDate.toString();
        this.startIntervalDate = startIntervalDate.toString();
        this.goalId = goalId;
    }


    @Override
    public String toQuery() {
        return String.format(
                QUERY_TEMPLATE,
                counterId,
                oauthToken,
                startIntervalDate,
                endIntervalDate,
                makeGoal(goalId)
        );
    }

    @Override
    public List<PhraseRow> transform(final String input) {
        goals.clear();
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final JsonNode root = mapper.readTree(input);
            final JsonNode results = root.get("data");

            if (root.get("query").get("metrics").size() > METRICS_START_INDEX) {
                final JsonNode queryNode = root.get("query").get("metrics");
                for (int i = METRICS_START_INDEX; i < queryNode.size(); i++) {
                    goals.add(queryNode.get(i).asText());
                }
            }

            return StreamSupport
                    .stream(
                            Spliterators.spliteratorUnknownSize(
                                    results.iterator(),
                                    Spliterator.SIZED
                            ),
                            false
                    )
                    .map(SEARCH_PHRASE_MAPPER)
                    .filter(phraseRow -> !phraseRow.getSearchPhrase().equals("null"))
                    .collect(Collectors.toList());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
