package io.digitalreactor.vendor.yandex.metrika.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public class Goal {
    private int id;
    private String name;
    private String type;
    private boolean isRetargeting;

    public static Goal getSystemGoal() {
        return Goal.builder().name("System goal").id(0).build();
    }
}
