package io.digitalreactor.vendor.yandex.metrika.specification;

/**
 * digital-reactor
 * Created by igor on 04.04.17.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.vendor.yandex.metrika.domain.CustomRequest;
import io.digitalreactor.vendor.yandex.metrika.model.MetrikaResponse;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.digitalreactor.vendor.yandex.metrika.specification.GoalStringFormatter.makeGoal;

/**
 * digital-reactor
 * Created by igor on 04.04.17.
 */

public class ReferringSourceWithGoalsRequest implements CustomRequest<String, List<ReferringSourceRow>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferringSourceWithGoalsRequest.class);

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String QUERY_TEMPLATE = "stat/v1/data/bytime?" +
            "group=day" +
            "&id=%s" +
            "&metrics=ym:s:users%s" +
            "&date1=%s" +
            "&date2=%s" +
            "&dimensions=ym:s:lastTrafficSource" +
            "&oauth_token=%s";

    private final String oauthToken;
    private final long counterId;
    private final String endIntervalDate;
    private final String startIntervalDate;
    private final String goalId;

    public ReferringSourceWithGoalsRequest(
            final String oauthToken,
            final long counterId,
            final LocalDate startDate,
            final LocalDate endDate,
            final String goalId) {
        this.oauthToken = oauthToken;
        this.counterId = counterId;
        this.endIntervalDate = endDate.toString();
        this.startIntervalDate = startDate.toString();
        this.goalId = goalId;
    }

    @Override
    public List<ReferringSourceRow> transform(final String input) {
        try {
            final JsonNode jsonNode = objectMapper.readValue(input, JsonNode.class).get("data");

            final List<MetrikaResponse> metrikaResponses = StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(
                            jsonNode.iterator(),
                            Spliterator.SIZED
                    ),
                    false
            )
                    .map(node -> {
                        try {
                            return objectMapper.readValue(node.toString(), MetrikaResponse.class);
                        } catch (final IOException e) {
                            LOGGER.error("Can't read MetrikaResponse Exception message {}", e.getMessage());
                            return new MetrikaResponse(new ArrayList<>(), new ArrayList<>());
                        }
                    })
                    .collect(Collectors.toList());


            return metrikaResponses.stream()
                    .map(metrikaResponse ->
                            ReferringSourceRow
                                    .builder()
                                    .name(metrikaResponse.getDimensions().get(0).get("name"))
                                    .metrics(
                                            metrikaResponse.getMetrics().stream()
                                                    .map(list ->
                                                            list.stream()
                                                                    .map(Double::intValue)
                                                                    .collect(Collectors.toList())
                                                    )
                                                    .collect(Collectors.toList())
                                    )
                                    .build()
                    )
                    .collect(Collectors.toList());

        } catch (final IOException e) {
            LOGGER.error("Can't read from json Exception message {}", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public String toQuery() {
        return String.format(
                QUERY_TEMPLATE,
                counterId,
                makeGoal(goalId),
                startIntervalDate,
                endIntervalDate,
                oauthToken
        );
    }
}

