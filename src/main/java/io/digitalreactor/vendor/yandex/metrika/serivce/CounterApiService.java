package io.digitalreactor.vendor.yandex.metrika.serivce;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.vendor.yandex.metrika.model.Counter;
import io.digitalreactor.vendor.yandex.metrika.model.CounterResponse;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class CounterApiService {
    private static final Logger logger = LoggerFactory.getLogger(CounterApiService.class);

    public static final String COUNTETS = "/management/v1/counters?oauth_token=";

    private final String applicationAuth;
    private final String apiUrl;

    private final HttpClient httpClient;
    private final ObjectMapper mapper = new ObjectMapper();

    public CounterApiService(final HttpClient httpClient, final String apiUrl, final String applicationAuth) {
        this.applicationAuth = applicationAuth;
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    public List<Counter> getCounters(final String clientToken) {
        //TODO[St.maxim] error resolve
        try {
            final CounterResponse counterResponse = getCounterResponse(request(clientToken));

            return counterResponse.getCounters();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        //&oauth_token=
        return null;
    }

    private HttpGet request(final String accessToken) throws UnsupportedEncodingException {
        final HttpGet httpGet = new HttpGet("https://" + apiUrl + COUNTETS + accessToken);
        httpGet.addHeader("Authorization", applicationAuth);

        return httpGet;
    }

    private CounterResponse getCounterResponse(final HttpGet request) throws IOException {
        return httpClient.execute(request, response -> {
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return mapper.readValue(response.getEntity().getContent(), CounterResponse.class);
            } else {
                //TODO[St.maxim] custom exception
                throw new RuntimeException("Gotten: " + statusCode + IOUtils.toString(response.getEntity().getContent(), "UTF-8"));
            }
        });
    }
}
