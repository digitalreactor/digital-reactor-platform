package io.digitalreactor.vendor.yandex.oauth;

public interface TokenResolveService {
    /**
     * @param grantCode https://tech.yandex.ru/oauth/doc/dg/reference/console-client-docpage/
     * @return OAuth token for yandex API.
     */
    String getTokenByGrantCode(int grantCode);
}
