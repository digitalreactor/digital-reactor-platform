package io.digitalreactor.vendor.yandex.oauth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class TokenResolveServiceImpl implements TokenResolveService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenResolveServiceImpl.class);

    private final String TOKEN_RESOURCE = "/token";

    private final String applicationId;
    private final String applicationAuth;
    private final String clientSecret;
    private final String oauthUrl;
    private final HttpClient httpClient;

    private final ObjectMapper mapper = new ObjectMapper();

    public TokenResolveServiceImpl(
            @Nonnull final String applicationId,
            @Nonnull final String applicationAuth,
            @Nonnull final String clientSecret,
            @Nonnull final String oauthUrl,
            @Nonnull final HttpClient httpClient
    ) {
        this.applicationId = applicationId;
        this.applicationAuth = applicationAuth;
        this.clientSecret = clientSecret;
        this.oauthUrl = oauthUrl;
        this.httpClient = httpClient;
    }

    @Override
    @Nonnull
    public String getTokenByGrantCode(final int grantCode) {
        LOGGER.info("Try to get token by grantCode: {}", grantCode);
        try {
            return getToken(request(grantCode)).getAccessToken();
        } catch (final Exception e) {
            LOGGER.error("Token hasn't got by grantCode: {} , message: {}", grantCode, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private HttpPost request(final int grantCode) throws UnsupportedEncodingException {
        final HttpPost httpPost = new HttpPost("https://" + oauthUrl + TOKEN_RESOURCE);
        httpPost.addHeader("Authorization", applicationAuth);
        httpPost.setEntity(new StringEntity(requestBody(grantCode)));

        return httpPost;
    }

    private TokenResponse getToken(@Nonnull final HttpPost tokenRequest) throws IOException {
        return httpClient.execute(tokenRequest, response -> {
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return mapper.readValue(response.getEntity().getContent(), TokenResponse.class);
            } else {
                throw new RuntimeException("The status code status was obtained: " + statusCode);
            }
        });
    }

    private String requestBody(final int grantCode) {
        return "grant_type=authorization_code&code=" + grantCode +
                "&client_id=" + applicationId + "&client_secret=" + clientSecret;
    }
}
