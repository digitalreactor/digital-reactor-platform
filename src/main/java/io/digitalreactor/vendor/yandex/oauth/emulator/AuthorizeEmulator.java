package io.digitalreactor.vendor.yandex.oauth.emulator;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;

@Profile(value = SPRING_PROFILE_TEST)
@RestController
@RequestMapping(value = "authorize")
public class AuthorizeEmulator {
    public static final int CODE = 23432;

    //response_type=code&client_id=
    @RequestMapping(method = RequestMethod.GET)
    public void get(final HttpServletResponse response) throws IOException {
        response.sendRedirect("/oauth/yandex/grant_code?code=" + CODE + "&state=23432");
    }
}