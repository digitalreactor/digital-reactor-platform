package io.digitalreactor.analytic.metrika.common.report.object;

import io.digitalreactor.analytic.metrika.model.DSPCutType;
import lombok.*;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SearchPhraseCut {
    private String goalName;
    private DSPCutType type;
    private List<SearchPhrase> successPhrases;
    private List<SearchPhrase> badPhrases;
}
