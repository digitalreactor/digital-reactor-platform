package io.digitalreactor.analytic.metrika.common.report;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.digitalreactor.analytic.metrika.common.report.object.ReferringSource;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 04.04.17.
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class ReferringSourceReport extends BaseReportType {
    private SamplingInterval sampling;

    private ReportGoal reportGoal;

    private List<ReferringSource> sources;
}
