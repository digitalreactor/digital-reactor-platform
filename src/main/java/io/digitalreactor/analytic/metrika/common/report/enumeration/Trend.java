package io.digitalreactor.analytic.metrika.common.report.enumeration;

public enum Trend {
    INSUFFICIENT_DATA, INCREASING, UNALTERED, DECREASING
}
