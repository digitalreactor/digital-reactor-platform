package io.digitalreactor.analytic.metrika.common.report;

import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhrase;
import lombok.*;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 02.04.17.
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SearchPhraseReport extends BaseReportType {
    private SamplingInterval sampling;

    private ReportGoal reportGoal;

    private List<SearchPhrase> successPhrases;
    private List<SearchPhrase> badPhrases;
}
