package io.digitalreactor.analytic.metrika.common.report.enumeration;

import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingIntervalEvaluator;

import java.util.function.Supplier;

/**
 * digital-reactor
 * Created by igor on 02.05.17.
 */

public enum Interval {
    TODAY(SamplingIntervalEvaluator::today),
    YESTERDAY(SamplingIntervalEvaluator::yesterday),
    LAST_7_DAYS(SamplingIntervalEvaluator::last7Day),
    LAST_30_DAYS(SamplingIntervalEvaluator::last30Day),
    LAST_MONTH(SamplingIntervalEvaluator::lastMonth),
    CUSTOM(SamplingIntervalEvaluator::custom);

    private final Supplier<SamplingInterval> samplingIntervalResolver;

    Interval(final Supplier<SamplingInterval> samplingIntervalResolver) {
        this.samplingIntervalResolver = samplingIntervalResolver;
    }

    public SamplingInterval getSamplingInterval() {
        return samplingIntervalResolver.get();
    }
}