package io.digitalreactor.analytic.metrika.common.report;

import io.digitalreactor.analytic.metrika.common.report.object.Page;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import lombok.*;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 27.03.17.
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PagesWithHighFailureRateReport extends BaseReportType {
    private SamplingInterval sampling;

    private ReportGoal reportGoal;

    private List<Page> metrics;
    private int minVisits;
}
