package io.digitalreactor.analytic.metrika.common.report.object;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.digitalreactor.analytic.metrika.common.report.enumeration.Interval;
import lombok.*;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper=false)
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SamplingInterval {
    private Interval interval;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate startDate;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate endDate;
    private String signature;

    public SamplingInterval(final Interval interval) {
        this(interval.getSamplingInterval());
        this.interval = interval;
    }

    private SamplingInterval(final SamplingInterval samplingInterval) {
        this.startDate = samplingInterval.startDate;
        this.endDate = samplingInterval.endDate;
        this.signature = samplingInterval.signature;
    }

    public static SamplingInterval of(final Interval interval) {
        return new SamplingInterval(interval);
    }
}
