package io.digitalreactor.analytic.metrika.common.report.enumeration;

public enum DayType {
    HOLIDAY, WEEKDAY
}
