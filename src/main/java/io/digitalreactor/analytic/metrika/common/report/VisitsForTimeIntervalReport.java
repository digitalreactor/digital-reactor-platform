package io.digitalreactor.analytic.metrika.common.report;

import io.digitalreactor.analytic.metrika.common.report.enumeration.Trend;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.VisitsAndConversion;
import lombok.*;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class VisitsForTimeIntervalReport extends BaseReportType {
    private Trend trend;
    private SamplingInterval sampling;

    private ReportGoal reportGoal;

    private List<VisitsAndConversion> metrics;
    private int visitChange;
    private double visitChangePercent;

    private double conversionChange;
    private double conversionChangePercent;

    private String reason;
}
