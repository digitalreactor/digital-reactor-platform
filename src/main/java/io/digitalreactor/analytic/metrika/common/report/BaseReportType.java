package io.digitalreactor.analytic.metrika.common.report;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = VisitsForTimeIntervalReport.class, name = "visitsForTimeIntervalReport"),
        @JsonSubTypes.Type(value = PagesWithHighFailureRateReport.class, name = "pagesWithHighFailureRateReport"),
        @JsonSubTypes.Type(value = SearchPhraseReport.class, name = "searchPhraseReport"),
        @JsonSubTypes.Type(value = ReferringSourceReport.class, name = "referringSourceReport")
})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class BaseReportType {
    abstract public SamplingInterval getSampling();
    abstract public ReportGoal getReportGoal();
}
