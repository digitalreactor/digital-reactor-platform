package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.*;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 04.04.17.
 */

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class GoalReferringSource {
    private String name;
    private List<ReferringSource> sources;
    private double conversion;
    private double conversionChange;
    private int numberOfCompletedGoal;
}
