package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.*;

/**
 * digital-reactor
 * Created by igor on 03.04.17.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SearchPhrase {
    private String searchPhrase;
    private int visits;
    private double bounceRate;
    private double pageDepth;
    private double avgVisitDurationSeconds;
    private double quality;
}
