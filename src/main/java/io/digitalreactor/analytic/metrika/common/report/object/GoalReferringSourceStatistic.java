package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.*;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GoalReferringSourceStatistic {
    private int totalGoalVisits;
    private double totalGoalVisitsChangePercent;
    private double conversion;
    private double conversionChangePercent;
}
