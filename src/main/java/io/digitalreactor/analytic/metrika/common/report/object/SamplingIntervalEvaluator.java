package io.digitalreactor.analytic.metrika.common.report.object;

import static io.digitalreactor.analytic.metrika.common.report.enumeration.Interval.*;
import static java.time.LocalDate.now;

/**
 * digital-reactor
 * Created by igor on 02.05.17.
 */
public class SamplingIntervalEvaluator {
    public static SamplingInterval today() {
        return new SamplingInterval(TODAY, now(), now(), "Отчет за сегодня");
    }

    public static SamplingInterval yesterday() {
        return new SamplingInterval(
                YESTERDAY,
                now().minusDays(1),
                now(),
                "Отчет за " + now().minusDays(1).toString()
        );
    }

    public static SamplingInterval last7Day() {
        return new SamplingInterval(
                LAST_7_DAYS,
                now().minusDays(8),
                now().minusDays(1),
                "Отчет за " + now().minusDays(8).toString() + " по " + now().minusDays(1).toString());
    }

    public static SamplingInterval last30Day() {
        return new SamplingInterval(
                LAST_30_DAYS,
                now().minusDays(31),
                now().minusDays(1),
                "Отчет за " + now().minusDays(31).toString() + " по " + now().minusDays(1).toString());
    }

    public static SamplingInterval lastMonth() {
        return new SamplingInterval(
                LAST_MONTH,
                now().minusMonths(1).withDayOfMonth(1),
                now().minusMonths(1).withDayOfMonth(now().minusMonths(1).lengthOfMonth()),
                "Отчет за " + now().minusMonths(1).getMonth());
    }

    public static SamplingInterval custom() {
        return new SamplingInterval(CUSTOM,now(), now(), "null");
    }
}
