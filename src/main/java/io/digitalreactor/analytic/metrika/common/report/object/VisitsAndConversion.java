package io.digitalreactor.analytic.metrika.common.report.object;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.digitalreactor.analytic.metrika.common.report.enumeration.DayType;
import lombok.*;

import java.time.LocalDate;

/**
 * digital-reactor
 * Created by igor on 08.05.17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VisitsAndConversion {
    private int number;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate date;
    private DayType dayType;

    private double conversion;
}
