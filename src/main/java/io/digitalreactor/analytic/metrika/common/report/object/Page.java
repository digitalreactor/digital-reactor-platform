package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.*;

/**
 * digital-reactor
 * Created by igor on 08.05.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Page {
    private String siteName;
    private String favicon;

    private int visits;
    private double bounceRate;
    private int pageViews;
    private double avgVisitDurationSeconds;

    private double conversion;
}
