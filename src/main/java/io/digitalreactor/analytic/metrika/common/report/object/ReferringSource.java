package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 04.04.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReferringSource {
    private String name;
    @Builder.Default
    private List<Visit> metrics = new ArrayList();
    private int totalVisits;
    private double totalVisitsChangePercent;
    private GoalReferringSourceStatistic goalReferringSourceStatistic;
}
