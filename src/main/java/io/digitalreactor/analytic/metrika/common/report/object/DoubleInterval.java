package io.digitalreactor.analytic.metrika.common.report.object;

import lombok.Data;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * digital-reactor
 * Created by igor on 05.04.17.
 */

@Data
public class DoubleInterval {
    private LocalDate start;
    private LocalDate middle;
    private LocalDate end;

    private SamplingInterval samplingInterval;

    public DoubleInterval(final SamplingInterval samplingInterval) {
        this.samplingInterval = samplingInterval;
        this.middle = samplingInterval.getStartDate();
        this.end = samplingInterval.getEndDate();

        long daysBetween = DAYS.between(middle, end);

        if (daysBetween == 0) {
            daysBetween = 1;
        }

        this.start = middle.minusDays(daysBetween);
    }

    public LocalDate firstDayOfSecondInterval() {
        return middle;
    }
}
