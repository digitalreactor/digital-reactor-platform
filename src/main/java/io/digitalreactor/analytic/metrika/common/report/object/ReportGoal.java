package io.digitalreactor.analytic.metrika.common.report.object;

import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import lombok.Builder;
import lombok.Data;

/**
 * digital-reactor
 * Created by igor on 28.04.17.
 */

@Data
@Builder
public class ReportGoal {
    String goalName;
    long goalId;

    public static ReportGoal getSystemGoal() {
        final Goal systemGoal = Goal.getSystemGoal();
        return ReportGoal.builder().goalName(systemGoal.getName()).goalId(systemGoal.getId()).build();
    }

    public static ReportGoal valueOf(final Goal goal) {
        return ReportGoal.builder().goalName(goal.getName()).goalId(goal.getId()).build();
    }
}
