package io.digitalreactor.analytic.metrika.model.summary;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SummaryExtend {
    private long id;
    
    private long projectId;
    private String projectName;
    
    private SummaryStatus status;
    private LocalDateTime loadDate;
}
