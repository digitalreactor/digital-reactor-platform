package io.digitalreactor.analytic.metrika.model.summary;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Summary {
    private long id;
    private long projectId;
    private SummaryStatus status;
    private LocalDateTime loadDate;
}
