package io.digitalreactor.analytic.metrika.model.summary;

import javax.annotation.Nonnull;

public enum SummaryStatus {
    NEW, LOADING, COMPLETED, BROKEN;

    public static SummaryStatus getStatus(@Nonnull final String value) {
        if (NEW.name().equals(value)) {
            return NEW;
        } else if (LOADING.name().equals(value)) {
            return LOADING;
        } else if (COMPLETED.name().equals(value)) {
            return COMPLETED;
        } else if (BROKEN.name().equals(value)) {
            return BROKEN;
        }

        throw new RuntimeException("Project status not found for: " + value);
    }
}
