package io.digitalreactor.analytic.metrika.model;

/**
 * Created by ingvard on 05.01.17.
 */
public enum DSPCutType {
    GOAL, SYSTEM
}
