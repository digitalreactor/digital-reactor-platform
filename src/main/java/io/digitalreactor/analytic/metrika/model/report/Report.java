package io.digitalreactor.analytic.metrika.model.report;

import io.digitalreactor.analytic.metrika.common.report.BaseReportType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Report {
    private long id;
    private long summaryId;
    private String type;
    private BaseReportType payload;
}
