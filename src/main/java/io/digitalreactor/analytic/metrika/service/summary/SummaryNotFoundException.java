package io.digitalreactor.analytic.metrika.service.summary;

public class SummaryNotFoundException extends SummaryServiceException {
    public SummaryNotFoundException(final String message) {
        super(message);
    }
}
