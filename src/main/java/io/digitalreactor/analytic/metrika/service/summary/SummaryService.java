package io.digitalreactor.analytic.metrika.service.summary;

import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.service.summary.dto.SummaryInfo;

import java.util.List;
import java.util.Optional;

public interface SummaryService {
    Summary getLastSummary(final long userId, final long projectId) throws SummaryNotFoundException;

    Summary makeSummary(long projectId);

    /**
     * The method used for loading summaries with NEW status. It is a schedule process.
     */
    void autoLoadNewSummary();

    Summary getSummaryById(long summaryId);

    List<Summary> getSummaries(long userId, long projectId);

    SummaryInfo getSummaryInfo(long userId, long summaryId);
}
