package io.digitalreactor.analytic.metrika.service.report;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digitalreactor.analytic.metrika.common.report.BaseReportType;
import io.digitalreactor.analytic.metrika.common.report.enumeration.Interval;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.dao.ReportDao;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.service.report.builder.pages.PagesWithHighFailureRateBuilder;
import io.digitalreactor.analytic.metrika.service.report.builder.visits.VisitsForTimeIntervalReportBuilder;
import io.digitalreactor.analytic.metrika.service.report.builder.referringsources.ReferringSourceBuilder;
import io.digitalreactor.analytic.metrika.service.report.builder.searchphrase.SearchPhraseBuilder;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.service.counter.CounterService;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.serivce.GoalApiService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);
    private final ReportDao reportDao;
    private final VisitsForTimeIntervalReportBuilder visitsForTimeIntervalReportBuilder;
    private final PagesWithHighFailureRateBuilder pagesWithHighFailureRateBuilder;
    private final SearchPhraseBuilder searchPhraseBuilder;
    private final ReferringSourceBuilder referringSourceBuilder;
    private final AccessTokenService accessTokenService;
    private final CounterService counterService;
    private final GoalApiService goalApiService;

    @Override
    @Transactional
    public boolean tryToMakeDefaultReports(final long summaryId) {
        final Optional<MetrikaCounter> counterOpt = counterService.findBySummaryId(summaryId);
        if (!counterOpt.isPresent()) {
            LOGGER.error("Counter for summary with id {} doesn't present", summaryId);
            return false;
        }
        final MetrikaCounter counter = counterOpt.get();
        final Optional<MetrikaToken> metrikaTokenOpt = accessTokenService.findByTokenId(counter.getTokenId());
        if (!metrikaTokenOpt.isPresent()) {
            LOGGER.error("Token with id {} doesn't exists", counter.getTokenId());
            return false;
        }
        final MetrikaToken metrikaToken = metrikaTokenOpt.get();

        final List<BaseReportType> reports = new ArrayList<>();

        final List<Goal> goals = goalApiService.getGoals(counter.getExternalId(), metrikaToken.getToken());
        goals.add(Goal.getSystemGoal());

        for (final Interval interval : Interval.values()) {
            final SamplingInterval samplingInterval = new SamplingInterval(interval);
            if (samplingInterval.getInterval() != Interval.CUSTOM) {
                reports.addAll(visitsForTimeIntervalReportBuilder.makeReports(metrikaToken, counter, samplingInterval, goals));
                reports.addAll(pagesWithHighFailureRateBuilder.makeReports(metrikaToken, counter, samplingInterval, goals));
                reports.addAll(searchPhraseBuilder.makeReports(metrikaToken, counter, samplingInterval, goals));
                reports.addAll(referringSourceBuilder.makeReports(metrikaToken,counter,samplingInterval,goals));
            }
        }

        reports.forEach(report -> {
            try {
                reportDao.add(summaryId, report);
            } catch (final JsonProcessingException e) {
                LOGGER.error(String.format("Can't make report for summaryId: %s", summaryId), e);
            }
        });

        return true;
    }

    @Override
    @Transactional
    public List<Report> getReports(final long userId, final long summaryId, final String type) {
        return reportDao.findBy(userId, summaryId, type);
    }

    @Override
    @Transactional
    public List<ReportGoal> findAllGoalsForSummary(final long userId, final long summaryId) {
        return reportDao.findAllGoalsForSummary(userId, summaryId);
    }

    @Override
    public Report getReport(final long userId, final long reportId) {
        final Optional<Report> report = reportDao.findByUserIdAndReportId(userId, reportId);
        return report.get();
    }
}