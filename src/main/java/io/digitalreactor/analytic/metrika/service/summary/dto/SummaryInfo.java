package io.digitalreactor.analytic.metrika.service.summary.dto;

import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.model.summary.SummaryStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class SummaryInfo {
    private long summaryId;
    private List<ReportGoal> goals;
    private String projectName;
    private long projectId;
    private SummaryStatus status;
    private LocalDateTime date;
}
