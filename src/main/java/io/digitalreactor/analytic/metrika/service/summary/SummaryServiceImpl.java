package io.digitalreactor.analytic.metrika.service.summary;

import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.dao.SummaryDao;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.model.summary.SummaryExtend;
import io.digitalreactor.analytic.metrika.service.report.ReportService;
import io.digitalreactor.analytic.metrika.service.summary.dto.SummaryInfo;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.LOADING;

@Service
@AllArgsConstructor
public class SummaryServiceImpl implements SummaryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SummaryServiceImpl.class);
    private final SummaryDao summaryDao;
    private final SummaryLoader summaryLoader;
    private final ReportService reportService;

    @Transactional
    public Summary getLastSummary(final long userId, final long projectId) throws SummaryNotFoundException {
        final Optional<Summary> summaryOpt = summaryDao.findLastSummaryByProjectId(userId, projectId);
        if (!summaryOpt.isPresent()) {
            throw new SummaryNotFoundException(String.format("Last summary not found for project %s user: %s", projectId, userId));
        }

        return summaryOpt.get();
    }

    @Override
    @Transactional
    public Summary makeSummary(final long projectId) {
        LOGGER.info("Make new summary for project {}", projectId);
        return summaryDao.add(projectId, LocalDateTime.now());
    }

    @Override
    @Scheduled(fixedDelay = 5 * 1000)
    public void autoLoadNewSummary() {
        LOGGER.info("Auto load a new summary started.");
        final List<Summary> newSummaries = summaryLoader.findAllNewSummaries();
        for (final Summary newSummary : newSummaries) {
            summaryLoader.markAs(newSummary.getId(), LOADING);
            summaryLoader.loadSummary(newSummary);
        }
    }

    @Override
    @Transactional
    public Summary getSummaryById(final long summaryId) throws SummaryNotFoundException {
        final Optional<Summary> summaryOpt = summaryDao.findSummaryById(summaryId);
        if (!summaryOpt.isPresent()) {
            throw new SummaryNotFoundException(String.format("Summary with id: %s not found.", summaryId));
        }

        return summaryOpt.get();
    }

    @Override
    @Transactional
    public List<Summary> getSummaries(final long userId, final long projectId) {
        return summaryDao.findAllByUserAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public SummaryInfo getSummaryInfo(long userId, long summaryId) {
        final Optional<SummaryExtend> summaryExtendOpt = summaryDao.findExtendSummaryById(userId, summaryId);

        if (!summaryExtendOpt.isPresent()) {
            throw new SummaryNotFoundException(String.format("Summary with id: %s not found.", summaryId));
        }
        final SummaryExtend summaryExtend = summaryExtendOpt.get();
        final List<ReportGoal> goals = reportService.findAllGoalsForSummary(userId, summaryId);

        return SummaryInfo.builder()
                .date(summaryExtend.getLoadDate())
                .projectId(summaryExtend.getProjectId())
                .projectName(summaryExtend.getProjectName())
                .status(summaryExtend.getStatus())
                .summaryId(summaryExtend.getId())
                .goals(goals)
                .build();
    }
}
