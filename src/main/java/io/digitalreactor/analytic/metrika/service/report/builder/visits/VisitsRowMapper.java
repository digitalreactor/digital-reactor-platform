package io.digitalreactor.analytic.metrika.service.report.builder.visits;

import io.digitalreactor.analytic.metrika.common.report.object.VisitsAndConversion;
import io.digitalreactor.vendor.yandex.metrika.model.VisitWithGoalRow;
import org.mapstruct.Mapper;

/**
 * digital-reactor
 * Created on 14.05.17.
 */

@Mapper(componentModel = "spring")
public interface VisitsRowMapper {
    VisitsAndConversion map(VisitWithGoalRow visitWithGoalRow);
}
