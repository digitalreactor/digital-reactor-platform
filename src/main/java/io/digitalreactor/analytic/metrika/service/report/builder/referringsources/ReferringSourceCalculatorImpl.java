package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.enumeration.DayType;
import io.digitalreactor.analytic.metrika.common.report.object.*;
import io.digitalreactor.analytic.metrika.service.report.ReportMathUtils;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static io.digitalreactor.analytic.metrika.service.report.ReportMathUtils.changePercent;
import static io.digitalreactor.analytic.metrika.service.report.ReportMathUtils.conversion;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */

public class ReferringSourceCalculatorImpl implements ReferringSourceCalculator {
    private static final int WITHOUT_GOAL = 0;
    private static final int GOAL = 1;

    @Override
    @NonNull
    public ReferringSourceReport calculateAll(
            @NonNull final List<ReferringSourceRow> referringSourceRows,
            @NonNull final DoubleInterval doubleInterval,
            @NonNull final Goal goal
    ) {
        final List<ReferringSource> sources;
        if (goal.getId() == 0) {
            sources = calculateSource(doubleInterval, referringSourceRows, WITHOUT_GOAL);
        } else {
            sources = calculateSource(doubleInterval, referringSourceRows, GOAL);
        }

        return ReferringSourceReport.builder()
                .sources(sources)
                .sampling(doubleInterval.getSamplingInterval())
                .reportGoal(ReportGoal.builder().goalName(goal.getName()).goalId((long) goal.getId()).build())
                .build();
    }


    @Override
    @NonNull
    public List<ReferringSource> calculateSource(
            @NonNull final DoubleInterval doubleInterval,
            @NonNull final List<ReferringSourceRow> referringSourceRows,
            final int goalNumber) {
        return referringSourceRows.stream()
                .map(row -> referringSource(doubleInterval, row, goalNumber))
                .collect(Collectors.toList());
    }

    private ReferringSource referringSource(final DoubleInterval doubleInterval, final ReferringSourceRow row, final int goalNumber) {
        final ReferringSource.ReferringSourceBuilder referringSourceBuilder = ReferringSource.builder()
                .name(row.getName());

        final List<Visit> metrics = visitsListWithDay(
                row.getMetrics().get(0),
                doubleInterval.firstDayOfSecondInterval()
        );

        final int totalVisits = row.getSums().get(0).getRight();
        final double totalVisitsChangePercent = ReportMathUtils.changePercent(
                row.getSums().get(0).getLeft(),
                row.getSums().get(0).getRight()
        );

        referringSourceBuilder.metrics(metrics)
                .totalVisits(totalVisits)
                .totalVisitsChangePercent(totalVisitsChangePercent);

        if (goalNumber != WITHOUT_GOAL) {
            referringSourceBuilder.goalReferringSourceStatistic(calculateStatistic(
                    row.getSums().get(goalNumber),
                    row.getSums().get(0)));
        } else {
            referringSourceBuilder.goalReferringSourceStatistic(
                    getEmptyStatistic()
            );
        }

        return referringSourceBuilder.build();
    }

    private GoalReferringSourceStatistic getEmptyStatistic() {
        return GoalReferringSourceStatistic.builder().build();
    }

    private GoalReferringSourceStatistic calculateStatistic(
            final Pair<Integer, Integer> sumsForDoubleInterval,
            final Pair<Integer, Integer> totalVisitsForDoubleInterval) {
        final int totalGoalVisits = sumsForDoubleInterval.getRight();
        final double totalGoalVisitsChangePercent = ReportMathUtils.changePercent(
                sumsForDoubleInterval.getLeft(),
                sumsForDoubleInterval.getRight());

        final double oldConversion = conversion(
                sumsForDoubleInterval.getLeft(),
                totalVisitsForDoubleInterval.getLeft()
        );

        final double newConversion = conversion(
                sumsForDoubleInterval.getRight(),
                totalVisitsForDoubleInterval.getRight()
        );

        final double conversionChangePercent = changePercent(oldConversion, newConversion);

        return GoalReferringSourceStatistic.builder()
                .totalGoalVisits(totalGoalVisits)
                .totalGoalVisitsChangePercent(totalGoalVisitsChangePercent)
                .conversion(newConversion)
                .conversionChangePercent(conversionChangePercent)
                .build();
    }

    private List<Visit> visitsListWithDay(final List<Integer> metrics, final LocalDate localDate) {
        return metrics.stream().map(metrica -> Visit.builder()
                .date(localDate)
                .dayType(localDate.getDayOfWeek().getValue() > 5 ? DayType.HOLIDAY : DayType.WEEKDAY)
                .number(metrica)
                .build()
        ).collect(Collectors.toList());
    }
}