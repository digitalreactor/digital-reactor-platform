package io.digitalreactor.analytic.metrika.service.report.builder.pages;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.object.Page;
import io.digitalreactor.vendor.yandex.metrika.model.PageRow;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.serivce.ReportApiService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * digital-reactor
 * Created by igor on 27.03.17.
 */

@Service
@AllArgsConstructor
public class PagesWithHighFailureRateBuilder {
    private static final int MIN_VISITS = 5;
    private static final int MAX_METRICS = 5;

    private final ReportApiService reportApiService;
    private final PageRowMapper pageRowMapper;

    public List<PagesWithHighFailureRateReport> makeReports(
            @NonNull final MetrikaToken metrikaToken,
            @NonNull final MetrikaCounter counter,
            @NonNull final SamplingInterval samplingInterval,
            @NonNull final List<Goal> goals
    ) {
        final LocalDate startDate = samplingInterval.getStartDate();
        final LocalDate endDate = samplingInterval.getEndDate();

        return goals.stream()
                .map(
                        goal ->
                                Pair.of(
                                        reportApiService.getPages(
                                                metrikaToken.getToken(),
                                                counter.getExternalId(),
                                                startDate,
                                                endDate,
                                                goal
                                        ),
                                        goal
                                )
                )
                .map(
                        pagesAndGoalPair ->
                                makeReportByRowData(
                                        pagesAndGoalPair.getLeft(),
                                        samplingInterval,
                                        pagesAndGoalPair.getRight()
                                )
                )
                .collect(Collectors.toList());
    }

    private PagesWithHighFailureRateReport makeReportByRowData(
            final List<PageRow> pageRows,
            @Nonnull final SamplingInterval samplingInterval,
            final Goal goal
    ) {
        return PagesWithHighFailureRateReport.builder()
                .reportGoal(ReportGoal.valueOf(goal))
                .sampling(samplingInterval)
                .metrics(mapPages(filteredAndSortedPages(pageRows)))
                .minVisits(MIN_VISITS)
                .build();
    }

    public List<PageRow> filteredAndSortedPages(final List<PageRow> pageRows) {
        return pageRows.stream().filter(page -> page.getVisits() > MIN_VISITS)
                .sorted(PageRow::compareTo)
                .limit(MAX_METRICS)
                .collect(Collectors.toList());
    }

    public List<Page> mapPages(final List<PageRow> pageRows) {
        return pageRows.stream()
                .map(pageRowMapper::map)
                .collect(Collectors.toList());
    }
}
