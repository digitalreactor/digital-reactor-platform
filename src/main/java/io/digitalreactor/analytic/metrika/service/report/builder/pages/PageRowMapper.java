package io.digitalreactor.analytic.metrika.service.report.builder.pages;

import io.digitalreactor.analytic.metrika.common.report.object.Page;
import io.digitalreactor.vendor.yandex.metrika.model.PageRow;
import org.mapstruct.Mapper;

/**
 * digital-reactor
 * Created on 14.05.17.
 */

@Mapper(componentModel = "spring")
public interface PageRowMapper {
    Page map(PageRow pageRow);
}
