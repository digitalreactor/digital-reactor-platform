package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.DoubleInterval;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import io.digitalreactor.vendor.yandex.metrika.serivce.ReportApiService;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;


/**
 * digital-reactor
 * Created by igor on 05.04.17.
 */

@Service
public class ReferringSourceBuilder {
    private final ReportApiService reportApiService;
    private final ReferringSourceCalculatorImpl referringSourceCalculator = new ReferringSourceCalculatorImpl();

    public ReferringSourceBuilder(final ReportApiService reportApiService) {
        this.reportApiService = reportApiService;
    }

    public List<ReferringSourceReport> makeReports(@NonNull final MetrikaToken metrikaToken,
                                                   @NonNull final MetrikaCounter counter,
                                                   @NonNull final SamplingInterval samplingInterval,
                                                   @Nonnull final List<Goal> goals) {
        final DoubleInterval doubleInterval = new DoubleInterval(samplingInterval);

        final List<Pair<List<ReferringSourceRow>,Goal>> referringSourcesByGoals =
                goals
                        .stream()
                        .map(
                                goal -> Pair.of(
                                        reportApiService.getSources(
                                                metrikaToken.getToken(), counter.getExternalId(), doubleInterval.getStart(), doubleInterval.getEnd(), goal
                                        ),
                                        goal
                                )
                        )
                        .collect(Collectors.toList());

        evaluateSumsForReferringSourceRowList(referringSourcesByGoals);

        return referringSourcesByGoals
                .stream()
                .map(
                        referringSourcesByGoal ->
                            referringSourceCalculator.calculateAll(
                                    referringSourcesByGoal.getLeft(),
                                    doubleInterval,
                                    referringSourcesByGoal.getRight()
                            )
                )
                .collect(Collectors.toList());
    }

    public static List<Pair<List<ReferringSourceRow>, Goal>> evaluateSumsForReferringSourceRowList(final List<Pair<List<ReferringSourceRow>, Goal>> referringSourcesByGoals) {
        referringSourcesByGoals
                .forEach(
                        referringSourceRows ->
                                referringSourceRows
                                        .getLeft()
                                        .forEach(ReferringSourceRow::evaluateSums)
                );

        return referringSourcesByGoals;
    }
}
