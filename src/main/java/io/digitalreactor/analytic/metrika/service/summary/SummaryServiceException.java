package io.digitalreactor.analytic.metrika.service.summary;

public class SummaryServiceException extends RuntimeException {
    public SummaryServiceException(final String message) {
        super(message);
    }
}
