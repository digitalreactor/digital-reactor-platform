package io.digitalreactor.analytic.metrika.service.report;

import lombok.NonNull;

import java.util.List;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */
public class ReportMathUtils {
    private static final double EPS = 1e-5;

    public static double changePercent(
            @NonNull final List<Integer> firstInterval,
            @NonNull final List<Integer> secondInterval) {

        final int firstIntervalSum = sum(firstInterval);
        final int secondIntervalSum = sum(secondInterval);

        return changePercent(firstIntervalSum, secondIntervalSum);
    }

    public static double conversion(final int goalVisits, final int allVisits) {
        if (allVisits == 0) {
            return 0.0;
        } else {
            return (double) goalVisits / (double) allVisits;
        }
    }

    public static double changePercent(final double left, final double right) {
        if (isZero(left) && isZero(right)) {
            return 0;
        } else if (isZero(left)) {
            return 100;
        } else {
            return (right / left - 1) * 100;
        }
    }

    private static boolean isZero(final double left) {
        return abs(left) < EPS;
    }

    public static double changePercent(
            final int firstIntervalSum,
            final int secondIntervalSum) {

        if (firstIntervalSum == 0 && secondIntervalSum == 0) {
            return 0;
        } else if (firstIntervalSum == 0) {
            return 100;
        } else {
            return ((double) secondIntervalSum / firstIntervalSum - 1) * 100;
        }
    }

    public static <T> int sum(final List<T> list, final ToIntFunction<? super T> mapper) {
        return list
                .stream()
                .mapToInt(mapper)
                .sum();
    }

    public static <T> double sum(final List<T> list, final ToDoubleFunction<? super T> mapper) {
        return list
                .stream()
                .mapToDouble(mapper)
                .sum();
    }

    public static int sum(final List<Integer> list) {
        return list.stream().mapToInt(Integer::intValue).sum();
    }

    public static List<Integer> toIntegerList(final List<Double> doubles) {
        return doubles.stream().mapToInt(Double::intValue).boxed().collect(Collectors.toList());
    }

    public static <T> List<T> getFirstPartOfList(final List<T> list) {
        return list.subList(0, list.size() / 2);
    }

    public static <T> List<T> getSecondPartOfList(final List<T> list) {
        return list.subList(list.size() / 2, list.size());
    }
}
