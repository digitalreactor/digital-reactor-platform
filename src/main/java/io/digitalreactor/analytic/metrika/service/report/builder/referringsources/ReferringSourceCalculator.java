package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.object.DoubleInterval;
import io.digitalreactor.analytic.metrika.common.report.object.ReferringSource;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import lombok.NonNull;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */

public interface ReferringSourceCalculator {

    @NonNull
    ReferringSourceReport calculateAll(@NonNull List<ReferringSourceRow> sources,
                                       @NonNull DoubleInterval doubleInterval,
                                       @NonNull Goal goal);

    @NonNull
    List<ReferringSource> calculateSource(@NonNull DoubleInterval doubleInterval,
                                                      @NonNull List<ReferringSourceRow> sources,
                                                      int goalNumber);
}
