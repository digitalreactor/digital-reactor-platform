package io.digitalreactor.analytic.metrika.service.report.builder.searchphrase;

import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhrase;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.PhraseRow;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * digital-reactor
 * Created by igor on 02.04.17.
 */

@Service
public class SearchPhraseBuilderUtil {
    /*
        Имеем полученный из метрики список поисковых запросов(в которые входят
            -поисковая фраза
            -количество визитов
            -показатель отказов
            -глубина просмотра
            -средняя длительность посещения в секундах
            -и мапа имен целей и их конверсий)

        Необходимо получить некую сущность для каждой цели, которая будет включать в себя
            -цель
            -список успешных просмотров
            -список неуспешных просмотров

        Сначала из голого списка поисковых запросов и дополнительной информации получаем успешные и неуспешные
        Это определяется следующим образом
            -успешные : показатель отказов < 50 и среднее время на странице > 15 секунд
            -неуспешные : показатель отказов больше либо равен 50 ИЛИ глубина просмотра < 2 ИЛИ среднее время на сайте < 15

        Строим предварительный отчет из полученных данных
        Сопоставляем вычисленные на предыдущем шаге успешные и неуспешные фразы некой абстрактной цели, которую назовем System Test Goal
        Это нужно, потому что целей может на сайте не оказаться вообще

        Далее ищем фразы уже для реальных целей и добавляем в результирующий отчет

        Для каждой цели выделяем успешные и неуспешные фразы, делаем из этого сущность отчета и добавляем в результирующий список

        Для удобства введем такое понятие как конверсия.
        Конверсия равна 0 если количество визитов = 0,
        в противном случаее она равна конверсии, которая пришла от метрики поделенная на количество визитов
        {у тебя обозначено как количество юзеров, я не особо разбираюсь в том, что такое конверсия}

        Успешные фразы для цели определяются следующим образом
        К условиям, которые должны выполняться без целей добавляется то, что конверсия больше единицы

        Неуспешные фразы для цели определяются следующим образом
        Конверсия меньше единицы
     */

    /*
        We have list of search phrase structure from metrika that included
            -search phrase
            -visits amount
            -bounce rate
            -page depth
            -
        Имеем полученный из метрики список поисковых запросов(в которые входят
            -поисковая фраза
            -количество визитов
            -показатель отказов
            -глубина просмотра
            -Average visit duration in seconds
            -Map of goals and conversions

        Need to get list of some entities named SearchPhraseCut that included
            -goal
            -list of success phrases
            -list of bad phrases

        (Success and bad ate contingent terms. How to define this will be explained below)

        Firstly Get succsess s and bad phrases from row data of Phrases from Metrika without any goals
        Phrase is success if
            -bounce rate < 50 and
            -average visit duration > 15 seconds
        Phrse is bad if
            -bounce rate >= 50 OR
            -page depth < 2 OR
            -average visit duration < 15

        Make preliminary report from just recivied data.
        Link this data with abstract goal named System Test Goal

        Then look up succes and bad phrases for real goals and add to result.

        For each goal build succes and bad phrases.
        Make Entity
        Add to result.

        For comfort defite conversion term.
        Conversion equals 0, if visit amounts == 0,
        else if conversion = conversion from metrika (that associated with current goal) divided by visit amount

        Success phrases are phrases with conditions described above plus conversion > 1

        Bad phrases are phrases with conversion < 1
     */

    public static SearchPhraseReport makeReportByRowData(@Nonnull final List<PhraseRow> phrases,
                                                         @Nonnull final SamplingInterval samplingInterval,
                                                         @Nonnull final Goal goal) {
        final List<SearchPhrase> badPhrases = evaluateBadPhrase(phrases, goal);
        final List<SearchPhrase> successPhrases = evaluateSuccessPhrase(phrases, goal);

        return SearchPhraseReport.builder()
                .sampling(samplingInterval)
                .badPhrases(badPhrases)
                .successPhrases(successPhrases)
                .reportGoal(ReportGoal.builder().goalName(goal.getName()).goalId((long) goal.getId()).build())
                .build();

    }

    private static List<SearchPhrase> evaluateSuccessPhrase(final List<PhraseRow> phrases, final Goal goal) {
        if (goal.getId() == 0) {
            return filterPhrasesByConditionAndSortedByVisits(
                    phrases, SearchPhraseBuilderUtil::successCondition,
                    comparing(SearchPhrase::getVisits).reversed()
            );
        } else {
            return filterPhrasesByConditionAndSortedByVisits(
                    phrases,
                    p -> successCondition(p) && !badConditionWithGoal(p, goal.getId()),
                    comparing(SearchPhrase::getQuality).reversed()
            );
        }
    }

    private static List<SearchPhrase> evaluateBadPhrase(final List<PhraseRow> phrases, final Goal goal) {
        if (goal.getId() == 0) {
            return filterPhrasesByConditionAndSortedByVisits(
                    phrases,
                    SearchPhraseBuilderUtil::badCondition,
                    comparing(SearchPhrase::getVisits).reversed()
            );
        } else {
            return filterPhrasesByConditionAndSortedByVisits(
                    phrases,
                    p -> badConditionWithGoal(p, goal.getId()),
                    comparing(SearchPhrase::getVisits).reversed()
            );
        }
    }

    private static List<SearchPhrase> filterPhrasesByConditionAndSortedByVisits(
            final List<PhraseRow> phrases,
            final Predicate<PhraseRow> predicate,
            final Comparator<SearchPhrase> comparator) {
        return phrases.stream()
                .filter(predicate)
                .map(
                        p -> SearchPhrase.builder()
                                .searchPhrase(p.getSearchPhrase())
                                .visits(p.getVisits())
                                .bounceRate(p.getBounceRate())
                                .pageDepth(p.getPageDepth())
                                .avgVisitDurationSeconds(p.getAvgVisitDurationSeconds())
                                .quality(0)
                                .build()
                )
                .sorted(comparator)
                .limit(10)
                .collect(Collectors.toList());
    }

    private static boolean badCondition(final PhraseRow phrase) {
        return phrase.getBounceRate() >= 50.0 ||
                phrase.getPageDepth() < 2.0 ||
                phrase.getAvgVisitDurationSeconds() < 15;
    }

    private static boolean badConditionWithGoal(final PhraseRow phrase, final int goalId) {
        return conversion(phrase, goalId) < 1;
    }

    private static boolean successCondition(final PhraseRow phrase) {
        return phrase.getBounceRate() < 50.0
                && phrase.getAvgVisitDurationSeconds() > 15;
    }

    private static double conversion(final PhraseRow phrase, final int goalId) {
        //todo fix it
        final int countUsers = phrase.getGoalAndConversion().get(String.format("ym:s:goal%susers", goalId));

        return phrase.getVisits() == 0 ? 0 : ((double) countUsers) / ((double) phrase.getVisits());
    }

}
