package io.digitalreactor.analytic.metrika.service.report.builder.searchphrase;

import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.serivce.ReportApiService;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * digital-reactor
 * Created by igor on 02.04.17.
 */

@Service
public class SearchPhraseBuilder {
    private final ReportApiService reportApiService;

    public SearchPhraseBuilder(final ReportApiService reportApiService) {
        this.reportApiService = reportApiService;
    }

    public List<SearchPhraseReport> makeReports(@NonNull final MetrikaToken metrikaToken,
                                                @NonNull final MetrikaCounter counter,
                                                @Nonnull final SamplingInterval samplingInterval,
                                                @Nonnull final List<Goal> goals) {
        final LocalDate startDate = samplingInterval.getStartDate();
        final LocalDate endDate = samplingInterval.getEndDate();

        return goals.stream()
                .map(
                        goal -> Pair.of(
                                reportApiService.getPhrases(
                                        metrikaToken.getToken(),
                                        counter.getExternalId(),
                                        startDate,
                                        endDate,
                                        goal
                                ),
                                goal
                        )
                )
                .map(
                        pair ->
                                SearchPhraseBuilderUtil.makeReportByRowData(pair.getLeft(), samplingInterval, pair.getRight())
                )
                .collect(Collectors.toList());
    }
}