package io.digitalreactor.analytic.metrika.service.report;

import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.model.report.Report;

import java.util.List;

public interface ReportService {
    /**
     * It is demo method for MVP.
     *
     * @return status of creating.
     */
    boolean tryToMakeDefaultReports(long summaryId);

    <T> List<T> getReports(long userId, long summaryId, String type);

    List<ReportGoal> findAllGoalsForSummary(final long userId, final long summaryId);

    Report getReport(long id, long reportId);
}
