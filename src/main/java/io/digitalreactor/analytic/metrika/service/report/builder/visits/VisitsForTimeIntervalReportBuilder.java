package io.digitalreactor.analytic.metrika.service.report.builder.visits;

import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.analytic.metrika.common.report.enumeration.Trend;
import io.digitalreactor.analytic.metrika.common.report.object.DoubleInterval;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.analytic.metrika.common.report.object.VisitsAndConversion;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.VisitWithGoalRow;
import io.digitalreactor.vendor.yandex.metrika.serivce.ReportApiService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static io.digitalreactor.analytic.metrika.service.report.ReportMathUtils.*;

/**
 * Myjetbrains: DR-9 https://digitalreactor.myjetbrains.com/youtrack/issue/DR-9
 * Визиты сайта за последний (прошедний месяц).
 * Пример: если сейчас 26 марта, то отчёт будет построин за февраль, и будет сравниваться
 * с январём. Различное колличество дней в месяцах будет игнорированно.
 * <p>
 * Visits of a site for last month (last full month).
 * For example: If today 20 march the report will be for February relative to January.
 * The different counts of days of months are ignored.
 */

@Service
@AllArgsConstructor
public class VisitsForTimeIntervalReportBuilder {
    private final ReportApiService reportApiService;
    private final VisitsRowMapper visitsRowMapper;

    /**
     * For report it loads visits for previous 30 * 2 days from current date.
     *
     * @param metrikaToken
     * @param counter
     * @param goals
     * @return
     */
    public List<VisitsForTimeIntervalReport> makeReports(
            @NonNull final MetrikaToken metrikaToken,
            @NonNull final MetrikaCounter counter,
            @NonNull final SamplingInterval samplingInterval,
            @NonNull final List<Goal> goals
    ) {
        final DoubleInterval doubleInterval = new DoubleInterval(samplingInterval);

        return goals.stream()
                .map(
                        goal -> Pair.of(
                                reportApiService.getVisits(
                                        metrikaToken.getToken(),
                                        counter.getExternalId(),
                                        doubleInterval.getStart(),
                                        doubleInterval.getEnd(),
                                        goal),
                                goal
                        )
                )
                .map(
                        visitsAndGoalPair ->
                                makeReportByRowData(
                                        visitsAndGoalPair.getLeft(),
                                        doubleInterval,
                                        visitsAndGoalPair.getRight()
                                )
                )
                .collect(Collectors.toList());
    }

    /*
     * @param visits list contains 60 or 60 + number of day not full week.
     */
    private VisitsForTimeIntervalReport makeReportByRowData(
            final List<VisitWithGoalRow> visits,
            @NonNull final DoubleInterval doubleInterval,
            final Goal goal
    ) {

        final List<VisitsAndConversion> visitsWithGoals = mapVisits(visits);

        final Pair<Integer, Integer> visitsForTwoInterval = Pair.of(
                sumVisitsFirstInterval(visitsWithGoals, VisitsAndConversion::getNumber),
                sumVisitsSecondInterval(visitsWithGoals, VisitsAndConversion::getNumber)
        );

        final Pair<Double, Double> conversionsForTwoInterval = Pair.of(
                sumVisitsFirstInterval(visitsWithGoals, VisitsAndConversion::getConversion),
                sumVisitsSecondInterval(visitsWithGoals, VisitsAndConversion::getConversion)
        );

        final double trendChangePercent = changePercent(
                visitsForTwoInterval.getRight(),
                visitsForTwoInterval.getLeft()
        );
        final int visitChange = visitsForTwoInterval.getRight() - visitsForTwoInterval.getLeft();
        final Trend trend = trendChanges(visitChange);

        final double conversionChangePercent = changePercent(
                conversionsForTwoInterval.getRight(),
                conversionsForTwoInterval.getLeft()
        );
        final double conversionChange = conversionsForTwoInterval.getRight() - conversionsForTwoInterval.getLeft();

        return VisitsForTimeIntervalReport.builder()
                .sampling(doubleInterval.getSamplingInterval())
                .reportGoal(ReportGoal.valueOf(goal))
                .metrics(mapVisits(visits))
                .visitChangePercent(trendChangePercent)
                .trend(trend)
                .visitChange(visitChange)
                .conversionChange(conversionChange)
                .conversionChangePercent(conversionChangePercent)
                .reason("На посещаемость может влиять сезонность, отключение рекламного канала или снижение видимости сайта в поисковой выдачи.")
                .build();
    }

    private List<VisitsAndConversion> mapVisits(final List<VisitWithGoalRow> visits) {
        return visits.stream()
                .map(visitsRowMapper::map)
                .collect(Collectors.toList());
    }

    private Trend trendChanges(final double delta) {
        Trend trend = Trend.DECREASING;

        if (delta == 0) {
            trend = Trend.UNALTERED;
        } else if (delta > 0) {
            trend = Trend.INCREASING;
        }

        return trend;
    }

    private int sumVisitsFirstInterval(final List<VisitsAndConversion> visits, final ToIntFunction<? super VisitsAndConversion> mapper) {
        return sum(getFirstPartOfList(visits),mapper);
    }

    private int sumVisitsSecondInterval(final List<VisitsAndConversion> visits, final ToIntFunction<? super VisitsAndConversion> mapper) {
        return sum(getSecondPartOfList(visits), mapper);
    }

    private double sumVisitsFirstInterval(final List<VisitsAndConversion> visits, final ToDoubleFunction<? super VisitsAndConversion> mapper) {
        return sum(getFirstPartOfList(visits),mapper);
    }

    private double sumVisitsSecondInterval(final List<VisitsAndConversion> visits, final ToDoubleFunction<? super VisitsAndConversion> mapper) {
        return sum(getSecondPartOfList(visits), mapper);
    }
}