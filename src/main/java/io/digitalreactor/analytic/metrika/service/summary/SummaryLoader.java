package io.digitalreactor.analytic.metrika.service.summary;

import io.digitalreactor.analytic.metrika.dao.SummaryDao;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.model.summary.SummaryStatus;
import io.digitalreactor.analytic.metrika.service.report.ReportService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.*;

/**
 * digital-reactor
 * Created by igor on 04.05.17.
 */

@Service
@AllArgsConstructor
public class SummaryLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(SummaryLoader.class);
    private final SummaryDao summaryDao;
    private final ReportService reportService;

    @Transactional
    public List<Summary> findAllNewSummaries() {
        return summaryDao.findAllWithStatus(NEW);
    }

    @Transactional
    public void markAs(final long summaryId, final SummaryStatus summaryStatus) {
        final boolean isMarked = summaryDao.changeStatus(summaryId, summaryStatus);
        if(isMarked) {
            LOGGER.warn("Summary {} is marked as {}", summaryId, summaryStatus.name());
        } else {
            LOGGER.error("Summary {} isn't marked as {}", summaryId, summaryStatus.name());
        }
    }

    @Transactional
    public void loadSummary(final Summary summary) {
        try {
            reportService.tryToMakeDefaultReports(summary.getId());
            summaryDao.changeStatus(summary.getId(), COMPLETED);
            LOGGER.info("Summary {} is marked as COMPLETED", summary.getId());
        } catch (final Exception e) {
            LOGGER.error(String.format("Summary %s is crashed", summary.getId()), e);
            markAs(summary.getId(), BROKEN);
        }
    }


}
