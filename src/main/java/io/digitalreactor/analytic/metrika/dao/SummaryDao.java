package io.digitalreactor.analytic.metrika.dao;

import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.model.summary.SummaryExtend;
import io.digitalreactor.analytic.metrika.model.summary.SummaryStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.NEW;
import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class SummaryDao {
    private static final RowMapper<Summary> SUMMARY_MAPPER = (rs, rowNum) -> Summary.builder()
            .id(rs.getInt("id"))
            .projectId(rs.getInt("project_id"))
            .status(SummaryStatus.getStatus(rs.getString("status")))
            .loadDate(rs.getTimestamp("load_date").toLocalDateTime())
            .build();

    private static final RowMapper<SummaryExtend> SUMMARY_EXTEND_MAPPER = (rs, rowNum) -> SummaryExtend.builder()
            .id(rs.getInt("id"))
            .loadDate(rs.getTimestamp("load_date").toLocalDateTime())
            .projectId(rs.getInt("project_id"))
            .projectName(rs.getString("name"))
            .status(SummaryStatus.getStatus(rs.getString("status")))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public SummaryDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Summary add(final long projectId, final LocalDateTime localDateTime) {
        final SummaryStatus firstStatus = NEW;
        final int summaryId = jdbcTemplate.queryForObject(
                "INSERT INTO summaries (project_id, status, load_date) VALUES (?, CAST(? AS SUMMARY_STATUS), ?) RETURNING id",
                new Object[]{projectId, firstStatus.name(), Timestamp.valueOf(localDateTime)},
                Integer.class
        );

        return Summary.builder()
                .id(summaryId)
                .projectId(projectId)
                .status(firstStatus)
                .loadDate(localDateTime)
                .build();
    }

    public Optional<Summary> findLastSummaryByProjectId(final long userId, final long projectId) {
        final Summary summary = singleResult(jdbcTemplate.query(
                "SELECT summaries.* FROM summaries INNER JOIN projects ON summaries.project_id = projects.id"
                        + " WHERE summaries.project_id = ? AND projects.user_id = ? ORDER BY id DESC LIMIT 1",
                new Object[]{projectId, userId},
                SUMMARY_MAPPER
        ));

        return Optional.ofNullable(summary);
    }

    public List<Summary> findAllWithStatus(@Nonnull final SummaryStatus status) {
        return jdbcTemplate.query(
                "SELECT * FROM summaries WHERE status = CAST(? AS SUMMARY_STATUS)",
                new Object[]{status.name()},
                SUMMARY_MAPPER
        );
    }

    public boolean changeStatus(final long summaryId, @Nonnull final SummaryStatus status) {
        return 1 == jdbcTemplate.update(
                "UPDATE summaries SET status = CAST(? AS SUMMARY_STATUS) WHERE id = ?",
                status.name(),
                summaryId
        );
    }

    public Optional<Summary> findSummaryById(final long summaryId) {
        final Summary summary = singleResult(jdbcTemplate.query(
                "SELECT * FROM summaries WHERE id = ?",
                new Object[]{summaryId},
                SUMMARY_MAPPER
        ));

        return Optional.ofNullable(summary);
    }

    public List<Summary> findAllByUserAndProjectId(final long userId, final long projectId) {
        return jdbcTemplate.query(
                "SELECT summaries.* FROM summaries INNER JOIN projects ON summaries.project_id = projects.id"
                        + " WHERE summaries.project_id = ? AND projects.user_id = ? ORDER BY id DESC",
                new Object[]{projectId, userId},
                SUMMARY_MAPPER
        );
    }

    public Optional<SummaryExtend> findExtendSummaryById(final long userId, final long summaryId) {
        final SummaryExtend summaryExtend = singleResult(jdbcTemplate.query(
                "SELECT summaries.*, projects.name FROM summaries INNER JOIN projects ON summaries.project_id = projects.id"
                        + " WHERE summaries.id = ? AND projects.user_id = ? ",
                new Object[]{summaryId, userId},
                SUMMARY_EXTEND_MAPPER
        ));

        return Optional.ofNullable(summaryExtend);
    }
}
