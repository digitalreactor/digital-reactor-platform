package io.digitalreactor.analytic.metrika.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.analytic.metrika.common.report.BaseReportType;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.model.report.Report;
import lombok.NonNull;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
public class ReportDao {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final RowMapper<ReportGoal> REPORT_GOAL_MAPPER = (rs, rowNum) -> {
        try {
            BaseReportType baseReportType = MAPPER.readValue(rs.getString("payload"), BaseReportType.class);

            return baseReportType.getReportGoal();
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    };

    private static final RowMapper<Report> REPORT_MAPPER = (rs, rowNum) -> {
        try {
            return Report.builder()
                    .id(rs.getLong("id"))
                    .summaryId(rs.getLong("summary_id"))
                    .type(rs.getString("type"))
                    .payload(MAPPER.readValue(rs.getString("payload"), BaseReportType.class))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    };

    private final JdbcTemplate jdbcTemplate;

    public ReportDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Report add(final long summaryId, @Nonnull final BaseReportType report) throws JsonProcessingException {
        final String type = report.getClass().getSimpleName();

        final String stringPayload = MAPPER.writeValueAsString(report);

        final Long reportId = jdbcTemplate.queryForObject(
                "INSERT INTO reports (summary_id, type, payload) VALUES (?, ?, ?) RETURNING id",
                new Object[]{summaryId, type, stringPayload},
                Long.class
        );

        return Report.builder()
                .id(reportId)
                .summaryId(summaryId)
                .type(type)
                .payload(report)
                .build();
    }

    public Optional<Report> findByReportId(final long reportId) {
        final Report report = singleResult(jdbcTemplate.query(
                "SELECT * FROM reports WHERE id = ?",
                new Object[]{reportId},
                REPORT_MAPPER
        ));

        return Optional.ofNullable(report);
    }

    public List<Report> findBy(final long userId, final long summaryId, @NonNull final String reportType) {
        return jdbcTemplate.query(
                "SELECT reports.* FROM reports INNER JOIN summaries ON reports.summary_id = summaries.id " +
                        "INNER JOIN projects ON summaries.project_id = projects.id WHERE summaries.id = ? AND user_id = ? AND LOWER(reports.type) = LOWER(?)",
                new Object[]{summaryId, userId, reportType},
                REPORT_MAPPER
        );
    }

    public Optional<Report> findByUserIdAndReportId(final long userId, final long reportId) {
        final Report report = singleResult(jdbcTemplate.query(
                "SELECT reports.* FROM reports INNER JOIN summaries ON reports.summary_id = summaries.id " +
                        "INNER JOIN projects ON summaries.project_id = projects.id WHERE user_id = ? AND reports.id = ?",
                new Object[]{userId, reportId},
                REPORT_MAPPER
        ));

        return Optional.ofNullable(report);
    }

    /**
     * @return List of goals without duplicate.
     */
    public List<ReportGoal> findAllGoalsForSummary(final long userId, final long summaryId) {
        final List<ReportGoal> goals = jdbcTemplate.query(
                "SELECT reports.* FROM reports INNER JOIN summaries ON reports.summary_id = summaries.id " +
                        "INNER JOIN projects ON summaries.project_id = projects.id WHERE summaries.id = ? AND user_id = ?",
                new Object[]{summaryId, userId},
                REPORT_GOAL_MAPPER
        );

        return goals.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
