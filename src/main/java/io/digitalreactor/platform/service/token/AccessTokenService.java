package io.digitalreactor.platform.service.token;

import io.digitalreactor.platform.model.metrika.MetrikaToken;

import java.util.List;
import java.util.Optional;

public interface AccessTokenService {
    String getSyntheticToken(int grantCode);
    String popOriginalToken(String syntheticToken);
    String peekOriginalToken(String syntheticToken);
    int save(long userId, String token);
    List<MetrikaToken> findByUserId(long userId);
    Optional<MetrikaToken> findByTokenId(int tokenId);
}
