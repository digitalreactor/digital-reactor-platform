package io.digitalreactor.platform.service.token;

import io.digitalreactor.platform.dao.TokenDao;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.oauth.TokenResolveService;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static io.digitalreactor.platform.model.metrika.MetrikaTokenStatus.ACTIVE;
import static java.util.stream.Collectors.toList;

@Service
public class AccessTokenServiceImpl implements AccessTokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessTokenServiceImpl.class);
    // (syntheticToken, originalToken)
    private final ConcurrentHashMap<String, String> syntheticTokens = new ConcurrentHashMap<>();

    @Value("${security.aes-key}")
    private String aesKey;

    private final TokenResolveService tokenResolve;
    private final TokenDao tokenDao;

    public AccessTokenServiceImpl(
            final HttpClient httpClient,
            final TokenResolveService tokenResolve,
            final TokenDao tokenDao
    ) {
        this.tokenResolve = tokenResolve;
        this.tokenDao = tokenDao;
    }

    @Override
    public String getSyntheticToken(final int grantCode) {
        final String syntheticToken = UUID.randomUUID().toString();
        final String token = tokenResolve.getTokenByGrantCode(grantCode);
        syntheticTokens.put(syntheticToken, token);

        return syntheticToken;
    }

    @Override
    public String popOriginalToken(@Nonnull final String syntheticToken) {
        final String originalToken = syntheticTokens.computeIfAbsent(syntheticToken, st -> {
            throw new RuntimeException("Synthetic Token " + st + " not found");
        });
        syntheticTokens.remove(syntheticToken);

        return originalToken;
    }

    @Override
    public String peekOriginalToken(final String syntheticToken) {
        return syntheticTokens.computeIfAbsent(syntheticToken, st -> {
            throw new RuntimeException("Synthetic Token " + st + " not found");
        });
    }

    @Override
    @Transactional
    @SneakyThrows
    public int save(final long userId, @Nonnull final String token) {
        final Key secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");
        final Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final String encryptedToken = new Base64().encodeAsString(cipher.doFinal(token.getBytes()));

        return tokenDao.add(userId, encryptedToken, ACTIVE).getId();
    }

    @Override
    @Transactional
    @SneakyThrows
    public List<MetrikaToken> findByUserId(final long userId) {
        final Key secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");
        final Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        return tokenDao.findByUserId(userId).stream()
                .map(encryptedToken -> MetrikaToken.builder()
                        .status(encryptedToken.getStatus())
                        .userId(encryptedToken.getUserId())
                        .id(encryptedToken.getId())
                        .dateAdded(encryptedToken.getDateAdded())
                        .token(doFinal(cipher, encryptedToken.getToken()))
                        .build()).collect(toList());
    }

    @Override
    @Transactional
    @SneakyThrows
    public Optional<MetrikaToken> findByTokenId(final int tokenId) {
        final Key secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");
        final Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        return tokenDao.findById(tokenId).map(token ->
                MetrikaToken.builder()
                        .id(token.getId())
                        .status(token.getStatus())
                        .token(doFinal(cipher, token.getToken()))
                        .userId(token.getId())
                        .dateAdded(token.getDateAdded())
                        .build()
        );
    }

    @SneakyThrows
    private String doFinal(@Nonnull final Cipher cipher, @Nonnull final String text) {
        return new String(cipher.doFinal(new Base64().decode(text)));
    }
}
