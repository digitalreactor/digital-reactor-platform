package io.digitalreactor.platform.service.project;

import io.digitalreactor.platform.dao.ProjectDao;
import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.counter.CounterService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);

    private final ProjectDao projectDao;
    private final CounterService counterService;

    @Override
    @Transactional
    public Project addProject(final long userId, @Nonnull final String projectName) {
        LOGGER.info("add new project named \'{} \' for user with id = {}", projectName, userId);
        return projectDao.add(userId, projectName);
    }

    @Override
    @Transactional
    public List<Project> findAllByUserId(final long userId) {
        return projectDao.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public Optional<Project> findByProjectId(final long projectId) {
        return projectDao.findByProjectId(projectId);
    }

    @Override
    public boolean counterIdBelongsUser(final long userId, final long externalCounterId) {
        final List<CounterWithToken> allUnusedByUserId = counterService.findAllUnusedByUserId(userId);
        return allUnusedByUserId
                .stream()
                .filter(
                        counterWithToken ->
                                counterWithToken.getExternalId() == externalCounterId
                )
                .count() > 0;
    }
}
