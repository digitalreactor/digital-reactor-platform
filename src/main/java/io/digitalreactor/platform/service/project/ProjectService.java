package io.digitalreactor.platform.service.project;

import io.digitalreactor.platform.model.project.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectService {
    Project addProject(long userId, String projectName);

    List<Project> findAllByUserId(long userId);

    Optional<Project> findByProjectId(long projectId);

    boolean counterIdBelongsUser(long userId, long externalCounterId);
}
