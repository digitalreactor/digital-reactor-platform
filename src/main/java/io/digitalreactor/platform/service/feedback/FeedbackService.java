package io.digitalreactor.platform.service.feedback;

import io.digitalreactor.platform.dao.FeedbackDao;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class FeedbackService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackService.class);
    private final FeedbackDao feedbackDao;

    @Transactional
    public void save(@NonNull final String user, @NonNull final String message) {
        LOGGER.info("Feedback from: {}, message: {}", user, message);
        feedbackDao.save(user, message);
    }
}
