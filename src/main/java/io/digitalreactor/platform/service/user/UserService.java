package io.digitalreactor.platform.service.user;

import io.digitalreactor.platform.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {
    User register(String email, String password);

    Optional<User> findByEmail(String email);

    User getCurrentUser();
}
