package io.digitalreactor.platform.service.user;

import io.digitalreactor.platform.dao.UserDao;
import io.digitalreactor.platform.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserDao userDao;

    public UserServiceImpl(final UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public User register(@Nonnull final String email, @Nonnull final String password) {
        return userDao.register(email, password);
    }

    @Override
    @Transactional
    public Optional<User> findByEmail(@Nonnull final String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User getCurrentUser() {
        //может быть тут Optional.ofNullable()?
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@Nonnull final String email) throws UsernameNotFoundException {
        final Optional<User> userOpt = userDao.findByEmail(email);
        if (userOpt.isPresent()) {
            return userOpt.get();
        } else {
            final String message = String.format("User with email: %s not found.", email);
            LOGGER.warn(message);
            throw new UsernameNotFoundException(message);
        }
    }
}
