package io.digitalreactor.platform.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ServiceConfig {

    @Value("${mail.server}")
    private String mailServer;
    @Value("${mail.port}")
    private Integer mailPort;
    @Value("${mail.ssl}")
    private Boolean mailSsl;
    @Value("${mail.username}")
    private String mailUser;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.address.sender}")
    private String mailAddressSender;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
