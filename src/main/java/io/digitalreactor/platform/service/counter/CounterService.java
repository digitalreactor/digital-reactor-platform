package io.digitalreactor.platform.service.counter;

import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;

import java.util.List;
import java.util.Optional;

public interface CounterService {
    Optional<MetrikaCounter> findByProjectId(long projectId);

    List<MetrikaCounter> findAll(long userId);

    List<CounterWithToken> findAllUnusedByUserId(long userId);

    List<CounterWithToken> findAllUnusedByUserIdAndTokenId(long userId, int tokenId);

    Optional<MetrikaCounter> findBySummaryId(long summaryId);

    MetrikaCounter addCounterToProject(long projectId, int tokenId, long externalCounterId);
}
