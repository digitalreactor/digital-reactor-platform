package io.digitalreactor.platform.service.counter;

import io.digitalreactor.platform.dao.CounterDao;
import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.vendor.yandex.metrika.serivce.CounterApiService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CounterServiceImpl implements CounterService {
    private final CounterDao counterDao;
    private final AccessTokenService accessTokenService;
    private final CounterApiService counterApiService;

    @Override
    @Transactional
    public Optional<MetrikaCounter> findByProjectId(final long projectId) {
        return counterDao.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public List<MetrikaCounter> findAll(final long userId) {
        return counterDao.findAll(userId);
    }

    @Override
    @Transactional
    public List<CounterWithToken> findAllUnusedByUserId(final long userId) {
        final List<MetrikaToken> metrikaTokens = accessTokenService.findByUserId(userId);

        return getUnusedCounters(userId, metrikaTokens);
    }

    @Override
    @Transactional
    public List<CounterWithToken> findAllUnusedByUserIdAndTokenId(final long userId, final int tokenId) {
        List<MetrikaToken> metrikaTokens = accessTokenService.findByUserId(userId);
        metrikaTokens = filteredListById(metrikaTokens, tokenId);

        return getUnusedCounters(userId, metrikaTokens);
    }

    private List<MetrikaToken> filteredListById(final List<MetrikaToken> tokens, final int tokenId) {
        return tokens.stream().filter(token -> token.getId() == tokenId).collect(Collectors.toList());
    }

    private List<CounterWithToken> getUnusedCounters(final long userId, final List<MetrikaToken> metrikaTokens) {
        final List<CounterWithToken> availableCounterWithTokens = getAllCountersByTokens(metrikaTokens);
        final List<MetrikaCounter> usedCounters = findAll(userId);

        return getCountersFromUserCountersNotIncludedInUsedCounters(availableCounterWithTokens, usedCounters);
    }

    private List<CounterWithToken> getCountersFromUserCountersNotIncludedInUsedCounters(final List<CounterWithToken> counterWithTokens, final List<MetrikaCounter> metrikaCounters) {
        final List<CounterWithToken> resultCounterWithTokens = new ArrayList<>();

        for (final CounterWithToken counterWithToken : counterWithTokens) {
            if (metrikaCounters.stream().noneMatch(usedCounter ->
                    usedCounter.getExternalId() == counterWithToken.getExternalId())) {
                resultCounterWithTokens.add(counterWithToken);
            }
        }

        return resultCounterWithTokens;
    }

    private List<CounterWithToken> getAllCountersByTokens(final List<MetrikaToken> metrikaTokens) {
        final List<CounterWithToken> allCounterWithTokens = new ArrayList<>();

        for (final MetrikaToken metrikaToken : metrikaTokens) {
            final String token = metrikaToken.getToken();

            allCounterWithTokens.addAll(counterApiService.getCounters(token).stream()
                    .map(counter ->
                            CounterWithToken.builder()
                                    .tokenId(metrikaToken.getId())
                                    .name(counter.getName())
                                    .externalId(counter.getId())
                                    .tokenName(metrikaToken.getToken())
                                    .build())
                    .collect(Collectors.toList()
                    )
            );
        }

        return allCounterWithTokens;
    }

    @Override
    @Transactional
    public Optional<MetrikaCounter> findBySummaryId(final long summaryId) {
        return counterDao.findBySummaryId(summaryId);
    }

    @Override
    @Transactional
    public MetrikaCounter addCounterToProject(
            final long projectId,
            final int tokenId,
            final long externalCounterId
    ) {
        return counterDao.add(projectId,tokenId,externalCounterId);
    }
}
