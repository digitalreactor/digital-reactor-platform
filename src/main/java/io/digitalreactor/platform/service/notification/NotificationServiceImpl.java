package io.digitalreactor.platform.service.notification;

import freemarker.template.Configuration;
import io.digitalreactor.platform.service.notification.email.EmailNotification;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.Map;

@Service
public class NotificationServiceImpl implements NotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);
    private static final String TEMPLATES_PATH = "/email";

    private final JavaMailSender mailSender;
    private final Configuration cfg;

    @Value("${mail.address.sender}")
    private String mailAddressSender;

    public NotificationServiceImpl(final JavaMailSender mailSender) {
        this.mailSender = mailSender;
        cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(NotificationServiceImpl.class, TEMPLATES_PATH);
    }

    @Override
    public void sendEmailNotification(
            @Nonnull final EmailNotification notification,
            @Nonnull final String toAddress,
            @Nonnull final Map<String, String> params
    ) {
        try {
            sendEmail(toAddress, mailAddressSender, notification.getTitle(), prepareMessage(notification.getTemplate(), params));
            LOGGER.info(String.format(
                    "Email (%s, %s, %s, %s) was sent",
                    toAddress,
                    mailAddressSender,
                    notification.getTitle(),
                    notification.getTemplate()
            ));
        } catch (final Exception e) {
            LOGGER.error(String.format(
                    "Email (%s, %s, %s, %s) wasn't send",
                    toAddress,
                    mailAddressSender,
                    notification.getTitle(),
                    notification.getTemplate()
            ), e);
        }
    }

    @SneakyThrows
    private String prepareMessage(@Nonnull final String templateName, @Nonnull final Map<String, String> params) {
        final StringWriter writer = new StringWriter();
        cfg.getTemplate(templateName + ".html").process(params, writer);

        return writer.toString();
    }

    private void sendEmail(
            @Nonnull final String toAddress,
            @Nonnull final String fromAddress,
            @Nonnull final String subject,
            @Nonnull final String msgBody
    ) throws MessagingException {
        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false);
        mimeMessage.setContent(msgBody, "text/html; charset=UTF-8");
        helper.setTo(toAddress);
        mimeMessage.setFrom(fromAddress);
        mimeMessage.setSubject(subject);

        mailSender.send(mimeMessage);
    }
}
