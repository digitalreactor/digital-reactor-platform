package io.digitalreactor.platform.service.notification.email;

import lombok.Getter;

import javax.annotation.Nonnull;

@Getter
public enum EmailNotification {
    NEW_USER("Регистрация нового пользователя.", "new_user"),
    RESET_PASSWORD("Сброс пароля.", "reset_password");

    private final String title;
    private final String template;

    EmailNotification(@Nonnull final String title, @Nonnull final String template) {
        this.title = title;
        this.template = template;
    }
}
