package io.digitalreactor.platform.service.notification;

import io.digitalreactor.platform.service.notification.email.EmailNotification;

import java.util.Map;

public interface NotificationService {
    void sendEmailNotification(EmailNotification notification, String toAddress, Map<String, String> params);
}
