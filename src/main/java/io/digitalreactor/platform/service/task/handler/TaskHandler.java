package io.digitalreactor.platform.service.task.handler;

import io.digitalreactor.platform.service.task.entity.AbstractExecutableTask;

public interface TaskHandler {
    void handle(AbstractExecutableTask task);
}
