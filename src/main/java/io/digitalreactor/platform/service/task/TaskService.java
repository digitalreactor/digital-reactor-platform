package io.digitalreactor.platform.service.task;

import io.digitalreactor.platform.model.task.Task;
import io.digitalreactor.platform.model.task.TaskStatus;
import io.digitalreactor.platform.service.task.entity.AbstractExecutableTask;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

public interface TaskService {
    Task add(final int projectId, @Nonnull final AbstractExecutableTask task);

    Optional<Task> findLastGlobalTaskByProjectId(int projectId);

    List<Task> findAllNewTasks();

    void changeStatus(int id, TaskStatus error);
}
