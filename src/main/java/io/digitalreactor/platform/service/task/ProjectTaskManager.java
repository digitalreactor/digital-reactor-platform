package io.digitalreactor.platform.service.task;

import io.digitalreactor.platform.model.task.Task;
import io.digitalreactor.platform.service.task.entity.AbstractExecutableTask;

public interface ProjectTaskManager {
    Task createTask(int projectId, AbstractExecutableTask task);
    void handleTasks();
}
