package io.digitalreactor.platform.service.task.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.digitalreactor.platform.model.task.TaskScope;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BasicSummaryTask.class, name = "basicSummaryTask")
})
public abstract class AbstractExecutableTask {
    public abstract TaskScope getScope();
}

