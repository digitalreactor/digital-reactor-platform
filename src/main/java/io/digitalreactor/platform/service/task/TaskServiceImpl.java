package io.digitalreactor.platform.service.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.platform.dao.TaskDao;
import io.digitalreactor.platform.model.task.Task;
import io.digitalreactor.platform.model.task.TaskStatus;
import io.digitalreactor.platform.service.project.ProjectService;
import io.digitalreactor.platform.service.task.entity.AbstractExecutableTask;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDateTime.now;

@Service
public class TaskServiceImpl implements TaskService {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TaskDao taskDao;
    private final ProjectService projectService;

    public TaskServiceImpl(final TaskDao taskDao, final ProjectService projectService) {
        this.taskDao = taskDao;
        this.projectService = projectService;
    }

    @Override
    public Task add(final int projectId, @Nonnull final AbstractExecutableTask task) {
        try {
            return taskDao.add(
                    projectId,
                    task.getClass().getName(),
                    objectMapper.writeValueAsString(task),
                    task.getScope()
            );
        } catch (final JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Task> findLastGlobalTaskByProjectId(final int projectId) {
        return Optional.ofNullable(Task.builder()
                .startDate(now())
                .status(TaskStatus.COMPLETED)
                .build()
        );
    }

    @Override
    public List<Task> findAllNewTasks() {
        return null;
    }

    @Override
    public void changeStatus(final int id, final TaskStatus error) {

    }
}
