package io.digitalreactor.platform.service.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.platform.model.task.Task;
import io.digitalreactor.platform.service.task.entity.AbstractExecutableTask;
import io.digitalreactor.platform.service.task.entity.BasicSummaryTask;
import io.digitalreactor.platform.service.task.handler.BasicSummaryHandler;
import io.digitalreactor.platform.service.task.handler.TaskHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.digitalreactor.platform.model.task.TaskStatus.ERROR;

@Service
public class ProjectTaskManagerImpl implements ProjectTaskManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectTaskManagerImpl.class);
    private final Map<Class<?>, TaskHandler> handlers = new ConcurrentHashMap<>();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final TaskService taskService;

    public ProjectTaskManagerImpl(final TaskService taskService) {
        this.taskService = taskService;
        handlers.put(BasicSummaryTask.class, new BasicSummaryHandler());

    }

    public void execute(@Nonnull final AbstractExecutableTask task) {
        handlers.get(task.getClass()).handle(task);
    }

    @Override
    @Transactional
    public Task createTask(final int projectId, @Nonnull final AbstractExecutableTask task) {
        LOGGER.info("Create task: {} for projectId: {}", task.getClass(), projectId);

        return null;

    }

    @Override
    public void handleTasks() {
        //TODO[St.maxim] transform int that service.
        final List<Task> tasks = taskService.findAllNewTasks();
        for (final Task task : tasks) {
            try {
                final AbstractExecutableTask abstractTask = new ObjectMapper().readValue(task.getPayload(), AbstractExecutableTask.class);
                final TaskHandler taskHandler = handlers.get(abstractTask);
                if(taskHandler == null) {
                    //TODO
                    break;
                }
                taskHandler.handle(abstractTask);
            } catch (final IOException e) {
                taskService.changeStatus(task.getId(), ERROR);
                LOGGER.error(String.format("Can't deserialize taskId: %s.", task.getId()), e);
            }
        }
    }
}
