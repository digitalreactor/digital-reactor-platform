package io.digitalreactor.platform.service.registration;

import com.google.common.collect.ImmutableMap;
import io.digitalreactor.analytic.metrika.service.summary.SummaryService;
import io.digitalreactor.platform.dao.CounterDao;
import io.digitalreactor.platform.dao.ProjectDao;
import io.digitalreactor.platform.dao.UserDao;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.notification.NotificationService;
import io.digitalreactor.platform.service.notification.email.EmailNotification;
import io.digitalreactor.platform.service.token.AccessTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Map;
import java.util.Optional;

import static io.digitalreactor.platform.service.notification.email.EmailNotification.NEW_USER;
import static io.digitalreactor.platform.service.notification.email.EmailNotification.RESET_PASSWORD;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationServiceImpl.class);
    private final SecureRandom random = new SecureRandom();
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private final UserDao userDao;
    private final ProjectDao projectDao;
    private final CounterDao counterDao;
    private final AccessTokenService accessTokenService;
    private final NotificationService notificationService;
    private final SummaryService summaryService;

    public RegistrationServiceImpl(
            final UserDao userDao,
            final ProjectDao projectDao,
            final CounterDao counterDao,
            final AccessTokenService accessTokenService,
            final NotificationService notificationService,
            final SummaryService summaryService
    ) {
        this.userDao = userDao;
        this.projectDao = projectDao;
        this.counterDao = counterDao;
        this.accessTokenService = accessTokenService;
        this.notificationService = notificationService;
        this.summaryService = summaryService;
    }

    @Override
    @Transactional
    public void register(
            @Nonnull final String email,
            @Nonnull final String token,
            final long counterId,
            @Nonnull final String projectName
    ) {
        LOGGER.info("Beginning of registration for email: {}.", email);
        final String password = generatePassword();
        final String hashingPassword = passwordEncoder.encode(password);
        final User user = userDao.register(email, hashingPassword);
        final Project project = projectDao.add(user.getId(), projectName);
        final int tokenId = accessTokenService.save(user.getId(), token);
        counterDao.add(project.getId(), tokenId, counterId);

        sendEmail(email, password, NEW_USER);

        //projectTaskManager.createTask(project.getId(), new BasicSummaryTask());
        summaryService.makeSummary(project.getId());
        LOGGER.info("Registration was finished for email: {}.", email);
    }

    @Override
    @Transactional
    public boolean verifyEmail(final String email) {
        LOGGER.info("Verify email: {}.", email);

        return !userDao.findByEmail(email).isPresent();
    }

    @Transactional
    @Override
    public boolean resetPassword(final String email) {
        final Optional<User> userOptional = userDao.findByEmail(email);
        if (userOptional.isPresent()) {
            LOGGER.info("Resetting password for email: {}.", email);

            final String password = generatePassword();
            final String hashingPassword = passwordEncoder.encode(password);

            if (userDao.resetPassword(email, hashingPassword)) {
                sendEmail(email, password, RESET_PASSWORD);
                LOGGER.info("Resetting password was finished for email: {}.", email);
                return true;
            } else {
                LOGGER.error("Can't reset password for user {}.", email);
                return false;
            }
        } else {
            LOGGER.error("Can't find user {}.", email);
            return false;
        }
    }

    private String generatePassword() {
        return new BigInteger(43, random).toString(32);
    }

    private void sendEmail(final String email, final String password, final EmailNotification emailNotification) {
        final Map<String, String> emailParams = ImmutableMap.<String, String>builder().
                put("email", email).
                put("password", password).
                build();
        notificationService.sendEmailNotification(emailNotification, email, emailParams);
    }
}
