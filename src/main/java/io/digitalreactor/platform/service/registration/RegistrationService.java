package io.digitalreactor.platform.service.registration;

public interface RegistrationService {
    void register(String email, String token, long counterId, String projectName);
    /**
     * This method used for checking email format and existing
     *
     * @return true if email correct and doesn't exist.
     */
    boolean verifyEmail(String email);

    boolean resetPassword(String email);
}
