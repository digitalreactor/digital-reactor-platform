package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.project.Project;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ProjectDao {
    private static final RowMapper<Project> PROJECT_MAPPER = (rs, rowNum) -> Project.builder()
            .id(rs.getInt("id"))
            .name(rs.getString("name"))
            .userId(rs.getInt("user_id"))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public ProjectDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Project add(final long userId, @Nonnull final String projectName) {
        final Integer projectId = jdbcTemplate.queryForObject(
                "INSERT INTO projects (user_id, name) VALUES (?, ?) RETURNING id",
                new Object[]{userId, projectName},
                Integer.class
        );

        return Project.builder()
                .id(projectId)
                .userId(userId)
                .name(projectName)
                .build();
    }

    public List<Project> findAllByUserId(final long userId) {
        return jdbcTemplate.query(
                "SELECT * FROM projects WHERE user_id = ?",
                new Object[]{userId},
                PROJECT_MAPPER
        );
    }

    public Optional<Project> findByProjectId(final long projectId) {
        final Project project = singleResult(jdbcTemplate.query(
                "SELECT * FROM projects WHERE id = ?",
                new Object[]{projectId},
                PROJECT_MAPPER));

        return Optional.ofNullable(project);
    }

    public Optional<Project> findById(final long projectId) {
        final Project project = singleResult(jdbcTemplate.query("SELECT * FROM projects WHERE id = ?", new Object[]{projectId}, PROJECT_MAPPER));

        return Optional.ofNullable(project);
    }
}
