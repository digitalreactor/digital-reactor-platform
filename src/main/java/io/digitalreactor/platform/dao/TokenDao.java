package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.model.metrika.MetrikaTokenStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class TokenDao {
    private static final RowMapper<MetrikaToken> TOKEN_MAPPER = (rs, rowNum) -> MetrikaToken.builder()
            .id(rs.getInt("id"))
            .userId(rs.getInt("user_id"))
            .token(rs.getString("token"))
            .dateAdded(rs.getTimestamp("date_added").toLocalDateTime())
            .status(MetrikaTokenStatus.getStatus(rs.getString("status")))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public TokenDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public MetrikaToken add(final long userId, @Nonnull final String token, @Nonnull final MetrikaTokenStatus status) {
        final Integer tokenId = jdbcTemplate.queryForObject(
                "INSERT INTO metrika_tokens (token, date_added, user_id, status) VALUES (?, ?, ?, CAST(? as metrika_token_status)) RETURNING id",
                new Object[]{token, Timestamp.valueOf(now()), userId , status.name()},
                Integer.class
        );

        return MetrikaToken.builder()
                .id(tokenId)
                .userId(userId)
                .token(token)
                .status(status)
                .build();
    }

    public Optional<MetrikaToken> findById(final int tokeId) {
        final MetrikaToken metrikaToken  = singleResult(jdbcTemplate.query(
                "SELECT * FROM metrika_tokens WHERE id = ?",
                new Object[]{tokeId},
                TOKEN_MAPPER
        ));

        return Optional.ofNullable(metrikaToken);
    }

    public List<MetrikaToken> findByUserId(final long userId) {
        return jdbcTemplate.query(
                "SELECT * FROM metrika_tokens WHERE user_id = ?",
                new Object[]{userId},
                TOKEN_MAPPER
        );
    }
}
