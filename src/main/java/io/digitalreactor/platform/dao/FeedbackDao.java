package io.digitalreactor.platform.dao;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
@AllArgsConstructor
public class FeedbackDao {
    private final JdbcTemplate jdbcTemplate;

    public boolean save(@NonNull final String user, @NonNull final String message) {
        return 1 == jdbcTemplate.update("INSERT INTO feedbacks (user_name, message) VALUES (?, ?)", user, message);
    }
}
