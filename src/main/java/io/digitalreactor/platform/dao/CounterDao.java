package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CounterDao {
    private static final RowMapper<MetrikaCounter> COUNTER_MAPPER = (rs, rowNum) -> MetrikaCounter.builder()
            .id(rs.getInt("id"))
            .externalId(rs.getLong("external_id"))
            .projectId(rs.getInt("project_id"))
            .tokenId(rs.getInt("token_id"))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public CounterDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public MetrikaCounter add(final long projectId, final int tokenId, final long externalCounterId) {
        final Integer counterId = jdbcTemplate.queryForObject(
                "INSERT INTO metrika_counters (project_id, external_id, token_id) VALUES (?, ?, ?) RETURNING id",
                new Object[]{projectId, externalCounterId, tokenId},
                Integer.class
        );

        return MetrikaCounter.builder()
                .id(counterId)
                .tokenId(tokenId)
                .projectId(projectId)
                .externalId(externalCounterId)
                .build();
    }

    public Optional<MetrikaCounter> findByProjectId(final long projectId) {
        final MetrikaCounter metrikaCounter = singleResult(jdbcTemplate.query(
                "SELECT * FROM metrika_counters WHERE project_id = ?",
                new Object[]{projectId},
                COUNTER_MAPPER
        ));

        return Optional.ofNullable(metrikaCounter);
    }

    public List<MetrikaCounter> findAll(final long userId) {
        return jdbcTemplate.query(
                "SELECT metrika_counters.* FROM projects,metrika_counters " +
                        "WHERE metrika_counters.project_id = projects.id and projects.user_id = ?",
                new Object[]{userId},
                COUNTER_MAPPER);
    }

    public Optional<MetrikaCounter> findBySummaryId(final long summaryId) {
        final MetrikaCounter metrikaCounter = singleResult(jdbcTemplate.query(
                "SELECT * FROM metrika_counters INNER JOIN projects ON metrika_counters.project_id =  projects.id" +
                        " INNER JOIN summaries ON projects.id = summaries.project_id WHERE summaries.id = ?",
                new Object[]{summaryId},
                COUNTER_MAPPER
        ));
        return Optional.ofNullable(metrikaCounter);
    }
}
