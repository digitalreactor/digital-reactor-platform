package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.task.Task;
import io.digitalreactor.platform.model.task.TaskScope;
import io.digitalreactor.platform.model.task.TaskStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static io.digitalreactor.platform.model.task.TaskStatus.NEW;
import static java.time.LocalDateTime.now;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class TaskDao {
    private static final RowMapper<Task> TASK_MAPPER = (rs, rowNum) -> Task.builder()
            .id(rs.getInt("id"))
            .projectId(rs.getInt("project_id"))
            .type(rs.getString("task_type"))
            .payload(rs.getString("payload"))
            .startDate(rs.getTimestamp("date_added").toLocalDateTime())
            .status(TaskStatus.getStatus(rs.getString("status")))
            .scope(TaskScope.getScope(rs.getString("scope")))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public TaskDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Task add(
            final int projectId,
            @Nonnull final String type,
            @Nonnull final String payload,
            @Nonnull final TaskScope scope
    ) {
        final LocalDateTime current = now();
        final Integer taskId = jdbcTemplate.queryForObject(
                "INSERT INTO project_tasks (project_id, task_type, payload, status, start_date, scope) VALUES (?, ?, ?, CAST(? AS task_status), ?, CAST(? AS task_scope)) RETURNING id",
                new Object[]{projectId, type, payload, NEW.name(), Timestamp.valueOf(current), scope.name()},
                Integer.class
        );

        return Task.builder()
                .id(taskId)
                .projectId(projectId)
                .type(type)
                .payload(payload)
                .status(NEW)
                .startDate(current)
                .scope(scope)
                .build();
    }
}
