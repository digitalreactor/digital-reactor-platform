package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.User;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static org.springframework.dao.support.DataAccessUtils.singleResult;

@AllArgsConstructor
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserDao {
    private static final RowMapper<User> USER_MAPPER = (rs, rowNum) -> User.builder()
            .id(rs.getInt("id"))
            .registrationDate(rs.getTimestamp("reg_date").toLocalDateTime())
            .password(rs.getString("password"))
            .email(rs.getString("email"))
            .build();


    private final JdbcTemplate jdbcTemplate;

    public User register(@Nonnull final String email, @Nonnull final String password) {
        final LocalDateTime registrationDate = now();
        final Integer userId = jdbcTemplate.queryForObject(
                "INSERT INTO users (email, password, reg_date) VALUES (?, ?, ?) RETURNING id",
                new Object[]{email, password, Timestamp.valueOf(registrationDate)},
                Integer.class
        );

        return User.builder()
                .id(userId)
                .email(email)
                .password(password)
                .registrationDate(registrationDate)
                .build();
    }

    public boolean resetPassword(@Nonnull final String email,@Nonnull final String newPassword) {
        return 1 == jdbcTemplate.update(
                "UPDATE users SET password = ? WHERE email = ? ",
                newPassword,
                email
        );
    }

    public Optional<User> findByEmail(@Nonnull final String email) {
        final User user = singleResult(
                jdbcTemplate.query(
                        "SELECT * FROM users WHERE email = ?",
                        new Object[]{email},
                        USER_MAPPER
                )
        );

        return Optional.ofNullable(user);
    }
}
