package io.digitalreactor.platform;

public interface Constants {
    String SPRING_PROFILE_TEST = "test";
    String SPRING_PROFILE_PROD = "prod";
    String SPRING_PROFILE_DEV = "dev";
}
