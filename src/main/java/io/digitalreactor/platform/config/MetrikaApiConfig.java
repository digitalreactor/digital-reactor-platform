package io.digitalreactor.platform.config;

import io.digitalreactor.vendor.yandex.metrika.serivce.CounterApiService;
import io.digitalreactor.vendor.yandex.oauth.TokenResolveService;
import io.digitalreactor.vendor.yandex.oauth.TokenResolveServiceImpl;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetrikaApiConfig {
    @Value("${vendor.yandex.oauth.url}")
    private String oauthUrl;
    @Value("${vendor.yandex.api.url}")
    private String apiUrl;
    @Value("${vendor.yandex.api.client.secret}")
    private String clientSecret;
    @Value("${vendor.yandex.api.application.id}")
    private String applicationId;
    @Value("${vendor.yandex.api.application.auth}")
    private String applicationAuth;

    @Bean
    public CounterApiService counterApiService(final HttpClient httpClient) {
        return new CounterApiService(httpClient, apiUrl, applicationAuth);
    }

    @Bean
    public TokenResolveService tokenResolveService(final HttpClient httpClient) {
        return new TokenResolveServiceImpl(applicationId, applicationAuth, clientSecret, oauthUrl, httpClient);
    }

}
