package io.digitalreactor.platform.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfig {
    @Value("${mail.server}")
    private String mailServer;
    @Value("${mail.port}")
    private Integer mailPort;
    @Value("${mail.ssl}")
    private Boolean mailSsl;
    @Value("${mail.username}")
    private String mailUser;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.address.sender}")
    private String mailAddressSender;

    @Bean
    public JavaMailSenderImpl javaMailSenderImpl() {
        final JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(mailServer);
        javaMailSender.setPort(mailPort);
        final Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.smtp.auth", "true");
        javaMailProperties.setProperty("mail.smtp.ssl.enable", String.valueOf(mailSsl));

        javaMailSender.setJavaMailProperties(javaMailProperties);

        javaMailSender.setUsername(mailUser);
        javaMailSender.setPassword(mailPassword);

        return javaMailSender;
    }
}
