package io.digitalreactor.platform.model.project;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Project {
    private final long id;
    private final long userId;
    private final String name;
}
