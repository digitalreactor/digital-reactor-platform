package io.digitalreactor.platform.model.task;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Task {
    private int id;
    private int projectId;
    private String type;
    private String payload;
    private TaskStatus status;
    private TaskScope scope;
    private LocalDateTime startDate;
    private int revisionNumber;
}
