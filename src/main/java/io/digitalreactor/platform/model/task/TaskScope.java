package io.digitalreactor.platform.model.task;

import javax.annotation.Nonnull;

public enum TaskScope {
    ALL, PARTIAL;

    public static TaskScope getScope(@Nonnull final String scope) {
        if (ALL.name().equals(scope)) {
            return ALL;
        } else if (PARTIAL.name().equals(scope)) {
            return PARTIAL;
        }

        throw new RuntimeException("Invalid scope: " + scope);
    }
}
