package io.digitalreactor.platform.model.task;

import javax.annotation.Nonnull;

public enum TaskStatus {
    NEW, RUNNING, COMPLETED, ERROR, INTERRUPTED, WAITING;

    public static TaskStatus getStatus(@Nonnull final String status) {
        if (NEW.name().equals(status)) {
            return NEW;
        } else if (RUNNING.name().equals(status)) {
            return RUNNING;
        } else if (COMPLETED.name().equals(status)) {
            return COMPLETED;
        } else if (ERROR.name().equals(status)) {
            return ERROR;
        } else if (INTERRUPTED.name().equals(status)) {
            return INTERRUPTED;
        } else if (WAITING.name().equals(status)) {
            return WAITING;
        }

        throw new RuntimeException("Invalid status: " + status);
    }
}
