package io.digitalreactor.platform.model.metrika;

import javax.annotation.Nonnull;

public enum MetrikaTokenStatus {
    ACTIVE, EXPIRED;

    public static MetrikaTokenStatus getStatus(@Nonnull final String status) {
        if (ACTIVE.name().equals(status)) {
            return ACTIVE;
        } else if (EXPIRED.name().equals(status)) {
            return EXPIRED;
        }

        throw new RuntimeException("Invalid status: " + status);
    }
}
