package io.digitalreactor.platform.model.metrika;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MetrikaCounter {
    private final int id;
    private final long projectId;
    private final int tokenId;
    private final long externalId;
}
