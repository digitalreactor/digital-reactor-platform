package io.digitalreactor.platform.model.metrika;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class MetrikaToken {
    private final int id;
    private final long userId;
    private final String token;
    private final LocalDateTime dateAdded;
    private final MetrikaTokenStatus status;
}
