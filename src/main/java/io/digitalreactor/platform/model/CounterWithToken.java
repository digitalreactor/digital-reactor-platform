package io.digitalreactor.platform.model;

import lombok.Builder;
import lombok.Data;

/**
 * digital-reactor
 * Created by igor on 13.04.17.
 */

@Data
@Builder
public class CounterWithToken {
    private final int tokenId;
    private final String name;
    private final long externalId;
    private final String tokenName;
}
