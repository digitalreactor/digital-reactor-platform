package io.digitalreactor.platform.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebConfig.class);

}
