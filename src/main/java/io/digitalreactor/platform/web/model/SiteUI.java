package io.digitalreactor.platform.web.model;

/**
 * Created by MStepachev on 12.09.2016.
 */
public class SiteUI {
    private final String name;
    private final String id;

    public SiteUI(final String name, final String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
