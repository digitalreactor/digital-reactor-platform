package io.digitalreactor.platform.web.model.report;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisitUI {
    private int number;
    private LocalDate date;
    private String dayType;
}
