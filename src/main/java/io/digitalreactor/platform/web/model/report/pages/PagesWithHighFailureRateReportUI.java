package io.digitalreactor.platform.web.model.report.pages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PagesWithHighFailureRateReportUI {
    private final String type = "PagesWithHighFailureRateReport";
    private String samplingInterval;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<PageUI> metrics;
}
