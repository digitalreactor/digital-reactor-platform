package io.digitalreactor.platform.web.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CounterUI {
    private String name;
    private long counterId;
}
