package io.digitalreactor.platform.web.model.report.sources;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReferringSourceReportUI {
    private final String type = "ReferringSourceReport";
    private String samplingInterval;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<ReferringSourceUI> sources;
}
