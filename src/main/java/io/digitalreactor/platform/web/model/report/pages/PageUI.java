package io.digitalreactor.platform.web.model.report.pages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageUI {
    private String siteName;
    private String favicon;
    private int visits;
    private double bounceRate;
    private int pageViews;
    private double avgVisitDurationSeconds;
    private double conversion;
}
