package io.digitalreactor.platform.web.model.report.sources;

import io.digitalreactor.platform.web.model.report.VisitUI;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReferringSourceUI {
    private String name;
    private List<VisitUI> metrics;
    private int totalVisits;
    private double totalVisitsChangePercent;
    private int totalGoalVisits;
    private double totalGoalVisitsChangePercent;
    private double conversion;
    private double conversionChangePercent;
}
