package io.digitalreactor.platform.web.model;


/**
 * Created by MStepachev on 12.09.2016.
 */
public enum SummaryStatusEnum {
    UNKNOWN, ERROR, LOADING, DONE
}
