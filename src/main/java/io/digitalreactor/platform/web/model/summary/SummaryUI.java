package io.digitalreactor.platform.web.model.summary;

import io.digitalreactor.platform.web.model.GoalUI;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.List;

@Data
@Builder
public class SummaryUI {
    private List<GoalUI> goals;
    private String project;
    private String date;
}
