package io.digitalreactor.platform.web.model.report;

/**
 * Created by ingvard on 07.04.16.
 */
public enum ReportTypeEnum {
    VISITS,
    REFERRING_SOURCE,
    SEARCH_PHRASE_YANDEX_DIRECT,
    HIGH_FAILURE_RATE
}
