package io.digitalreactor.platform.web.model.summary;

import io.digitalreactor.analytic.metrika.model.summary.SummaryStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SummaryInfoUI {
    private long id;
    private SummaryStatus status;
    private String date;
}
