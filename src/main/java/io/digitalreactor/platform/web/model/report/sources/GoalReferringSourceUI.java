package io.digitalreactor.platform.web.model.report.sources;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Data
@Builder

@AllArgsConstructor
@NoArgsConstructor
public class GoalReferringSourceUI {
    private String name;
    private List<ReferringSourceUI> sources;
    private double conversion;
    private double conversionChange;
    private int numberOfCompletedGoal;
}
