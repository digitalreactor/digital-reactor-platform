package io.digitalreactor.platform.web.model.report.phrases;

import lombok.*;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SearchPhraseUI {
    private String searchPhrase;
    private int visits;
    private double bounceRate;
    private double pageDepth;
    private double avgVisitDurationSeconds;
    private double quality;
}
