package io.digitalreactor.platform.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by MStepachev on 12.09.2016.
 */

public class SummaryStatusUI {
    private final String status;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd.MM.yyyy")
    private final Date date;
    private final String taskId;

    public SummaryStatusUI(final String status, final Date date, final String taskId) {
        this.status = status;
        this.date = date;
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public Date getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }
}
