package io.digitalreactor.platform.web.model;

/**
 * Created by MStepachev on 08.09.2016.
 */
public class EmailCheckUI {
    private String email;

    public EmailCheckUI() {
    }

    public EmailCheckUI(final String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }
}
