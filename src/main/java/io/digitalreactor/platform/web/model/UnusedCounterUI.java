package io.digitalreactor.platform.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

/**
 * Created by igor on 21.03.17.
 */

@Data
@Builder
public class UnusedCounterUI {
    private String name;
    @JsonIgnore
    private String tokenName;
    private int tokenId;
    private long externalId;
}
