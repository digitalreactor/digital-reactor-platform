package io.digitalreactor.platform.web.model;

import lombok.Data;

@Data
public class SuccessUI {
    private final String status;
}
