package io.digitalreactor.platform.web.model.report.visits;

import io.digitalreactor.platform.web.model.report.VisitUI;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisitsForTimeIntervalReportUI {
    private final String type = "VisitsForTimeIntervalReport";
    private String samplingInterval;
    private String trend;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<VisitUI> metrics;
    private int visitChange;
    private double visitChangePercent;
    private double conversionChange;
    private double conversionChangePercent;
    private String reason;
}
