package io.digitalreactor.platform.web.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * digital-reactor
 * Created by igor on 30.04.17.
 */

@Data
@Builder
@EqualsAndHashCode
public class GoalUI {
    private String name;
    private long id;
}
