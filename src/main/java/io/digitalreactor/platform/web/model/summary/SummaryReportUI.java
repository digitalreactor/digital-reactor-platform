package io.digitalreactor.platform.web.model.summary;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class SummaryReportUI {
    private final List<Long> reportIds = new ArrayList<>();
    private String reportType;

    public SummaryReportUI addReportId(final long reportId) {
        reportIds.add(reportId);

        return this;
    }
}
