package io.digitalreactor.platform.web.model;

/**
 * Created by MStepachev on 11.10.2016.
 */
public class ShortUserInfoUI {
    private final String name;

    public ShortUserInfoUI(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
