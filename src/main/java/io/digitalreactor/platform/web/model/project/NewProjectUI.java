package io.digitalreactor.platform.web.model.project;

import lombok.Builder;
import lombok.Data;

/**
 * Created by igor on 21.03.17.
 */

@Data
@Builder
public class NewProjectUI {
    private int tokenId;
    private long externalCounterId;
    private String name;
}
