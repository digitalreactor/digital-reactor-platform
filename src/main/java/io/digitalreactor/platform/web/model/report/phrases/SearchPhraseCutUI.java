package io.digitalreactor.platform.web.model.report.phrases;

import lombok.*;

import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SearchPhraseCutUI {
    private String goalName;
    private String type;
    private List<SearchPhraseUI> successPhrases;
    private List<SearchPhraseUI> badPhrases;
}
