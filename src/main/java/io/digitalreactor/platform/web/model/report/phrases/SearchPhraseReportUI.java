package io.digitalreactor.platform.web.model.report.phrases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SearchPhraseReportUI {
    private final String type = "SearchPhraseReport";
    private String samplingInterval;
    private LocalDate startDate;
    private LocalDate endDate;

    private String goalName;
    private long goalId;
    private List<SearchPhraseUI> successPhrases;
    private List<SearchPhraseUI> badPhrases;
}
