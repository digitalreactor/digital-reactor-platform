package io.digitalreactor.platform.web.model.project;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class ProjectUI {
    private long id;
    private String name;
}
