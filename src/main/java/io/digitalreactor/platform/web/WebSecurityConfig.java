package io.digitalreactor.platform.web;

import io.digitalreactor.platform.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_DEV;
import static io.digitalreactor.platform.Constants.SPRING_PROFILE_PROD;

@Configuration
@EnableWebSecurity
@Profile(value = {SPRING_PROFILE_PROD, SPRING_PROFILE_DEV})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Autowired
    private Environment environment;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        final boolean isDevEnv = Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> env.equalsIgnoreCase("dev"));

        http.authorizeRequests()
                .antMatchers(
                        "/configuration/**",
                        "/feedback/**",
                        "/accounts/**",
                        "/oauth/yandex/**",
                        "/registration.html",
                        "/static/**",
                        "/registration/**",
                        "/index.html",
                        "/"
                ).permitAll()
                .anyRequest().authenticated();

        http.formLogin()
                .loginPage("/login.html")
                .permitAll().defaultSuccessUrl("/");

        http.logout()
                .permitAll();

        http.csrf().disable();

        // http.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);
    }

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }
}
