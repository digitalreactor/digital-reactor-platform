package io.digitalreactor.platform.web.controller;

import io.digitalreactor.platform.service.registration.RegistrationService;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.platform.web.model.CounterUI;
import io.digitalreactor.platform.web.model.EmailCheckUI;
import io.digitalreactor.platform.web.model.NewAccountUI;
import io.digitalreactor.platform.web.model.SuccessUI;
import io.digitalreactor.vendor.yandex.metrika.model.Counter;
import io.digitalreactor.vendor.yandex.metrika.serivce.CounterApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "registration")
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private CounterApiService counterApiService;

    //TODO[St.maxim] возможно это стоит перенести в апи связанное с counters
    @RequestMapping(value = "/counters/{syntheticToken}", method = RequestMethod.GET)
    private List<CounterUI> getCountersBySyntheticToken(@PathVariable(name = "syntheticToken") final String syntheticToken) {
        LOGGER.info("Try to get counters by synthetic token: {}", syntheticToken);
        final String token = accessTokenService.peekOriginalToken(syntheticToken);

        return mapper(counterApiService.getCounters(token));
    }

    private List<CounterUI> mapper(final List<Counter> counters) {
        return counters.stream()
                .map(counter -> CounterUI.builder()
                        .name(counter.getName())
                        .counterId(counter.getId())
                        .build()
                ).collect(toList());
    }

    @RequestMapping(value = "account", method = RequestMethod.POST)
    public SuccessUI createNewAccount(@RequestBody final NewAccountUI newAccountUI) {
        final String token = accessTokenService.popOriginalToken(newAccountUI.getSessionId());

        registrationService.register(
                newAccountUI.getEmail(),
                token,
                newAccountUI.getCounterId(),
                newAccountUI.getName()
        );

        return new SuccessUI("OK");
    }

    @RequestMapping(value = "/check/email", method = RequestMethod.POST)
    public boolean checkEmail(@RequestBody final EmailCheckUI email) {
        return registrationService.verifyEmail(email.getEmail());
    }

/*
    @Autowired
    private TemporalTokenStorage tokenStorage;

    @Autowired
    private GrantCodeToTokenResolver tokenResolver;

    @Autowired
    private CounterApiService counterApiService;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = ACTIVATE_CODE_PATH, method = RequestMethod.GET)
    @Override
    public ModelAndView activateRegistrationSession(@RequestParam Long code) {
        String token = tokenResolver.getTokenByGrantCode(code);
        String sessionId = tokenStorage.store(token);

        return new ModelAndView(String.format(REDIRECT_PATH, sessionId));
    }

    @RequestMapping(value = COUNTERS_PATH, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public List<CounterUI> getCountersBySessionId(@PathVariable String sessionId) {
        String token = tokenStorage.get(sessionId);
        List<CounterWithToken> counters = counterApiService.getCounters(token);

        //TODO[St.maxim] use mapper
        return counters.stream()
                .map(counter -> new CounterUI(counter.getName(), String.valueOf(counter.getId())))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = NEW_ACCOUNT_PATH, method = RequestMethod.POST)
    @ResponseBody
    @Override
    public Boolean createNewAccount(@RequestBody NewAccountUI newAccountUI) {
        String token = tokenStorage.poll(newAccountUI.getSessionId());

        return accountService.newAccount(
                newAccountUI.getEmail(),
                token,
                new CounterWithToken(newAccountUI.getCounterId(), newAccountUI.getName())
        );
    }*/
}
