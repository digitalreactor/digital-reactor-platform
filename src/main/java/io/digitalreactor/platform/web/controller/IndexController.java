package io.digitalreactor.platform.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping(value = {
        "/",
        "/projects/**",
        "/about/**"
})
public class IndexController {
    @RequestMapping
    public ModelAndView welcome(final Principal principal) {
        if (principal == null) {
            return new ModelAndView("welcome.html");
        } else {
            return new ModelAndView("/index.html");
        }
    }
}
