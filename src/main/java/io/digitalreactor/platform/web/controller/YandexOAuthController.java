package io.digitalreactor.platform.web.controller;

import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.platform.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/oauth/yandex")
public class YandexOAuthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(YandexOAuthController.class);
    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private UserService userService;

    @RequestMapping("grant_code")
    public void grantCodeResolver(
            @RequestParam(name = "code", required = true) final int code,
            @RequestParam(name = "state", required = true) final String state,
            final HttpServletResponse response
    ) throws IOException {
        LOGGER.info("Try to resolve code: {} for state: {}", code, state);
        final String syntheticToken = accessTokenService.getSyntheticToken(code);
        //TODO[St.Maxim] It is bad hack.
        if ("ADD_PROJECT".equals(state)) {
            final User currentUser = userService.getCurrentUser();
            if(currentUser == null) {
                throw new RuntimeException("Invalid user.");
            }

            final String originalToken = accessTokenService.popOriginalToken(syntheticToken);
            accessTokenService.save(currentUser.getId(), originalToken);

            response.sendRedirect("/projects/new");
        } else {
            response.sendRedirect("/registration.html#sites/session/" + syntheticToken);
        }
    }
}
