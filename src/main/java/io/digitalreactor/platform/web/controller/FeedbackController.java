package io.digitalreactor.platform.web.controller;

import io.digitalreactor.platform.service.feedback.FeedbackService;
import io.digitalreactor.platform.web.model.FeedbackUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping(method = RequestMethod.POST)
    public void createFeedback(final Principal principal, @RequestBody final FeedbackUI feedbackUI) {
        final String user = principal == null ? "anonymous" : principal.getName();

        feedbackService.save(user, feedbackUI.getMessage());
    }
}
