package io.digitalreactor.platform.web.api;

import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.service.counter.CounterService;
import io.digitalreactor.platform.service.user.UserService;
import io.digitalreactor.platform.web.model.UnusedCounterUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * digital-reactor
 * Created by igor on 21.03.17.
 */

@RestController
@RequestMapping("api/counters")
public class CounterRestController {
    @Autowired
    private CounterService counterService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "unused", method = RequestMethod.GET)
    public List<UnusedCounterUI> findAllUnused(@RequestParam(value = "tokenId",required = false) final Integer tokenId) {
        final long userId = userService.getCurrentUser().getId();

        if (tokenId == null) {
            return mapCounterForUser(
                    counterService.findAllUnusedByUserId(userId)
            );
        } else {
            return mapCounterForUser(
                    counterService.findAllUnusedByUserIdAndTokenId(
                            userId,
                            tokenId
                    )
            );
        }
    }

    private List<UnusedCounterUI> mapCounterForUser(final List<CounterWithToken> counterWithTokens) {
        return counterWithTokens.stream()
                .map(counterWithToken ->
                        UnusedCounterUI.builder()
                                .externalId(counterWithToken.getExternalId())
                                .tokenId(counterWithToken.getTokenId())
                                .tokenName("Not Implement")
                                .name(counterWithToken.getName())
                                .build()
                )
                .collect(Collectors.toList());
    }
}
