package io.digitalreactor.platform.web.api.mappers.pages;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.object.Page;
import io.digitalreactor.platform.web.model.report.pages.PageUI;
import io.digitalreactor.platform.web.model.report.pages.PagesWithHighFailureRateReportUI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * digital-reactor
 * Created by igor on 21.04.17.
 */

@Mapper(componentModel = "spring")
public interface PageUIMapper {
    @Mapping(source = "sampling.signature",target = "samplingInterval")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "startDate", source = "sampling.startDate")
    @Mapping(target = "endDate", source = "sampling.endDate")
    PagesWithHighFailureRateReportUI mapReport(PagesWithHighFailureRateReport report);

    PageUI mapPage(Page page);
}
