package io.digitalreactor.platform.web.api.mappers.phrases;

import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhrase;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhraseCut;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseCutUI;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseReportUI;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseUI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * digital-reactor
 * Created by igor on 21.04.17.
 */

@Mapper(componentModel = "spring")
public interface SearchPhraseMapper {

    @Mapping(source = "sampling.signature", target = "samplingInterval")
    @Mapping(source = "reportGoal.goalName", target = "goalName")
    @Mapping(source = "reportGoal.goalId",target = "goalId")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "startDate", source = "sampling.startDate")
    @Mapping(target = "endDate", source = "sampling.endDate")
    SearchPhraseReportUI mapReport(SearchPhraseReport searchPhraseReport);

    SearchPhraseCutUI mapPhraseCut(SearchPhraseCut searchPhraseCut);

    SearchPhraseUI mapPhrase(SearchPhrase searchPhrase);
}
