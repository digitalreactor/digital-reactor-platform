package io.digitalreactor.platform.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("configuration")
public class ConfigurationRestController {
    @Autowired
    private Environment environment;

    @Value("${vendor.yandex.api.application.id}")
    private String applicationId;

    @RequestMapping(path = "application/id", method = RequestMethod.GET)
    public String getApplicationId() {
        if (Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> env.equalsIgnoreCase("test"))) {
            return "dev";
        } else {
            return applicationId;
        }
    }
}
