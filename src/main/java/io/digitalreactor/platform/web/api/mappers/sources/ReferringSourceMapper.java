package io.digitalreactor.platform.web.api.mappers.sources;

import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.object.GoalReferringSource;
import io.digitalreactor.analytic.metrika.common.report.object.ReferringSource;
import io.digitalreactor.analytic.metrika.common.report.object.Visit;
import io.digitalreactor.platform.web.model.report.VisitUI;
import io.digitalreactor.platform.web.model.report.sources.GoalReferringSourceUI;
import io.digitalreactor.platform.web.model.report.sources.ReferringSourceReportUI;
import io.digitalreactor.platform.web.model.report.sources.ReferringSourceUI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * digital-reactor
 * Created by igor on 22.04.17.
 */

@Mapper(componentModel = "spring")
public interface ReferringSourceMapper {
    @Mapping(source = "sampling.signature", target = "samplingInterval")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "startDate", source = "sampling.startDate")
    @Mapping(target = "endDate", source = "sampling.endDate")
    ReferringSourceReportUI mapReport(ReferringSourceReport referringSourceReport);

    GoalReferringSourceUI mapGoalReferringSource(GoalReferringSource goalReferringSource);

    @Mapping(target = "totalGoalVisits",
            source = "goalReferringSourceStatistic.totalGoalVisits")
    @Mapping(target = "totalGoalVisitsChangePercent",
            source = "goalReferringSourceStatistic.totalGoalVisitsChangePercent")
    @Mapping(target = "conversion",
            source = "goalReferringSourceStatistic.conversion")
    @Mapping(target = "conversionChangePercent",
            source = "goalReferringSourceStatistic.conversionChangePercent")
    ReferringSourceUI mapReferringSource(ReferringSource referringSource);

    VisitUI mapVisit(Visit visit);
}
