package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.enumeration.Interval;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.service.report.ReportService;
import io.digitalreactor.platform.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/reports")
public class ReportRestController {
    @Autowired
    private UserService userService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private ReportMapper reportMapper;

    @RequestMapping(value = "{reportId}", method = RequestMethod.GET)
    public Object getReport(@PathVariable("reportId") final long reportId) {
        final Report report = reportService.getReport(userService.getCurrentUser().getId(), reportId);

        return reportMapper.mapToUI(report);
    }

    @RequestMapping(value = "grouping", method = RequestMethod.GET)
    public List getReport(
            @RequestParam(value = "summary") final long summaryId,
            @RequestParam("types") final String[] types,
            @RequestParam("interval") final String stringInterval,
            @RequestParam(value = "goal", required = false, defaultValue = "0") final Long goalId
    ) {
        final Interval interval = Interval.valueOf(stringInterval);
        final long currentUserId = userService.getCurrentUser().getId();
        final List<Report> reports = new ArrayList<>();

        Arrays.stream(types)
                .forEach(
                        type ->
                                reports.addAll(reportService.getReports(currentUserId, summaryId, type))
                );

        return filterReportsWithIntervalAndGoal(reports, interval, goalId);
    }

    private List filterReportsWithIntervalAndGoal(
            final List<Report> reports,
            final Interval interval,
            final Long goalId
    ) {
        return reports
                .stream()
                .filter(
                        report ->
                                report.getPayload().getSampling().getInterval() == interval &&
                                        report.getPayload().getReportGoal().getGoalId() == goalId
                )
                .map(
                        report -> reportMapper.mapToUI(report)
                )
                .collect(Collectors.toList());
    }
}
