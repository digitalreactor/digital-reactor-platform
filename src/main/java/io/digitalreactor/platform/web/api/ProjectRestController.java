package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.service.summary.SummaryService;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.counter.CounterService;
import io.digitalreactor.platform.service.project.ProjectService;
import io.digitalreactor.platform.service.user.UserService;
import io.digitalreactor.platform.web.exception.ResourceNotFoundException;
import io.digitalreactor.platform.web.model.project.NewProjectUI;
import io.digitalreactor.platform.web.model.project.ProjectUI;
import io.digitalreactor.platform.web.model.summary.SummaryInfoUI;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("api/projects")
public class ProjectRestController {
    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private SummaryService summaryService;

    @Autowired
    private CounterService counterService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectUI> getProjects() {
        final List<Project> projects = projectService.findAllByUserId(userService.getCurrentUser().getId());

        return map(projects);
    }

    @RequestMapping(path = "{projectId}", method = RequestMethod.GET)
    public ProjectUI getProject(@PathVariable(name = "projectId") final long projectId) {
        final Optional<Project> projectOpt = projectService.findByProjectId(projectId);

        return projectOpt
                .map(this::map)
                .orElseThrow(ResourceNotFoundException::new);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProjectUI> addProject(@RequestBody final NewProjectUI newProjectUI) {
        final long currentUserId = userService.getCurrentUser().getId();

        if (projectService.counterIdBelongsUser(currentUserId, newProjectUI.getExternalCounterId())) {
            final Project insertedProject = projectService.addProject(currentUserId, newProjectUI.getName());
            counterService.addCounterToProject(insertedProject.getId(),newProjectUI.getTokenId(),newProjectUI.getExternalCounterId());
            summaryService.makeSummary(insertedProject.getId());

            return ResponseEntity.ok(map(insertedProject));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(value = "{projectId}/summaries", method = RequestMethod.GET)
    public List<SummaryInfoUI> summariesInfo(@PathVariable(name = "projectId") final long projectId) {
        final List<Summary> summary = summaryService.getSummaries(userService.getCurrentUser().getId(), projectId);

        return mapSummaries(summary);
    }

    @RequestMapping(value = "{projectId}/summaries/last", method = RequestMethod.GET)
    public SummaryInfoUI lastSummaryInfo(@PathVariable(name = "projectId") final long projectId) {
        final Summary summary = summaryService.getLastSummary(userService.getCurrentUser().getId(), projectId);

        return map(summary);
    }

    @RequestMapping(value = "{projectId}/summaries", method = RequestMethod.POST)
    public SummaryInfoUI updateProject(@PathVariable(name = "projectId") final int projectId) {
        final Summary summary = summaryService.makeSummary(projectId);

        return map(summary);
    }

    private List<ProjectUI> map(@Nonnull final List<Project> projects) {
        return projects.stream()
                .map(this::map)
                .collect(toList());
    }

    private List<SummaryInfoUI> mapSummaries(@Nonnull final List<Summary> summaries) {
        return summaries.stream()
                .map(this::map)
                .collect(toList());
    }

    private SummaryInfoUI map(@NonNull final Summary summary) {
        return SummaryInfoUI.builder()
                .id(summary.getId())
                .status(summary.getStatus())
                .date(summary.getLoadDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
                .build();
    }

    private ProjectUI map(@Nonnull final Project project) {
        return ProjectUI.builder()
                .id(project.getId())
                .name(project.getName())
                .build();
    }
}
