package io.digitalreactor.platform.web.api.mappers.visits;

import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.vendor.yandex.metrika.model.VisitWithGoalRow;
import io.digitalreactor.platform.web.model.report.VisitUI;
import io.digitalreactor.platform.web.model.report.visits.VisitsForTimeIntervalReportUI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * digital-reactor
 * Created by igor on 22.04.17.
 */

@Mapper(componentModel = "spring")
public interface VisitForTimeIntervalMapper {

    @Mapping(target = "samplingInterval", source = "sampling.signature")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "startDate", source = "sampling.startDate")
    @Mapping(target = "endDate", source = "sampling.endDate")
    VisitsForTimeIntervalReportUI mapReport(VisitsForTimeIntervalReport visitsForTimeIntervalReport);

    VisitUI mapVisit(VisitWithGoalRow visit);
}
