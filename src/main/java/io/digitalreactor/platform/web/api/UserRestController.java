package io.digitalreactor.platform.web.api;

import io.digitalreactor.platform.service.registration.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;

/**
 * digital-reactor
 * Created by igor on 13.04.17.
 */

@RestController
@RequestMapping("api/users/")
@AllArgsConstructor
public class UserRestController {
    private final RegistrationService registrationService;

    @RequestMapping(value = "{email}/resetpassword", method = RequestMethod.POST)
    public void resetPassword(@PathVariable @Nonnull final String email) {
        registrationService.resetPassword(email);
    }
}
