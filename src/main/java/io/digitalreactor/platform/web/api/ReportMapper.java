package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.platform.web.api.mappers.pages.PageUIMapper;
import io.digitalreactor.platform.web.api.mappers.phrases.SearchPhraseMapper;
import io.digitalreactor.platform.web.api.mappers.sources.ReferringSourceMapper;
import io.digitalreactor.platform.web.api.mappers.visits.VisitForTimeIntervalMapper;
import io.digitalreactor.platform.web.model.report.pages.PagesWithHighFailureRateReportUI;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseReportUI;
import io.digitalreactor.platform.web.model.report.sources.ReferringSourceReportUI;
import io.digitalreactor.platform.web.model.report.visits.VisitsForTimeIntervalReportUI;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * digital-reactor
 * Created by igor on 16.04.17.
 */

@Service
@AllArgsConstructor
public class ReportMapper {
    private PageUIMapper pageUIMapper;
    private SearchPhraseMapper searchPhraseMapper;
    private ReferringSourceMapper referringSourceMapper;
    private VisitForTimeIntervalMapper visitForTimeIntervalMapper;

    public Object mapToUI(final Report report) {
        switch (report.getType()) {
            case "VisitsForTimeIntervalReport":
                return mapToUI((VisitsForTimeIntervalReport) report.getPayload());
            case "PagesWithHighFailureRateReport":
                return mapToUI((PagesWithHighFailureRateReport) report.getPayload());
            case "SearchPhraseReport":
                return mapToUI((SearchPhraseReport) report.getPayload());
            case "ReferringSourceReport":
                return mapToUI((ReferringSourceReport) report.getPayload());
            default:
                throw new IllegalStateException("Unknown report type");
        }
    }

    public PagesWithHighFailureRateReportUI mapToUI(final PagesWithHighFailureRateReport pagesWithHighFailureRateReport) {
        return pageUIMapper.mapReport(pagesWithHighFailureRateReport);
    }

    public SearchPhraseReportUI mapToUI(final SearchPhraseReport searchPhraseReport) {
        return searchPhraseMapper.mapReport(searchPhraseReport);
    }

    public ReferringSourceReportUI mapToUI(final ReferringSourceReport referringSourceReport) {
        return referringSourceMapper.mapReport(referringSourceReport);
    }

    public VisitsForTimeIntervalReportUI mapToUI(final VisitsForTimeIntervalReport visitsForTimeIntervalReport) {
        return visitForTimeIntervalMapper.mapReport(visitsForTimeIntervalReport);
    }
}
