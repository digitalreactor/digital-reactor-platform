package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.service.summary.SummaryService;
import io.digitalreactor.analytic.metrika.service.summary.dto.SummaryInfo;
import io.digitalreactor.platform.service.user.UserService;
import io.digitalreactor.platform.web.model.GoalUI;
import io.digitalreactor.platform.web.model.summary.SummaryUI;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeFormatter;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("api/summaries")
public class SummaryRestController {
    @Autowired
    private UserService userService;

    @Autowired
    private SummaryService summaryService;

    @RequestMapping(value = "{summaryId}", method = RequestMethod.GET)
    public SummaryUI getSummary(@PathVariable("summaryId") final long summaryId) {
        final SummaryInfo summaryInfo = summaryService.getSummaryInfo(userService.getCurrentUser().getId(), summaryId);

        return map(summaryInfo);
    }

    private SummaryUI map(@NonNull final SummaryInfo summaryInfo) {
        return SummaryUI.builder()
                .date(summaryInfo.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
                .goals(summaryInfo.getGoals().stream()
                        .filter(reportGoal -> reportGoal.getGoalId() != 0)
                        .map(this::map)
                        .collect(toList())
                )
                .project(summaryInfo.getProjectName())
                .build();
    }

    private GoalUI map(@NonNull final ReportGoal reportGoal) {
        return GoalUI.builder()
                .id(reportGoal.getGoalId())
                .name(reportGoal.getGoalName())
                .build();
    }
}
