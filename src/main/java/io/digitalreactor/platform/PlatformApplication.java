package io.digitalreactor.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

/**
 * digital-reactor
 * Created by MStepachev on 07.09.2016.
 */
@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
@ComponentScan(basePackages = "io.digitalreactor.*")
public class PlatformApplication {
    public static void main(final String[] args) throws Exception {
        SpringApplication.run(new Class<?>[]{
                PlatformApplication.class
        }, args);
    }
}
