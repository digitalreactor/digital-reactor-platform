package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.analytic.metrika.common.report.enumeration.Interval;
import io.digitalreactor.analytic.metrika.common.report.object.DoubleInterval;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */
public class ReferringSourceCalculatorTest {
    private final ReferringSourceCalculator referringSourceCalculator = new ReferringSourceCalculatorImpl();

    @Test
    public void calculateSourceWithGoal() {
        assertThat(referringSourceCalculator.calculateSource(
                new DoubleInterval(SamplingInterval.of(Interval.TODAY)),
                ReferringSourceResponseCreatorUtil.referringSourceRowResponseWithGoalWithSums(),
                1)
        )
                .hasSize(3);
    }

    @Test
    public void calculateSourceWithoutGoal() {
        assertThat(referringSourceCalculator.calculateSource(
                new DoubleInterval(SamplingInterval.of(Interval.TODAY)),
                ReferringSourceResponseCreatorUtil.referringSourceRowResponseWithoutGoalWithSums(),
                0)
        )
                .hasSize(3);
    }
}