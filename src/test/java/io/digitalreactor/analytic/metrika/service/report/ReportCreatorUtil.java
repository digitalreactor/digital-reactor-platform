package io.digitalreactor.analytic.metrika.service.report;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.analytic.metrika.common.report.enumeration.DayType;
import io.digitalreactor.analytic.metrika.common.report.enumeration.Trend;
import io.digitalreactor.analytic.metrika.common.report.object.*;
import io.digitalreactor.analytic.metrika.model.DSPCutType;

import java.util.Collections;
import java.util.List;

import static io.digitalreactor.analytic.metrika.common.report.enumeration.Interval.TODAY;
import static java.time.LocalDate.now;

/**
 * digital-reactor
 * Created by igor on 17.04.17.
 */
public class ReportCreatorUtil {
    public static PagesWithHighFailureRateReport pagesWithHighFailureRateReport() {
        return PagesWithHighFailureRateReport
                .builder()
                .sampling(SamplingInterval.of(TODAY))
                .metrics(singletonListPageList())
                .reportGoal(reportGoal())
                .build();
    }

    public static PagesWithHighFailureRateReport pagesWithHighFailureRateReportWithNoNullGoal() {
        return PagesWithHighFailureRateReport
                .builder()
                .sampling(SamplingInterval.of(TODAY))
                .metrics(singletonListPageList())
                .reportGoal(noNullReportGoal())
                .build();
    }

    public static SearchPhraseReport searchPhraseReport() {
        return SearchPhraseReport
                .builder()
                .sampling(SamplingInterval.of(TODAY))
                .badPhrases(singletonSearchPhraseList())
                .successPhrases(singletonSearchPhraseList())
                .reportGoal(reportGoal())
                .build();
    }

    public static ReferringSourceReport referringSourceReport() {
        return ReferringSourceReport
                .builder()
                .sampling(SamplingInterval.of(TODAY))
                .sources(singletonReferrinSourceList())
                .reportGoal(reportGoal())
                .build();
    }

    public static VisitsForTimeIntervalReport visitsForTimeIntervalReport() {
        return VisitsForTimeIntervalReport
                .builder()
                .sampling(SamplingInterval.of(TODAY))
                .metrics(singletonVisitWithGoalList())
                .reason("reason")
                .trend(Trend.DECREASING)
                .visitChange(45)
                .visitChangePercent(55)
                .conversionChange(12)
                .conversionChangePercent(22)
                .reportGoal(reportGoal())
                .build();
    }

    public static SearchPhrase searchPhrase() {
        return SearchPhrase
                .builder()
                .quality(0.1)
                .avgVisitDurationSeconds(2.3)
                .pageDepth(4.5)
                .bounceRate(6.7)
                .searchPhrase("searchPhrase")
                .visits(8)
                .build();
    }

    public static Page page() {
        return Page.builder()
                .visits(1)
                .bounceRate(2.3)
                .avgVisitDurationSeconds(4.5)
                .pageViews(6)
                .favicon("7")
                .siteName("8")
                .conversion(5)
                .build();
    }

    public static SearchPhraseCut searchPhraseCut() {
        return SearchPhraseCut.builder()
                .goalName("goalName")
                .type(DSPCutType.SYSTEM)
                .successPhrases(singletonSearchPhraseList())
                .badPhrases(singletonSearchPhraseList())
                .build();
    }

    public static ReferringSource referringSource() {
        return ReferringSource
                .builder()
                .name("name")
                .metrics(singletonVisitList())
                .totalVisits(1)
                .totalVisitsChangePercent(50)
                .goalReferringSourceStatistic(goalReferringSourceStatistic())
                .build();
    }

    public static Visit visit() {
        return Visit
                .builder()
                .number(1)
                .dayType(DayType.HOLIDAY)
                .date(now())
                .build();
    }

    public static VisitsAndConversion visitWithGoal() {
        return VisitsAndConversion
                .builder()
                .number(1)
                .dayType(DayType.HOLIDAY)
                .date(now())
                .conversion(4)
                .build();
    }

    public static GoalReferringSource goalReferringSource() {
        return GoalReferringSource
                .builder()
                .name("name")
                .conversion(10)
                .conversionChange(12)
                .numberOfCompletedGoal(45)
                .sources(singletonReferrinSourceList())
                .build();
    }

    private static ReportGoal reportGoal() {
        return ReportGoal.builder().goalId(0L).goalName("System goal").build();
    }

    private static ReportGoal noNullReportGoal() {
        return ReportGoal.builder().goalId(12345L).goalName("Goal").build();
    }

    private static List<Page> singletonListPageList() {
        return Collections.singletonList(page());
    }

    private static List<VisitsAndConversion> singletonVisitWithGoalList() {
        return Collections.singletonList(visitWithGoal());
    }

    private static List<SearchPhraseCut> singletonListSearchPhraseCutList() {
        return Collections.singletonList(searchPhraseCut());
    }

    private static List<SearchPhrase> singletonSearchPhraseList() {
        return Collections.singletonList(searchPhrase());
    }

    private static List<ReferringSource> singletonReferrinSourceList() {
        return Collections.singletonList(referringSource());
    }

    private static GoalReferringSourceStatistic goalReferringSourceStatistic() {
        return GoalReferringSourceStatistic
                .builder()
                .totalGoalVisitsChangePercent(1.2)
                .totalGoalVisits(30)
                .conversionChangePercent(3.4)
                .conversion(50)
                .build();
    }

    private static List<Visit> singletonVisitList() {
        return Collections.singletonList(visit());
    }

    private static List<GoalReferringSource> singletonGoalReferrinSourceList() {
        return Collections.singletonList(
                goalReferringSource()
        );
    }
}
