package io.digitalreactor.analytic.metrika.service.report;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 15.04.17.
 */
public class ReportMathUtilsTest {

    @Test
    public void conversion() {
        assertThat(ReportMathUtils.conversion(
                2, 4
        ))
                .isEqualTo(0.5);
    }

    @Test
    public void conversionWithoutAnyVisists() {
        assertThat(ReportMathUtils.conversion(
                0,0
        ))
                .isEqualTo(0.0);
    }

    @Test
    public void changePercentForEmptyIntervals() throws Exception {
        assertThat(ReportMathUtils.changePercent(
                emptyList(), emptyList()
        )).isEqualTo(0);
    }

    @Test
    public void changePercentLeftEmpty() {
        assertThat(ReportMathUtils.changePercent(
                emptyList(), notEmptyList())
        ).isEqualTo(100);
    }

    @Test
    public void changePercentRightEmpty() {
        assertThat(ReportMathUtils.changePercent(
                notEmptyList(), emptyList()
        )).isEqualTo(-100);
    }

    @Test
    public void changePercentNotEmpty() {
        assertThat(ReportMathUtils.changePercent(
                notEmptyList(), notEmptyList()
        )).isEqualTo(0);

        assertThat(ReportMathUtils.changePercent(
                makeListFromSum(10), makeListFromSum(5)
        )).isEqualTo(-50);

        assertThat(ReportMathUtils.changePercent(
                makeListFromSum(10), makeListFromSum(15)
        )).isEqualTo(50);


    }

    @Test
    public void divideOddList() {
        assertThat(ReportMathUtils.getFirstPartOfList(oddList()))
                .hasSize(2)
                .contains(1, 2);


        assertThat(ReportMathUtils.getSecondPartOfList(oddList()))
                .hasSize(3)
                .contains(3, 4, 5);
    }

    @Test
    public void divideEvenList() {
        assertThat(ReportMathUtils.getFirstPartOfList(evenList()))
                .hasSize(2)
                .contains(1, 2);

        assertThat(ReportMathUtils.getSecondPartOfList(evenList()))
                .hasSize(2)
                .contains(3, 4);
    }

    private List<Integer> emptyList() {
        return new ArrayList<>();
    }

    private List<Integer> notEmptyList() {
        return Arrays.asList(1, 2, 3);
    }

    private List<Integer> makeListFromSum(final int sum) {
        return Collections.singletonList(sum);
    }

    private List<Integer> oddList() {
        return Arrays.asList(1, 2, 3, 4, 5);
    }

    private List<Integer> evenList() {
        return Arrays.asList(1, 2, 3, 4);
    }
}