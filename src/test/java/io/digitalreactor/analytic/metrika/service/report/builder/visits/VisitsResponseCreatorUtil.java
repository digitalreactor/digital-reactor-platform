package io.digitalreactor.analytic.metrika.service.report.builder.visits;

import io.digitalreactor.analytic.metrika.common.report.enumeration.DayType;
import io.digitalreactor.vendor.yandex.metrika.model.VisitWithGoalRow;

import java.time.LocalDate;
import java.util.*;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */
public class VisitsResponseCreatorUtil {
    public static List<VisitWithGoalRow> visitsRow() {
        return Arrays.asList(
                VisitWithGoalRow.builder().date(LocalDate.of(2016, 8, 1)).dayType(DayType.HOLIDAY).number(31)
                        .goal("").conversion(0.0).build(),
                VisitWithGoalRow.builder().date(LocalDate.of(2016, 8, 2)).dayType(DayType.WEEKDAY).number(28)
                        .goal("").conversion(0.0).build(),
                VisitWithGoalRow.builder().date(LocalDate.of(2016, 8, 3)).dayType(DayType.WEEKDAY).number(34)
                        .goal("").conversion(0.0).build()
        );
    }
}
