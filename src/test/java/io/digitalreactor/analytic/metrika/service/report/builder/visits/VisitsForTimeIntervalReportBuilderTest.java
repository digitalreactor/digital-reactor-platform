package io.digitalreactor.analytic.metrika.service.report.builder.visits;

import io.digitalreactor.analytic.metrika.common.report.enumeration.Interval;
import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import io.digitalreactor.platform.PlatformApplication;
import io.digitalreactor.platform.TestConfig;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.vendor.yandex.metrika.serivce.ReportApiService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collections;

import static io.digitalreactor.analytic.metrika.service.report.builder.referringsources.ReferringSourceResponseCreatorUtil.fakeGoal;
import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlatformApplication.class, TestConfig.class})
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class VisitsForTimeIntervalReportBuilderTest {
    @MockBean
    private ReportApiService reportApiService;

    @Autowired
    private VisitsForTimeIntervalReportBuilder visitsForTimeIntervalReportBuilder;

    @Before
    public void setUp() {
        reportApiService = mock(ReportApiService.class);

        when(reportApiService.getVisits(anyString(), anyLong(), any(), any(),any()))
                .thenReturn(VisitsResponseCreatorUtil.visitsRow());
    }

    @Test
    public void makeReport() throws Exception {
        visitsForTimeIntervalReportBuilder.makeReports(
                fakeToken(),
                fakeCounter(),
                SamplingInterval.of(Interval.TODAY),
                Collections.singletonList(fakeGoal())
        );
    }

    private MetrikaCounter fakeCounter() {
        return MetrikaCounter.builder().build();
    }

    private MetrikaToken fakeToken() {
        return MetrikaToken.builder().token("token").build();
    }

}