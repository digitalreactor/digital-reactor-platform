package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */
public class ReferringSourceBuilderTest {
    @Test
    public void evaluateSumsForReferringSourceRowListWithGoal() throws Exception {
        final List<Pair<List<ReferringSourceRow>, Goal>> referringSourceRowsWithGoals = Arrays.asList(
                Pair.of(
                        ReferringSourceResponseCreatorUtil.referringSourceRowResponseWithGoal(),
                        ReferringSourceResponseCreatorUtil.fakeGoal()
                ),
                Pair.of(
                        ReferringSourceResponseCreatorUtil.referringSourceRowResponseWithGoal(),
                        ReferringSourceResponseCreatorUtil.fakeGoal()
                )
        );

        final Pair<List<ReferringSourceRow>, Goal> expected =
                Pair.of(
                        ReferringSourceResponseCreatorUtil.referringSourceRowResponseWithGoalWithSums(),
                        ReferringSourceResponseCreatorUtil.fakeGoal()
        );

        assertThat(ReferringSourceBuilder.evaluateSumsForReferringSourceRowList(referringSourceRowsWithGoals))
                .hasSize(2)
                .contains(expected);
    }

}