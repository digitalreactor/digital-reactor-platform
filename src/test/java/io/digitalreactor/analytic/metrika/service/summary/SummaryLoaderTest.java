package io.digitalreactor.analytic.metrika.service.summary;

import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.service.report.ReportService;
import io.digitalreactor.platform.PlatformApplication;
import io.digitalreactor.platform.TestConfig;
import io.digitalreactor.platform.TestUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * digital-reactor
 * Created by igor on 05.05.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlatformApplication.class, TestConfig.class})
@ActiveProfiles(SPRING_PROFILE_TEST)
public class SummaryLoaderTest {
    @Autowired
    private TestUtil testUtil;

    @Autowired
    private SummaryLoader summaryLoader;

    private ReportService reportService;

    @Before
    public void setUp() {
        reportService = mock(ReportService.class);
    }

    @After
    public void tearDown() {
        testUtil.clear();
    }

    @Test
    @Transactional
    public void findAllNewSummaries() throws Exception {
        final Summary summary1 = testUtil.addSummaryToFirstProject(),
        summary2 = testUtil.addSummaryToFirstProject();

        assertThat(summaryLoader.findAllNewSummaries())
                .hasSize(2)
                .containsOnlyOnce(summary1,summary2);
    }

    @Test
    @Transactional
    public void loadSummaryWithoutException() {
        final Summary summary = testUtil.addSummaryToFirstProject();

        summaryLoader.loadSummary(summary);
    }

    @Test
    @Transactional
    public void loadSummaryWithException() {
        final Summary summary = testUtil.addSummaryToFirstProject();

        when(reportService.tryToMakeDefaultReports(summary.getId()))
                .thenThrow(new RuntimeException());

        summaryLoader.loadSummary(summary);
    }
}