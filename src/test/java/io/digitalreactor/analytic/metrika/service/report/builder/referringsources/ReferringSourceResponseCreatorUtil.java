package io.digitalreactor.analytic.metrika.service.report.builder.referringsources;

import io.digitalreactor.vendor.yandex.metrika.model.Goal;
import io.digitalreactor.vendor.yandex.metrika.model.ReferringSourceRow;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */
public class ReferringSourceResponseCreatorUtil {
    public static List<ReferringSourceRow> referringSourceRowResponseWithGoal() {
        return Arrays.asList(
                ReferringSourceRow.builder().name("name1").metrics(metricsForReferringSourceRowWithGoal()).build(),
                ReferringSourceRow.builder().name("name2").metrics(metricsForReferringSourceRowWithGoal()).build(),
                ReferringSourceRow.builder().name("name3").metrics(metricsForReferringSourceRowWithGoal()).build()
        );
    }

    public static List<ReferringSourceRow> referringSourceRowResponseWithoutGoal() {
        return Arrays.asList(
                ReferringSourceRow.builder().name("name1").metrics(metricsForReferringSourceRowWithoutGoal()).build(),
                ReferringSourceRow.builder().name("name2").metrics(metricsForReferringSourceRowWithoutGoal()).build(),
                ReferringSourceRow.builder().name("name3").metrics(metricsForReferringSourceRowWithoutGoal()).build()
        );
    }

    public static List<ReferringSourceRow> referringSourceRowResponseWithGoalWithSums() {
        return Arrays.asList(
                ReferringSourceRow.builder().name("name1").metrics(metricsForReferringSourceRowWithGoal()).sums(sumsForMetricWithGoal()).build(),
                ReferringSourceRow.builder().name("name2").metrics(metricsForReferringSourceRowWithGoal()).sums(sumsForMetricWithGoal()).build(),
                ReferringSourceRow.builder().name("name3").metrics(metricsForReferringSourceRowWithGoal()).sums(sumsForMetricWithGoal()).build()
        );
    }

    public static List<ReferringSourceRow> referringSourceRowResponseWithoutGoalWithSums() {
        return Arrays.asList(
                ReferringSourceRow.builder().name("name1").metrics(metricsForReferringSourceRowWithoutGoal()).sums(sumsForMetricWithoutGoal()).build(),
                ReferringSourceRow.builder().name("name2").metrics(metricsForReferringSourceRowWithoutGoal()).sums(sumsForMetricWithoutGoal()).build(),
                ReferringSourceRow.builder().name("name3").metrics(metricsForReferringSourceRowWithoutGoal()).sums(sumsForMetricWithoutGoal()).build()
        );
    }

    private static List<Pair<Integer, Integer>> sumsForMetricWithoutGoal() {
        return Collections.singletonList(
                Pair.of(15, 79)
        );
    }

    private static List<Pair<Integer, Integer>> sumsForMetricWithGoal() {
        return Arrays.asList(
                Pair.of(
                        15, 79
                ),
                Pair.of(
                        0, 3
                )
        );
    }

    private static List<List<Integer>> metricsForReferringSourceRowWithoutGoal() {
        return Collections.singletonList(
                Arrays.asList(15, 43, 36)
        );
    }

    private static List<List<Integer>> metricsForReferringSourceRowWithGoal() {
        return Arrays.asList(
                Arrays.asList(15, 43, 36),
                Arrays.asList(0, 1, 2)
        );
    }

    public static Goal fakeGoal() {
        return Goal.builder().id(0).name("name").isRetargeting(true).type("type").build();
    }
}
