package io.digitalreactor.analytic.metrika.service.report.builder.pages;

import io.digitalreactor.platform.PlatformApplication;
import io.digitalreactor.platform.TestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlatformApplication.class, TestConfig.class})
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class PagesWithHighFailureRateBuilderTest {

    @Autowired
    private PagesWithHighFailureRateBuilder pagesWithHighFailureRateBuilder;

    @Test
    public void filteredAndSortedPages() throws Exception {
        assertThat(pagesWithHighFailureRateBuilder.filteredAndSortedPages(PagesResponseCreatorUtil.pagesRow()))
                .hasSize(1);
    }

}