package io.digitalreactor.analytic.metrika.service.report.builder.phrases;

import io.digitalreactor.vendor.yandex.metrika.model.PhraseRow;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */
public class SearchPhrasesResponseCreatorUtil {
    public static List<PhraseRow> phraseRowsWithGoal() {
        return Arrays.asList(
                PhraseRow.builder().bounceRate(0.0).avgVisitDurationSeconds(57).goalAndConversion(oneGoalMap(0)).pageDepth(2.8).searchPhrase("купить ирисы в спб").visits(5).build(),
                PhraseRow.builder().bounceRate(2.0).avgVisitDurationSeconds(32).goalAndConversion(oneGoalMap(1)).pageDepth(1.3).searchPhrase("цветы с доставкой в спб дешево").visits(3).build(),
                PhraseRow.builder().bounceRate(7.5).avgVisitDurationSeconds(104).goalAndConversion(oneGoalMap(2)).pageDepth(5.6).searchPhrase("null").visits(3).build()
        );
    }

    private static Map<String, Integer> oneGoalMap(final int goalMetric) {
        return new HashMap<String, Integer>() {{
            put("goal", goalMetric);
        }};
    }
}
