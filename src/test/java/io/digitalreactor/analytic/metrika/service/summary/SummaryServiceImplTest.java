package io.digitalreactor.analytic.metrika.service.summary;

import io.digitalreactor.analytic.metrika.dao.SummaryDao;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.model.summary.SummaryStatus;
import io.digitalreactor.platform.PlatformApplication;
import io.digitalreactor.platform.TestConfig;
import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;

/**
 * digital-reactor
 * Created by igor on 05.05.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlatformApplication.class, TestConfig.class})
@ActiveProfiles(SPRING_PROFILE_TEST)
public class SummaryServiceImplTest {
    @Autowired
    private TestUtil testUtil;

    @Autowired
    private SummaryDao summaryDao;

    private SummaryLoader summaryLoader;

    private SummaryService summaryService;

    @Before
    public void setUp() {
        summaryLoader = mock(SummaryLoader.class);

        summaryService = new SummaryServiceImpl(summaryDao,summaryLoader, null);
    }

    @After
    public void tearDown() {
        testUtil.clear();
    }

    @Test
    @Transactional
    public void getLastSummaryNotExists() throws Exception {
        final int userId = anyInt(),
                projectId = anyInt();

        assertThatThrownBy(() ->
                summaryService.getLastSummary(userId, projectId)
        )
                .isInstanceOf(SummaryNotFoundException.class)
                .hasMessageContaining(
                        String.format("Last summary not found for project %s user: %s", projectId, userId)
                );
    }

    private int anyInt() {
        return 0;
    }

    @Test
    @Transactional
    public void getLastSummary() throws Exception {
        final Summary summary1 = testUtil.addSummaryToFirstProject(),
                summary2 = testUtil.addSummaryToFirstProject();

        final User user = testUtil.getFirstUser();

        assertThat(summaryService.getLastSummary(user.getId(), summary1.getProjectId()))
                .isEqualToComparingFieldByField(summary2);
    }

    @Test
    @Transactional
    public void makeSummary() throws Exception {
        final Project project = testUtil.addProjectToFirstUser("project");

        assertThat(summaryService.makeSummary(project.getId()))
                .hasFieldOrPropertyWithValue("projectId", project.getId())
                .hasFieldOrPropertyWithValue("status", SummaryStatus.NEW);
    }

    @Test
    @Transactional
    public void getSummaryById() throws Exception {
        final Summary summary = testUtil.addSummaryToFirstProject();

        assertThat(summaryService.getSummaryById(summary.getId()))
                .isEqualToComparingFieldByField(summary);
    }

    @Test
    @Transactional
    public void getSummaryByIdNotExists() throws Exception {
        final int summaryId = 0;

        assertThatThrownBy(() ->
                summaryService.getSummaryById(summaryId)
        )
                .isInstanceOf(SummaryNotFoundException.class)
                .hasMessageContaining(
                        String.format("Summary with id: %s not found.", summaryId)
                );
    }

    @Test
    @Transactional
    public void getSummaries() throws Exception {
        final Summary summary1 = testUtil.addSummaryToFirstProject(),
                summary2 = testUtil.addSummaryToFirstProject();

        final User user = testUtil.getFirstUser();

        assertThat(summaryService.getSummaries(user.getId(), summary1.getProjectId()))
                .hasSize(2)
                .containsOnlyOnce(summary1, summary2);
    }

}