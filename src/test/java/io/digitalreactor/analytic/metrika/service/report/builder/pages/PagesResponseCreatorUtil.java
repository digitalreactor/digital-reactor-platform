package io.digitalreactor.analytic.metrika.service.report.builder.pages;

import io.digitalreactor.vendor.yandex.metrika.model.PageRow;

import java.util.Arrays;
import java.util.List;

/**
 * digital-reactor
 * Created by igor on 07.05.17.
 */
public class PagesResponseCreatorUtil {
    public static List<PageRow> pagesRow() {
        return Arrays.asList(
                PageRow.builder().avgVisitDurationSeconds(136.3984375).favicon("perm.im-slim.ru").bounceRate(13.28125).pageViews(128).siteName("site1").visits(4).build(),
                PageRow.builder().avgVisitDurationSeconds(99.11538462).favicon("im-slim.ru").bounceRate(53.84615385).pageViews(26).siteName("site2").visits(5).build(),
                PageRow.builder().avgVisitDurationSeconds(134.14285714).favicon("perm.im-slim.ru").bounceRate(0).pageViews(7).siteName("site3").visits(6).build()
        );
    }
}
