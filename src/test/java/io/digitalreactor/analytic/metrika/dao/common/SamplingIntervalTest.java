package io.digitalreactor.analytic.metrika.dao.common;

import io.digitalreactor.analytic.metrika.common.report.object.SamplingInterval;
import org.junit.Test;

import java.time.LocalDate;

import static io.digitalreactor.analytic.metrika.common.report.enumeration.Interval.*;
import static java.time.LocalDate.now;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * digital-reactor
 * Created by igor on 30.03.17.
 */
public class SamplingIntervalTest {
    private static final String DATE_PATTERN = "\\d{4}-\\d{2}-\\d{2}";
    private static final String MONTH_NAME_PATTERN = "[\\w]*";
    private static final String SINGLE_STRING_FORMAT = "^Отчет за %s$";
    private static final String DOUBLE_STRING_FORMAT = "^Отчет за %s по %s$";

    @Test
    public void todayTest() {
        final SamplingInterval today = SamplingInterval.of(TODAY);

        assertThat(today.getStartDate())
                .isEqualTo(now());
        assertThat(today.getEndDate())
                .isEqualTo(now());
        assertThat(today.getSignature())
                .matches(String.format(SINGLE_STRING_FORMAT,"сегодня"));
    }

    @Test
    public void yesterdayTest() {
        final SamplingInterval yesterday = SamplingInterval.of(YESTERDAY);

        assertThat(yesterday.getStartDate())
                .isEqualTo(now().minusDays(1));
        assertThat(yesterday.getEndDate())
                .isEqualTo(now());
        assertThat(yesterday.getSignature())
                .matches(String.format(SINGLE_STRING_FORMAT,DATE_PATTERN));
    }

    @Test
    public void last7DaysTest() {
        final SamplingInterval last7days = SamplingInterval.of(LAST_7_DAYS);

        assertThat(last7days.getStartDate())
                .isEqualTo(now().minusDays(8));
        assertThat(last7days.getEndDate())
                .isEqualTo(now().minusDays(1));
        assertThat(last7days.getSignature())
                .matches(String.format(DOUBLE_STRING_FORMAT,DATE_PATTERN,DATE_PATTERN));
    }

    @Test
    public void last30DaysTest() {
        final SamplingInterval last30days = SamplingInterval.of(LAST_30_DAYS);

        assertThat(last30days.getStartDate())
                .isEqualTo(now().minusDays(31));
        assertThat(last30days.getEndDate())
                .isEqualTo(now().minusDays(1));
        assertThat(last30days.getSignature())
                .matches(String.format(DOUBLE_STRING_FORMAT,DATE_PATTERN,DATE_PATTERN));
    }

    @Test
    public void lastMonthTest() {
        final SamplingInterval lastMonth = SamplingInterval.of(LAST_MONTH);
        final LocalDate initial = now().minusMonths(1);
        final LocalDate startDate = initial.withDayOfMonth(1);
        final LocalDate endDate = initial.withDayOfMonth(initial.lengthOfMonth());

        assertThat(lastMonth.getStartDate())
                .isEqualTo(startDate);
        assertThat(lastMonth.getEndDate())
                .isEqualTo(endDate);
        assertThat(lastMonth.getSignature())
                .matches(String.format(SINGLE_STRING_FORMAT,MONTH_NAME_PATTERN));
    }
}
