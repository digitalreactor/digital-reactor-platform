package io.digitalreactor.analytic.metrika.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digitalreactor.analytic.metrika.common.report.*;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.PlatformApplication;
import io.digitalreactor.platform.TestConfig;
import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.dao.ProjectDao;
import io.digitalreactor.platform.dao.UserDao;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlatformApplication.class, TestConfig.class})
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportDaoIT {
    @Autowired
    private ReportDao reportDao;

    @Autowired
    private SummaryDao summaryDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private TestUtil testUtil;

    private Summary currentSummary;

    @Test
    @Transactional
    public void saveAndFindVisitsForTimeIntervalReport() throws JsonProcessingException {
        final VisitsForTimeIntervalReport visitsForTimeIntervalReport = ReportCreatorUtil.visitsForTimeIntervalReport();
        final Report report = addReportAndAllNecessary(visitsForTimeIntervalReport);

        expectedReportAssertion(report, currentSummary, visitsForTimeIntervalReport);
    }

    @Test
    @Transactional
    public void saveAndFindPagesWithHighFailureRateReport() throws JsonProcessingException {
        final PagesWithHighFailureRateReport pagesWithHighFailureRateReport = ReportCreatorUtil.pagesWithHighFailureRateReport();
        final Report report = addReportAndAllNecessary(pagesWithHighFailureRateReport);

        expectedReportAssertion(report, currentSummary, pagesWithHighFailureRateReport);
    }

    @Test
    @Transactional
    public void saveAndFindReferringSourceReport() throws JsonProcessingException {
        final ReferringSourceReport referringSourceReport = ReportCreatorUtil.referringSourceReport();
        final Report report = addReportAndAllNecessary(referringSourceReport);

        expectedReportAssertion(report, currentSummary, referringSourceReport);
    }

    @Test
    @Transactional
    public void saveAndFindSearchPhraseReport() throws JsonProcessingException {
        final SearchPhraseReport searchPhraseReport = ReportCreatorUtil.searchPhraseReport();
        final Report report = addReportAndAllNecessary(searchPhraseReport);

        expectedReportAssertion(report, currentSummary, searchPhraseReport);
    }

    private void expectedReportAssertion(final Report report, final Summary summary, final BaseReportType realReport) {
        assertThat(reportDao.findByReportId(report.getId())).hasValueSatisfying(r -> {
            assertThat(r.getId()).isGreaterThan(0).isEqualTo(report.getId());
            assertThat(r.getSummaryId()).isEqualTo(summary.getId());
            assertThat(r.getType()).isEqualTo(realReport.getClass().getSimpleName());
            assertThat(r.getPayload()).isEqualTo(realReport);
        });
    }

    private Report addReportAndAllNecessary(final BaseReportType report) throws JsonProcessingException {
        final User user = userDao.register(testUtil.generateEmail(), "1234");
        final Project project = projectDao.add(user.getId(), "test");
        currentSummary = summaryDao.add(project.getId(), LocalDateTime.now());

        return reportDao.add(currentSummary.getId(), report);
    }
}