package io.digitalreactor.vendor.yandex.oauth;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("deprecation")
public class TokenResolveServiceTest {
    private static final String APPLICATION_ID = "23s23d";
    private static final String APPLICATION_AUTH = "Base adafs";
    private static final String CLIENT_SECRET = "secretClient";
    private static final String OAUTH_URL = "https://oauth.url/";
    private final HttpClientHelper httpClient = new HttpClientHelper();
    private TokenResolveService tokenResolveService;

    @Before
    public void setUp() {
        tokenResolveService = new TokenResolveServiceImpl(APPLICATION_ID, APPLICATION_AUTH, CLIENT_SECRET, OAUTH_URL, httpClient);
    }

    @Test
    public void getTokenByGrantCode() throws Exception {
        final String expectedToken = "asdf23sdaaasa";
        final int grantCode = 23423;
        httpClient.setMockDate(200, makeResponse("{\"access_token\": \"" + expectedToken + "\"}"));

        final String tokenByGrantCode = tokenResolveService.getTokenByGrantCode(grantCode);

        assertThat(tokenByGrantCode).isEqualTo(expectedToken);
    }

    private HttpEntity makeResponse(final String json) {
        final BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
        basicHttpEntity.setContent(new ByteArrayInputStream(json.getBytes()));

        return basicHttpEntity;
    }

    private static class HttpClientHelper implements HttpClient {
        private HttpUriRequest request;
        private int statusCode;
        private HttpEntity entity;

        public void setMockDate(final int statusCode, final HttpEntity httpEntity) {
            this.statusCode = statusCode;
            this.entity = httpEntity;
        }

        @Override
        @Deprecated
        public HttpParams getParams() {
            return null;
        }

        @Override
        @Deprecated
        public ClientConnectionManager getConnectionManager() {
            return null;
        }

        @Override
        public HttpResponse execute(final HttpUriRequest request) throws IOException {
            return null;
        }

        @Override
        public HttpResponse execute(final HttpUriRequest request, final HttpContext context) throws IOException {
            return null;
        }

        @Override
        public HttpResponse execute(final HttpHost target, final HttpRequest request) throws IOException {
            return null;
        }

        @Override
        public HttpResponse execute(final HttpHost target, final HttpRequest request, final HttpContext context) throws IOException {
            return null;
        }

        @Override
        public <T> T execute(final HttpUriRequest request,
                             final ResponseHandler<? extends T> responseHandler) throws IOException {
            this.request = request;

            final HttpResponse response = new BasicHttpResponse(
                    new BasicStatusLine(new ProtocolVersion("mock", 1, 1), statusCode, "it is mock")
            );
            response.setEntity(entity);

            return responseHandler.handleResponse(response);

        }

        @Override
        public <T> T execute(final HttpUriRequest request, final ResponseHandler<? extends T> responseHandler, final HttpContext context) throws IOException {
            return null;
        }

        @Override
        public <T> T execute(final HttpHost target, final HttpRequest request, final ResponseHandler<? extends T> responseHandler) throws IOException {
            return null;
        }

        @Override
        public <T> T execute(final HttpHost target, final HttpRequest request, final ResponseHandler<? extends T> responseHandler, final HttpContext context) throws IOException {
            return null;
        }

    }
}