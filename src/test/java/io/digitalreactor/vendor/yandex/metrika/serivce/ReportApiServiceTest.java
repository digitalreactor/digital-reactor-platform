package io.digitalreactor.vendor.yandex.metrika.serivce;

import io.digitalreactor.vendor.yandex.metrika.model.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static io.digitalreactor.analytic.metrika.common.report.enumeration.DayType.HOLIDAY;
import static io.digitalreactor.analytic.metrika.common.report.enumeration.DayType.WEEKDAY;
import static io.digitalreactor.analytic.metrika.service.report.builder.referringsources.ReferringSourceResponseCreatorUtil.fakeGoal;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportApiServiceTest {
    private static final String TOKEN = "XXXXXXXXXXXXXXXXX";
    private static final long COUNTER_ID = 43523443;

    private final JsonClientWrapper jsonClientWrapper = mock(JsonClientWrapper.class);
    private final ReportApiService reportApiService = new ReportApiService(jsonClientWrapper);

    @Test
    public void getVisitWithGoals() throws IOException {
        when(jsonClientWrapper.getJsonResponse(anyString())).thenReturn(FileUtils.readFileToString(new File("src/test/resources/dataset/visits.json"), "UTF-8"));
        final LocalDate startDate = LocalDate.of(2017, 4, 20);
        final LocalDate endDate = LocalDate.of(2017, 4, 24);

        final List<VisitWithGoalRow> visits = reportApiService.getVisits(TOKEN, COUNTER_ID, startDate, endDate, fakeGoal());

        assertThat(visits).containsOnlyOnce(
                VisitWithGoalRow.builder().date(startDate.atStartOfDay().toLocalDate())
                        .dayType(WEEKDAY).number(15).goal("").conversion(0.0).build(),
                VisitWithGoalRow.builder().date(startDate.plusDays(1).atStartOfDay().toLocalDate())
                        .dayType(WEEKDAY).number(21).goal("").conversion(1.0).build(),
                VisitWithGoalRow.builder().date(startDate.plusDays(2).atStartOfDay().toLocalDate())
                        .dayType(HOLIDAY).number(9).goal("").conversion(1.0).build(),
                VisitWithGoalRow.builder().date(startDate.plusDays(3).atStartOfDay().toLocalDate())
                        .dayType(HOLIDAY).number(15).goal("").conversion(3.0).build(),
                VisitWithGoalRow.builder().date(startDate.plusDays(4).atStartOfDay().toLocalDate())
                        .dayType(WEEKDAY).number(23).goal("").conversion(0.0).build()
        );
    }

    @Test
    public void getPages() throws IOException {
        when(jsonClientWrapper.getJsonResponse(anyString())).thenReturn(FileUtils.readFileToString(new File("src/test/resources/dataset/pages.json"), "UTF-8"));

        final LocalDate startDate = LocalDate.of(2017, 3, 19);
        final LocalDate endDate = LocalDate.of(2017, 3, 27);

        final List<PageRow> pageRows = reportApiService.getPages(TOKEN, COUNTER_ID, startDate, endDate, fakeGoal());

        assertThat(pageRows).hasSize(7).contains(
                PageRow.builder().siteName("http://perm.im-slim.ru/").favicon("perm.im-slim.ru").visits(128)
                        .bounceRate(13.28125).pageViews(347).avgVisitDurationSeconds(136.3984375)
                        .goal("").conversion(5.0).build(),
                PageRow.builder().siteName("http://im-slim.ru/").favicon("im-slim.ru").visits(26)
                        .bounceRate(53.84615385).pageViews(35).avgVisitDurationSeconds(99.11538462)
                        .goal("").conversion(4.0).build(),
                PageRow.builder().siteName("http://perm.im-slim.ru/reviews").favicon("perm.im-slim.ru").visits(7)
                        .bounceRate(0.0).pageViews(10).avgVisitDurationSeconds(134.14285714)
                        .goal("").conversion(3.0).build()
        );
    }

    @Test
    public void getSearchPhrases() throws IOException {
        when(jsonClientWrapper.getJsonResponse(anyString())).thenReturn(FileUtils.readFileToString(new File("src/test/resources/dataset/search_phrases.json"), "UTF-8"));

        final LocalDate startDate = LocalDate.of(2017, 3, 19);
        final LocalDate endDate = LocalDate.of(2017, 3, 27);

        final List<PhraseRow> phraseRows = reportApiService.getPhrases(TOKEN, COUNTER_ID, startDate, endDate, fakeGoal());

        assertThat(phraseRows).hasSize(9)
                .contains(
                        PhraseRow.builder().searchPhrase("корзина с розами купить в спб").visits(1).bounceRate(0.0).pageDepth(1.0).avgVisitDurationSeconds(15.0).build(),
                        PhraseRow.builder().searchPhrase("доставка цветов в коробке спб").visits(1).bounceRate(0.0).pageDepth(5.0).avgVisitDurationSeconds(69.0).build(),
                        PhraseRow.builder().searchPhrase("доставка цветов спб").visits(1).bounceRate(0.0).pageDepth(1.0).avgVisitDurationSeconds(26.0).build(),
                        PhraseRow.builder().searchPhrase("доставка цветов спб в течении часа").visits(1).bounceRate(0.0).pageDepth(1.0).avgVisitDurationSeconds(34.0).build(),
                        PhraseRow.builder().searchPhrase("цветы поштучно с доставкой спб").visits(1).bounceRate(0.0).pageDepth(4.0).avgVisitDurationSeconds(504.0).build()
                );
    }

    @Test
    public void getReferringSources() throws IOException {
        when(jsonClientWrapper.getJsonResponse(anyString())).thenReturn(FileUtils.readFileToString(new File("src/test/resources/dataset/referring_sources.json"), "UTF-8"));
        final LocalDate startDate = LocalDate.of(2017, 3, 19);
        final LocalDate endDate = LocalDate.of(2017, 3, 27);

        final List<ReferringSourceRow> metrikaResponseDatas = reportApiService.getSources(
                TOKEN,
                COUNTER_ID,
                startDate,
                endDate,
                Goal.builder().build()
        );

        assertThat(metrikaResponseDatas).hasSize(6);

        assertThat(metrikaResponseDatas.get(0).getName()).matches("Переходы по рекламе");
        assertThat(metrikaResponseDatas.get(0).getMetrics()).hasSize(2);
        assertThat(metrikaResponseDatas.get(0).getMetrics().get(0)).hasSize(32);
    }
}