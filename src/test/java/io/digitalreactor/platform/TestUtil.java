package io.digitalreactor.platform;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digitalreactor.analytic.metrika.common.report.BaseReportType;
import io.digitalreactor.analytic.metrika.dao.ReportDao;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.analytic.metrika.service.summary.SummaryService;
import io.digitalreactor.platform.dao.CounterDao;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.project.ProjectService;
import io.digitalreactor.platform.service.registration.RegistrationService;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.platform.service.user.UserService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;

@Service
@Profile(value = SPRING_PROFILE_TEST)
public class TestUtil {
    private static final AtomicInteger userCounter = new AtomicInteger(1);

    final private List<User> users = new ArrayList<>();
    final private List<Integer> tokenIds = new ArrayList<>();
    final private List<Project> projects = new ArrayList<>();
    final private List<Summary> summaries = new ArrayList<>();

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private UserService userService;

    @Autowired
    private CounterDao counterDao;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private SummaryService summaryService;

    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private RegistrationService registrationService;

    private static <T> Optional<T> findByFilter(final List<T> list, final Predicate<T> predicate) {
        return list.stream().filter(predicate).findAny();
    }

    public User makeUser() {
        final User newUser = userService.register(generateEmail(), "password");
        users.add(newUser);

        return newUser;
    }

    public int makeAccessToken(final long userId, @NonNull final String token) {
        final Optional<User> userOptional = findByFilter(users,user -> user.getId() == userId);
        if (userOptional.isPresent()) {
            final int tokenId = accessTokenService.save(userOptional.get().getId(), token);
            tokenIds.add(tokenId);

            return tokenId;
        } else {
            return -1;
        }
    }

    public int makeAccessTokenToFirstUser(@NonNull final String token) {
        if (users.isEmpty()) {
            makeUser();
        }
        return makeAccessToken(users.get(0).getId(), token);
    }

    public void register(@NonNull final String token, final int counterId, @NonNull final String projectName) {
        registrationService.register(generateEmail(),
                token,
                counterId,
                projectName
        );
    }

    public Project addProject(final long userId, @NonNull final String projectName) {
        final Optional<User> userOptional = findByFilter(users,user -> user.getId() == userId);

        if (userOptional.isPresent()) {
            final Project newProject = projectService.addProject(userOptional.get().getId(), projectName);
            projects.add(newProject);

            return newProject;
        } else {
            throw new IllegalArgumentException("Can't find user with that id");
        }
    }

    public Project addProjectToFirstUser(@NonNull final String projectName) {
        if (users.isEmpty()) {
            makeUser();
        }
        return addProject(users.get(0).getId(), projectName);
    }

    public Summary addSummary(final int projectId) {
        final Optional<Project> projectOptional = findByFilter(projects, p -> p.getId() == projectId);

        if (projectOptional.isPresent()) {
            return summaryService.makeSummary(projectId);
        } else {
            throw new IllegalArgumentException("Can't find project with that id");
        }
    }

    public Summary addSummaryToFirstProject() {
        if (projects.isEmpty()) {
            addProjectToFirstUser("projectName");
        }

        final Summary summary = summaryService.makeSummary(projects.get(0).getId());

        summaries.add(summary);
        return summary;
    }

    @Transactional
    public void addCounter(final int projectId, final int tokenId, final int externalId) {
        final Optional<Project> projectOptional = findByFilter(projects, p -> p.getId() == projectId);

        if (projectOptional.isPresent() && tokenIds.contains(tokenId)) {
            counterDao.add(projectId, tokenId, externalId);
        } else {
            if (!projectOptional.isPresent()) {
                throw new IllegalArgumentException("Can't find project by that id");
            } else {
                throw new IllegalArgumentException("TokenId doesn't exist");
            }
        }
    }

    @Transactional
    public void addCounterToFirstProject(final int tokenId, final int externalId) {
        if (projects.isEmpty()) {
            addProjectToFirstUser("projectName");
        }
        counterDao.add(projects.get(0).getId(), tokenId, externalId);
    }

    @Transactional
    public Report addReportToFirstSummary(final BaseReportType baseReportType) throws JsonProcessingException {
        if (summaries.isEmpty()) {
            addSummaryToFirstProject();
        }
        return reportDao.add(summaries.get(0).getId(),baseReportType);
    }

    public String generateEmail() {
        return String.format("email%s@email.ru", userCounter.getAndIncrement());
    }

    public User getFirstUser() {
        return users.get(0);
    }

    public void clear() {
        tokenIds.clear();
        projects.clear();
        summaries.clear();
    }

    public Project getFirstProject() {
        return projects.get(0);
    }

    public static DataSource currentTransactionDataSource(@NonNull final DataSource dataSource) {
        final Connection connection = DataSourceUtils.getConnection(dataSource);
        return new SingleConnectionDataSource(connection, true);
    }
}
