package io.digitalreactor.platform.dao;

import io.digitalreactor.analytic.metrika.dao.SummaryDao;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.COMPLETED;
import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.LOADING;
import static io.digitalreactor.analytic.metrika.model.summary.SummaryStatus.NEW;
import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class SummaryDaoIT {
    @Autowired
    private SummaryDao summaryDao;

    @Autowired
    private TestUtil testUtil;

    @After
    public void tearDown() {
        testUtil.clear();
    }

    @Test
    @Transactional
    public void findLastSummary() {
        final Summary lastAddedSummary = testUtil.addSummaryToFirstProject();
        final User user = testUtil.getFirstUser();

        final Optional<Summary> lastSummaryOpt = summaryDao.findLastSummaryByProjectId(
                user.getId(),
                lastAddedSummary.getProjectId()
        );

        assertThat(lastSummaryOpt).hasValueSatisfying(
                summary -> {
                    assertThat(summary.getId())
                            .isGreaterThan(0)
                            .isEqualTo(lastAddedSummary.getId());
                    assertThat(summary.getProjectId()).isEqualTo(summary.getProjectId());
                    assertThat(summary.getStatus()).isEqualTo(NEW);
                });
    }

    @Test
    @Transactional
    public void findLastSummaryNotExists() {
        final Project project = testUtil.addProjectToFirstUser(anyString());

        final Optional<Summary> lastSummaryOpt = summaryDao.findLastSummaryByProjectId(
                project.getUserId(),
                project.getId()
        );

        assertThat(lastSummaryOpt).isEmpty();
    }

    @Test
    @Transactional
    public void addSummary() {
        final String projectName = anyString();
        final Project project = testUtil.addProjectToFirstUser(projectName);
        final LocalDateTime localDateTime = now();

        final Summary expectedSummary = Summary
                .builder()
                .loadDate(localDateTime)
                .projectId(project.getId())
                .status(NEW)
                .build();

        final Summary realSummary = summaryDao.add(project.getId(), localDateTime);

        assertThat(realSummary)
                .matches(summary ->
                        summary.getId() > 0 &&
                                summary.getProjectId() == expectedSummary.getProjectId() &&
                                summary.getLoadDate().equals(expectedSummary.getLoadDate()) &&
                                summary.getStatus() == expectedSummary.getStatus()

                );

        assertThat(summaryDao.findSummaryById(realSummary.getId()))
                .contains(realSummary);
    }

    @Test
    @Transactional
    public void findAllWithStatus() {
        final Summary summary1 = testUtil.addSummaryToFirstProject(),
                summary2 = testUtil.addSummaryToFirstProject();

        summaryDao.changeStatus(summary2.getId(), LOADING);

        assertThat(summaryDao.findAllWithStatus(NEW))
                .hasSize(1)
                .containsOnlyOnce(summary1);
    }

    @Test
    @Transactional
    public void findAllWithStatusEmptyList() {
        testUtil.addSummaryToFirstProject();
        testUtil.addSummaryToFirstProject();

        assertThat(summaryDao.findAllWithStatus(LOADING))
                .hasSize(0);
    }

    @Transactional
    @Test
    public void changeStatus() {
        final Summary summary = testUtil.addSummaryToFirstProject();
        summary.setStatus(LOADING);

        assertThat(summaryDao.changeStatus(summary.getId(), LOADING))
                .isTrue();
        assertThat(summaryDao.findSummaryById(summary.getId()))
                .contains(summary);
    }

    @Transactional
    @Test
    public void changeStatusNotExists() {
        assertThat(summaryDao.changeStatus(anyLong(), LOADING))
                .isFalse();
    }

    @Test
    @Transactional
    public void findSummaryById() {
        final Summary summary = testUtil.addSummaryToFirstProject();

        assertThat(summaryDao.findSummaryById(summary.getId()))
                .contains(summary);
    }

    @Test
    @Transactional
    public void findSummaryByIdNotExists() {
        assertThat(summaryDao.findSummaryById(anyLong()))
                .isEmpty();
    }

    @Test
    @Transactional
    public void findAllByUserAndProjectId() {
        final Summary summary1 = testUtil.addSummaryToFirstProject(),
                summary2 = testUtil.addSummaryToFirstProject();

        final User user = testUtil.getFirstUser();

        assertThat(summaryDao.findAllByUserAndProjectId(user.getId(), summary1.getProjectId()))
                .hasSize(2)
                .containsOnlyOnce(summary1, summary2);
    }

    @Test
    @Transactional
    public void findAllByUsersAndProjectIdNotExists() {
        final Project project = testUtil.addProjectToFirstUser(anyString());

        assertThat(summaryDao.findAllByUserAndProjectId(project.getUserId(), project.getId()))
                .hasSize(0);
    }

    @Test
    @Transactional
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
            "INSERT INTO users (id, email, password, reg_date) VALUES (321, 'test@dr.io', 'pt', now())",
            "INSERT INTO projects (id, user_id, name) VALUES (43, 321, 'project_name_21')",
            "INSERT INTO summaries (id, project_id, status, load_date) VALUES (54, 43, 'COMPLETED'::summary_status, now())"
    })
    public void findExtendSummaryById() {
        final long summaryId = 54;
        final long userId = 321;

        assertThat(summaryDao.findExtendSummaryById(userId, summaryId)).hasValueSatisfying(summaryExtend -> {
            assertThat(summaryExtend.getId()).isEqualTo(summaryId);
            assertThat(summaryExtend.getProjectId()).isEqualTo(43);
            assertThat(summaryExtend.getProjectName()).isEqualTo("project_name_21");
            assertThat(summaryExtend.getStatus()).isEqualTo(COMPLETED);
            assertThat(summaryExtend.getLoadDate()).isEqualToIgnoringHours(now());
        });
    }

    private String anyString() {
        return "string";
    }

    private long anyLong() {
        return 0;
    }
}