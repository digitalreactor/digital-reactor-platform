package io.digitalreactor.platform.dao;

import org.assertj.db.api.Assertions;
import org.assertj.db.type.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static io.digitalreactor.platform.TestUtil.currentTransactionDataSource;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class FeedbackDaoIT {
    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private DataSource dataSource;

    @Test
    @Transactional
    public void save() throws Exception {
        final String userName = "Bob";
        final String message = "good service ' $ ? >< ";

        feedbackDao.save(userName, message);

        Assertions.assertThat(new Request(currentTransactionDataSource(dataSource), "SELECT * FROM feedbacks"))
                .hasNumberOfRows(1)
                .column("user_name").value().isEqualTo(userName)
                .column("message").value().isEqualTo(message);
    }
}