package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static io.digitalreactor.platform.model.metrika.MetrikaTokenStatus.ACTIVE;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class TokenDaoIT {
    @Autowired
    private UserDao userDao;

    @Autowired
    private TokenDao tokenDao;

    @Test
    @Transactional
    public void addToken() {
        final String token = "myTokenForSave";
        final User user = userDao.register("email", "password");
        final MetrikaToken metrikaToken = tokenDao.add(user.getId(), token, ACTIVE);

        assertThat(tokenDao.findById(metrikaToken.getId()))
                .isPresent()
                .hasValueSatisfying(dbToken -> {
                    assertThat(dbToken.getUserId()).isGreaterThan(0);
                    assertThat(dbToken.getUserId()).isEqualTo(user.getId());
                    assertThat(dbToken.getToken()).isEqualTo(token);
                    assertThat(dbToken.getStatus()).isEqualTo(ACTIVE);
                });
    }
}