package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static io.digitalreactor.platform.model.metrika.MetrikaTokenStatus.ACTIVE;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class CounterDaoIT {
    @Autowired
    private UserDao userDao;

    @Autowired
    private TokenDao tokenDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private CounterDao counterDao;

    @Test
    @Transactional
    public void addCounter() {
        final long externalId = 23432;
        final User user = userDao.register("email", "password");
        final Project project = projectDao.add(user.getId(), "projectName");
        final MetrikaToken token = tokenDao.add(user.getId(), "myTokenForSave", ACTIVE);
        counterDao.add(project.getId(), token.getId(), externalId);

        assertThat(counterDao.findByProjectId(project.getId()))
                .isPresent()
                .hasValueSatisfying(counter -> {
                    assertThat(counter.getExternalId()).isEqualTo(externalId);
                    assertThat(counter.getProjectId()).isEqualTo(project.getId());
                    assertThat(counter.getTokenId()).isEqualTo(token.getId());
                });
    }
}