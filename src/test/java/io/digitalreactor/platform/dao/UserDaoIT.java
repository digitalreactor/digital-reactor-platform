package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class UserDaoIT {
    @Autowired
    private UserDao userDao;

    @Autowired
    private TestUtil testUtil;

    @Test
    @Transactional
    public void registerUser() {
        final String email = "testEmail@email.ru";
        final String password = "testPassword";
        final User user = userDao.register(email, password);

        assertThat(userDao.findByEmail(user.getEmail())).isPresent().hasValueSatisfying(u -> {
            assertThat(u.getEmail()).isEqualTo(email);
            assertThat(u.getPassword()).isEqualTo(password);
            assertThat(u.getId()).isGreaterThan(0);
        });
    }

    @Test
    @Transactional
    public void resetPasswordForExitingUser() {
        final User user = testUtil.makeUser();
        final long userId = user.getId();
        final String userEmail = user.getEmail();
        final String newPassword = "newPassword";

        assertThat(userDao.resetPassword(userEmail,newPassword))
                .isTrue();

        assertThat(userDao.findByEmail(user.getEmail()))
                .isPresent().hasValueSatisfying(u -> {
                    assertThat(u.getEmail()).isEqualTo(userEmail);
                    assertThat(u.getPassword()).isEqualTo(newPassword);
                    assertThat(u.getId()).isEqualTo(userId);
                });
    }

    @Test
    @Transactional
    public void resetPasswordForNotExitingUser() {
        final String email = "notExitingEmail";
        final String password = "some";

        assertThat(userDao.resetPassword(email,password))
                .isFalse();
    }
}