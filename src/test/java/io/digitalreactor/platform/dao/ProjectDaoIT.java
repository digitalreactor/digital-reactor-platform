package io.digitalreactor.platform.dao;

import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ProjectDaoIT {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private TestUtil testUtil;

    @Test
    @Transactional
    public void addProject() {
        final User user = testUtil.makeUser();
        final String projectName = "myProjectName";

        projectDao.add(user.getId(), projectName);

        assertThat(projectDao.findAllByUserId(user.getId())).hasSize(1);
        assertThat(projectDao.findAllByUserId(user.getId()).get(0).getName()).isEqualTo(projectName);
    }

    @Test
    @Transactional
    public void revisionIncrement() {
        final User user = testUtil.makeUser();
        final String projectName = "myProjectName";
        final Project project = projectDao.add(user.getId(), projectName);

        assertThat(projectDao.findById(project.getId())).isPresent().hasValueSatisfying(p -> {
            assertThat(p.getId()).isGreaterThan(0);
        });

        assertThat(projectDao.findById(project.getId())).isPresent().hasValueSatisfying(p -> {
            assertThat(p.getId()).isGreaterThan(0);
        });
    }
}