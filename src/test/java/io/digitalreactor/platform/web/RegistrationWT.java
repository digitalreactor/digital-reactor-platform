package io.digitalreactor.platform.web;

import com.codeborne.selenide.Condition;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.service.counter.CounterService;
import io.digitalreactor.platform.service.project.ProjectService;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.platform.service.user.UserService;
import io.digitalreactor.vendor.yandex.oauth.TokenResolveService;
import io.digitalreactor.vendor.yandex.oauth.emulator.AuthorizeEmulator;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static io.digitalreactor.platform.Constants.SPRING_PROFILE_DEV;
import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = DEFINED_PORT)
@ActiveProfiles(SPRING_PROFILE_DEV)
public class RegistrationWT {
    private static final String DEV_TOKEN = "AQAAAAAUQvf-AAQfH-ET9xpCKUwIr4MDS8a21lY";

    @MockBean
    private TokenResolveService tokenResolveService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CounterService counterService;

    @Autowired
    private AccessTokenService accessTokenService;

    @Test
    @Ignore
    public void fullRegistration() throws MalformedURLException, InterruptedException {
        when(tokenResolveService.getTokenByGrantCode(AuthorizeEmulator.CODE)).thenReturn(DEV_TOKEN);
        final String email = "tester@digitalreactor.io";

        open("/registration.html");
        $("#email").val(email);
        $("#go-to-information-step").click();

        $("#go-to-site-step").shouldBe(Condition.exist);
        assertThat(new URL(url())).hasAnchor("access");
        $("#go-to-site-step").click();

        $("#save").shouldBe(Condition.exist);
        $("#project-list").shouldBe(Condition.exist);

        final long counterId = Integer.valueOf($("#project-list").getSelectedValue());
        final String projectName = $("#project-list").getSelectedText();

        $("#save").click();

        $("#go-to-login").shouldBe(Condition.exist);
        $("#go-to-login").click();
        assertThat(new URL(url())).hasPath("/login.html");


        assertThat(userService.findByEmail(email))
                .isPresent()
                .hasValueSatisfying(u -> {
                    assertThat(u.getEmail()).isEqualTo(email);
                    assertThat(u.getId()).isGreaterThan(0);

                    final List<Project> projects = projectService.findAllByUserId(u.getId());
                    assertThat(projects).hasSize(1);
                    assertThat(projects.get(0).getId()).isGreaterThan(0);
                    assertThat(projects.get(0).getName()).isEqualTo(projectName);

                    final List<MetrikaToken> userTokens = accessTokenService.findByUserId(u.getId());
                    assertThat(userTokens).hasSize(1);
                    assertThat(userTokens.get(0).getToken()).isEqualTo(DEV_TOKEN);

                    assertThat(counterService.findByProjectId(projects.get(0).getId()))
                            .isPresent()
                            .hasValueSatisfying(counter -> {
                                assertThat(counter.getExternalId()).isEqualTo(counterId);
                                assertThat(counter.getTokenId()).isEqualTo(userTokens.get(0).getId());
                            });
                });
    }
}
