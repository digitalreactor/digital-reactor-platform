package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.object.ReportGoal;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.hamcrest.Matchers.is;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * digital-reactor
 * Created by igor on 30.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class SummaryRestControllerTest {
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestUtil testUtil;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();

        testUtil.clear();
    }

    @Test
    @Transactional
    public void getSummary() throws Exception {
        final PagesWithHighFailureRateReport pagesWithHighFailureRateReportWithNoNullGoal = ReportCreatorUtil.pagesWithHighFailureRateReportWithNoNullGoal();
        final Report report = testUtil.addReportToFirstSummary(pagesWithHighFailureRateReportWithNoNullGoal);

        final ReportGoal reportGoal = pagesWithHighFailureRateReportWithNoNullGoal.getReportGoal();

        this.mockMvc.perform(
                get("/api/summaries/" + report.getSummaryId()).accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.goals[0].name", is(reportGoal.getGoalName())))
                .andExpect(jsonPath("$.goals[0].id", is((int)reportGoal.getGoalId())));
    }
}