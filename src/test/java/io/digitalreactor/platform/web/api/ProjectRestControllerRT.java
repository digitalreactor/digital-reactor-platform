package io.digitalreactor.platform.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.digitalreactor.analytic.metrika.model.summary.Summary;
import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.counter.CounterService;
import io.digitalreactor.platform.web.model.project.NewProjectUI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ProjectRestControllerRT {
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestUtil testUtil;

    @MockBean
    private CounterService counterService;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();

        testUtil.clear();
    }

    @Test
    @Transactional
    public void projectLists() throws Exception {
        testUtil.addProjectToFirstUser("project1");
        testUtil.addProjectToFirstUser("project2");

        this.mockMvc.perform(get("/api/projects").accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", greaterThan(0)))
                .andExpect(jsonPath("$[0].name", is("project1")))
                .andExpect(jsonPath("$[1].id", greaterThan(0)))
                .andExpect(jsonPath("$[1].name", is("project2")));
    }

    @Test
    @Transactional
    public void addProjectCantBeAdded() throws Exception {
        final NewProjectUI newProject = NewProjectUI
                .builder()
                .name("project")
                .externalCounterId(1)
                .tokenId(2)
                .build();
        final String json = objectMapper.writeValueAsString(newProject);

        this.mockMvc.perform(post("/api/projects")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void addProject() throws Exception {
        when(counterService.findAllUnusedByUserId(anyLong()))
                .thenReturn(
                        Collections.singletonList(
                                CounterWithToken
                                        .builder()
                                        .name("counter")
                                        .tokenId(2)
                                        .externalId(1)
                                        .tokenName("token")
                                        .build()
                        )
                );

        final String newProjectName = "project";

        final NewProjectUI newProject = NewProjectUI
                .builder()
                .name("project")
                .externalCounterId(1)
                .tokenId(2)
                .build();

        final String json = objectMapper.writeValueAsString(newProject);

        this.mockMvc.perform(post("/api/projects")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.name", is(newProjectName)));
    }

    @Test
    @Transactional
    public void updateProject() throws Exception {
        final Project project = testUtil.addProjectToFirstUser("project");

        this.mockMvc.perform(post("/api/projects/" + project.getId() + "/summaries").accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.status", is("NEW")));
    }

    @Test
    @Transactional
    public void lastSummaryInfo() throws Exception {
        final Summary summary = testUtil.addSummaryToFirstProject();
        final Project project = testUtil.getFirstProject();

        mockMvc.perform(get("/api/projects/" + project.getId() + "/summaries")
                .accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is((int) summary.getId())))
                .andExpect(jsonPath("$[0].status", is(summary.getStatus().name())))
                .andExpect(jsonPath("$[0].date", is(summary.getLoadDate().format(DateTimeFormatter.ISO_LOCAL_DATE))));
    }
}