package io.digitalreactor.platform.web.api.mappers.sources;

import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.object.GoalReferringSource;
import io.digitalreactor.analytic.metrika.common.report.object.ReferringSource;
import io.digitalreactor.analytic.metrika.common.report.object.Visit;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.web.model.report.VisitUI;
import io.digitalreactor.platform.web.model.report.sources.GoalReferringSourceUI;
import io.digitalreactor.platform.web.model.report.sources.ReferringSourceReportUI;
import io.digitalreactor.platform.web.model.report.sources.ReferringSourceUI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 22.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReferringSourceMapperTest {
    @Autowired
    ReferringSourceMapper referringSourceMapper;

    @Test
    public void mapVisit() {
        final Visit visit = ReportCreatorUtil.visit();

        final VisitUI visitUI = referringSourceMapper.mapVisit(visit);

        assertThat(visitUI)
                .hasFieldOrPropertyWithValue("number",visit.getNumber())
                .hasFieldOrPropertyWithValue("date", visit.getDate())
                .hasFieldOrPropertyWithValue("dayType",visit.getDayType().name());
    }

    @Test
    public void mapReferringSource() {
        final ReferringSource referringSource = ReportCreatorUtil.referringSource();

        final ReferringSourceUI referringSourceUI = referringSourceMapper.mapReferringSource(referringSource);

        assertThat(referringSourceUI)
                .hasFieldOrPropertyWithValue("name",referringSource.getName())
                .hasFieldOrPropertyWithValue("totalVisits", referringSource.getTotalVisits())
                .hasFieldOrPropertyWithValue("totalVisitsChangePercent", referringSource.getTotalVisitsChangePercent())
                .hasFieldOrPropertyWithValue("totalGoalVisits", referringSource.getGoalReferringSourceStatistic().getTotalGoalVisits())
                .hasFieldOrPropertyWithValue("totalGoalVisitsChangePercent",referringSource.getGoalReferringSourceStatistic().getTotalGoalVisitsChangePercent())
                .hasFieldOrPropertyWithValue("conversion",referringSource.getGoalReferringSourceStatistic().getConversion())
                .hasFieldOrPropertyWithValue("conversionChangePercent",referringSource.getGoalReferringSourceStatistic().getConversionChangePercent())
                .matches(
                        source -> source.getMetrics().size() == referringSource.getMetrics().size()
                );
    }

    @Test
    public void mapGoalReferringSource() {
        final GoalReferringSource goalReferringSource = ReportCreatorUtil.goalReferringSource();

        final GoalReferringSourceUI goalReferringSourceUI = referringSourceMapper.mapGoalReferringSource(goalReferringSource);

        assertThat(goalReferringSourceUI)
                .hasFieldOrPropertyWithValue("name",goalReferringSource.getName())
                .hasFieldOrPropertyWithValue("conversion", goalReferringSource.getConversion())
                .hasFieldOrPropertyWithValue("conversionChange",goalReferringSource.getConversionChange())
                .hasFieldOrPropertyWithValue("numberOfCompletedGoal",goalReferringSource.getNumberOfCompletedGoal())
                .matches(
                        goalSource ->
                                goalSource.getSources().size() == goalReferringSource.getSources().size()
                );
    }

    @Test
    public void mapReport() {
        final ReferringSourceReport referringSourceReport = ReportCreatorUtil.referringSourceReport();

        final ReferringSourceReportUI referringSourceReportUI = referringSourceMapper.mapReport(referringSourceReport);

        assertThat(referringSourceReportUI)
                .hasFieldOrPropertyWithValue("startDate", referringSourceReport.getSampling().getStartDate())
                .hasFieldOrPropertyWithValue("endDate", referringSourceReport.getSampling().getEndDate())
                .hasFieldOrPropertyWithValue("samplingInterval", referringSourceReport.getSampling().getSignature())
                .matches(
                        report -> report.getSources().size() == referringSourceReport.getSources().size()
                );
    }
}