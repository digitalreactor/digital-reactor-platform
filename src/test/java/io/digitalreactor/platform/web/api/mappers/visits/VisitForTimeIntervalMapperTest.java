package io.digitalreactor.platform.web.api.mappers.visits;

import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.web.model.report.visits.VisitsForTimeIntervalReportUI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 22.04.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class VisitForTimeIntervalMapperTest {
    @Autowired
    private VisitForTimeIntervalMapper visitForTimeIntervalMapper;

    @Test
    public void mapReport() {
        final VisitsForTimeIntervalReport visitsForTimeIntervalReport = ReportCreatorUtil.visitsForTimeIntervalReport();

        final VisitsForTimeIntervalReportUI referringSourceReportUI = visitForTimeIntervalMapper.mapReport(visitsForTimeIntervalReport);

        assertThat(referringSourceReportUI)
                .hasFieldOrPropertyWithValue("startDate", visitsForTimeIntervalReport.getSampling().getStartDate())
                .hasFieldOrPropertyWithValue("endDate", visitsForTimeIntervalReport.getSampling().getEndDate())
                .hasFieldOrPropertyWithValue("samplingInterval", visitsForTimeIntervalReport.getSampling().getSignature())
                .hasFieldOrPropertyWithValue("visitChangePercent", visitsForTimeIntervalReport.getVisitChangePercent())
                .hasFieldOrPropertyWithValue("reason", visitsForTimeIntervalReport.getReason())
                .hasFieldOrPropertyWithValue("trend", visitsForTimeIntervalReport.getTrend().name())
                .hasFieldOrPropertyWithValue("visitChange", visitsForTimeIntervalReport.getVisitChange())
                .matches(
                        report -> report.getMetrics().size() == visitsForTimeIntervalReport.getMetrics().size()
                );
    }
}