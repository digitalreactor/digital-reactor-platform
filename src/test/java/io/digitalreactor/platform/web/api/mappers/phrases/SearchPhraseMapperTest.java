package io.digitalreactor.platform.web.api.mappers.phrases;

import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhrase;
import io.digitalreactor.analytic.metrika.common.report.object.SearchPhraseCut;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseCutUI;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseReportUI;
import io.digitalreactor.platform.web.model.report.phrases.SearchPhraseUI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * digital-reactor
 * Created by igor on 21.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class SearchPhraseMapperTest {
    @Autowired
    SearchPhraseMapper searchPhraseMapper;

    @Test
    public void mapSearchPhrase() {
        final SearchPhrase searchPhrase = ReportCreatorUtil.searchPhrase();

        final SearchPhraseUI searchPhraseUI = searchPhraseMapper.mapPhrase(searchPhrase);

        assertThat(searchPhraseUI)
                .hasFieldOrPropertyWithValue("searchPhrase", searchPhrase.getSearchPhrase())
                .hasFieldOrPropertyWithValue("visits", searchPhrase.getVisits())
                .hasFieldOrPropertyWithValue("bounceRate", searchPhrase.getBounceRate())
                .hasFieldOrPropertyWithValue("pageDepth", searchPhrase.getPageDepth())
                .hasFieldOrPropertyWithValue("avgVisitDurationSeconds", searchPhrase.getAvgVisitDurationSeconds())
                .hasFieldOrPropertyWithValue("quality", searchPhrase.getQuality());
    }

    @Test
    public void mapSearchPhraseCut() {
        final SearchPhraseCut searchPhraseCut = ReportCreatorUtil.searchPhraseCut();

        final SearchPhraseCutUI searchPhraseCutUI = searchPhraseMapper.mapPhraseCut(searchPhraseCut);

        assertThat(searchPhraseCutUI)
                .hasFieldOrPropertyWithValue("goalName", searchPhraseCut.getGoalName())
                .hasFieldOrPropertyWithValue("type", searchPhraseCut.getType().name())
                .matches(searchPhrase ->
                        searchPhrase.getBadPhrases().size() == searchPhraseCut.getBadPhrases().size() &&
                                searchPhrase.getSuccessPhrases().size() == searchPhraseCut.getSuccessPhrases().size()
                );
    }

    @Test
    public void mapReport() {
        final SearchPhraseReport searchPhraseReport = ReportCreatorUtil.searchPhraseReport();

        final SearchPhraseReportUI searchPhraseReportUI = searchPhraseMapper.mapReport(searchPhraseReport);

        assertThat(searchPhraseReportUI)
                .hasFieldOrPropertyWithValue("startDate", searchPhraseReport.getSampling().getStartDate())
                .hasFieldOrPropertyWithValue("endDate", searchPhraseReport.getSampling().getEndDate())
                .hasFieldOrPropertyWithValue("samplingInterval", searchPhraseReport.getSampling().getSignature())
                .matches(
                        report-> report.getBadPhrases().size() == searchPhraseReport.getBadPhrases().size() &&
                                report.getSuccessPhrases().size() == searchPhraseReport.getSuccessPhrases().size()
                );
    }
}