package io.digitalreactor.platform.web.api;

import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.vendor.yandex.metrika.model.Counter;
import io.digitalreactor.vendor.yandex.metrika.serivce.CounterApiService;
import org.flywaydb.core.Flyway;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * digital-reactor
 * Created by igor on 21.03.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class CounterRestControllerRT {
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestUtil testUtil;

    @Autowired
    Flyway flyway;

    @Autowired
    FlywayMigrationStrategy flywayMigrationStrategy;

    @MockBean
    private CounterApiService counterApiService;

    private final List<Counter> allUserCounters = new ArrayList<>();

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void noUnused() throws Exception {
        noUnusedInitialization();

        this.mockMvc.perform(get("/api/counters/unused?tokenId=1")
                .accept(contentType))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    private void noUnusedInitialization() {
        testUtil.register("token1", 1, "project1");
        testUtil.makeAccessToken(1,"token1");

        allUserCounters.add(Counter.builder().name("project1").id(1L).build());

        when(counterApiService.getCounters(anyString())).thenReturn(allUserCounters);
    }

    @Test
    public void findAllUnusedByUserIdTest() throws Exception {
        initializeListsAndMocksFindAllUnusedByUserIdTest();

        this.mockMvc.perform(get("/api/counters/unused")
                .accept(contentType))
                .andExpect(status().isOk())
                //.andExpect(jsonPath("$[0].tokenName", is("token2")))
                .andExpect(jsonPath("$[0].tokenId", is(2)))
                .andExpect(jsonPath("$[0].externalId", is(3)))
                .andExpect(jsonPath("$[0].name", is("counter3")));

    }

    private void initializeListsAndMocksFindAllUnusedByUserIdTest() {
        allUserCounters.add(Counter.builder().id(1L).name("counter1").build());
        allUserCounters.add(Counter.builder().id(2L).name("counter2").build());
        allUserCounters.add(Counter.builder().id(3L).name("counter3").build());
        when(counterApiService.getCounters(anyString())).thenReturn(allUserCounters);

        testUtil.addProjectToFirstUser("project1");
        testUtil.addProjectToFirstUser("project2");

        testUtil.makeAccessTokenToFirstUser("token1");
        testUtil.makeAccessTokenToFirstUser("token2");

        testUtil.addCounter(1,1,1);
        testUtil.addCounter(2,2,2);

        when(counterApiService.getCounters("token1")).thenReturn(allUserCounters.subList(0,2));
        when(counterApiService.getCounters("token2")).thenReturn(allUserCounters.subList(2,3));
    }

    @Test
    public void findAllUnusedByUserIdAndTokenIdTest() throws Exception {
        flywayMigrationStrategy.migrate(flyway);
        initializeListsAndMocksFindAllUnusedByUserIdAndUserIdTest();

        this.mockMvc.perform(get("/api/counters/unused?tokenId=1")
                .accept(contentType))
                .andExpect(status().isOk())
               // .andExpect(jsonPath("$[0].tokenName", is("token1")))
                .andExpect(jsonPath("$[0].tokenId", is(1)))
                .andExpect(jsonPath("$[0].externalId", is(1234)))
                .andExpect(jsonPath("$[0].name", is("counter4")));

    }

    private void initializeListsAndMocksFindAllUnusedByUserIdAndUserIdTest() {
        testUtil.addProject(1,"project1");
        testUtil.addProject(1,"project2");
        testUtil.addProject(1,"project3");

        testUtil.makeAccessToken(1,"token1");
        testUtil.makeAccessToken(1,"token2");

        testUtil.addCounter(1,1,1);
        testUtil.addCounter(1,1,12);
        testUtil.addCounter(3,2,123);

        allUserCounters.add(Counter.builder().id(1L).name("counter1").build());
        allUserCounters.add(Counter.builder().id(12L).name("counter2").build());
        allUserCounters.add(Counter.builder().id(123L).name("counter3").build());
        allUserCounters.add(Counter.builder().id(1234L).name("counter4").build());

        when(counterApiService.getCounters("token1")).thenReturn(allUserCounters);
    }
}
