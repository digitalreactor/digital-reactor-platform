package io.digitalreactor.platform.web.api.mappers.pages;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.object.Page;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.web.model.report.pages.PageUI;
import io.digitalreactor.platform.web.model.report.pages.PagesWithHighFailureRateReportUI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * digital-reactor
 * Created by igor on 21.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class PageUIMapperTest {
    @Autowired
    private PageUIMapper pageUIMapper;

    @Test
    public void mapPage() {
        final Page page = ReportCreatorUtil.page();

        final PageUI expectedPageUI = PageUI
                .builder()
                .siteName(page.getSiteName())
                .favicon(page.getFavicon())
                .pageViews(page.getPageViews())
                .bounceRate(page.getBounceRate())
                .avgVisitDurationSeconds(page.getAvgVisitDurationSeconds())
                .visits(page.getVisits())
                .conversion(page.getConversion())
                .build();

        final PageUI pageUI = pageUIMapper.mapPage(page);

        assertThat(pageUI)
                .isEqualToComparingFieldByField(expectedPageUI);
    }

    @Test
    public void mapReport() {
        final PagesWithHighFailureRateReport pagesWithHighFailureRateReport = ReportCreatorUtil.pagesWithHighFailureRateReport();

        final PagesWithHighFailureRateReportUI pagesWithHighFailureRateReportUI = pageUIMapper.mapReport(pagesWithHighFailureRateReport);

        assertThat(pagesWithHighFailureRateReportUI)
                .hasFieldOrPropertyWithValue("startDate", pagesWithHighFailureRateReport.getSampling().getStartDate())
                .hasFieldOrPropertyWithValue("endDate", pagesWithHighFailureRateReport.getSampling().getEndDate())
                .hasFieldOrPropertyWithValue("samplingInterval", pagesWithHighFailureRateReport.getSampling().getSignature())
                .matches(
                        report-> report.getMetrics().size() == pagesWithHighFailureRateReport.getMetrics().size()
                );
    }
}