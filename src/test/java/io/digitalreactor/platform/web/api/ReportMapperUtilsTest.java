package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.common.report.ReferringSourceReport;
import io.digitalreactor.analytic.metrika.common.report.SearchPhraseReport;
import io.digitalreactor.analytic.metrika.common.report.VisitsForTimeIntervalReport;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * digital-reactor
 * Created by igor on 17.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportMapperUtilsTest {
    final private PagesWithHighFailureRateReport pagesWithHighFailureRateReport = ReportCreatorUtil.pagesWithHighFailureRateReport();
    final private SearchPhraseReport searchPhraseReport = ReportCreatorUtil.searchPhraseReport();
    final private ReferringSourceReport referringSourceReport = ReportCreatorUtil.referringSourceReport();
    final private VisitsForTimeIntervalReport visitsForTimeIntervalReport = ReportCreatorUtil.visitsForTimeIntervalReport();

    @Autowired
    private ReportMapper reportMapper;

    @Test
    public void mapToPagesWithHighFailureRateReportUI() throws Exception {
        assertThat(reportMapper.mapToUI(pagesWithHighFailureRateReport))
                .isNotNull()
                .matches(p ->
                        p.getEndDate().equals(pagesWithHighFailureRateReport.getSampling().getEndDate()) &&
                                p.getStartDate().equals(pagesWithHighFailureRateReport.getSampling().getStartDate())
                )
                .hasNoNullFieldsOrProperties();
    }

    @Test
    public void mapToSearchPhraseReportUI() throws Exception {
        assertThat(reportMapper.mapToUI(searchPhraseReport))
                .isNotNull()
                .matches(p ->
                        p.getEndDate().equals(searchPhraseReport.getSampling().getEndDate()) &&
                                p.getStartDate().equals(searchPhraseReport.getSampling().getStartDate())
                )
                .hasNoNullFieldsOrProperties();
    }

    @Test
    public void mapToReferringSourceReportUI() throws Exception {
        assertThat(reportMapper.mapToUI(referringSourceReport))
                .isNotNull()
                .matches(p ->
                        p.getEndDate().equals(referringSourceReport.getSampling().getEndDate()) &&
                                p.getStartDate().equals(referringSourceReport.getSampling().getStartDate())
                )
                .hasNoNullFieldsOrProperties();
    }

    @Test
    public void mapToVisitsForTimeIntervalReportUI() throws Exception {
        assertThat(reportMapper.mapToUI(visitsForTimeIntervalReport))
                .isNotNull()
                .matches(p ->
                        p.getEndDate().equals(visitsForTimeIntervalReport.getSampling().getEndDate()) &&
                                p.getStartDate().equals(visitsForTimeIntervalReport.getSampling().getStartDate())
                )
                .hasNoNullFieldsOrProperties();
    }

}