package io.digitalreactor.platform.web.api;

import io.digitalreactor.analytic.metrika.common.report.PagesWithHighFailureRateReport;
import io.digitalreactor.analytic.metrika.model.report.Report;
import io.digitalreactor.analytic.metrika.service.report.ReportCreatorUtil;
import io.digitalreactor.platform.TestUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * digital-reactor
 * Created by igor on 30.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportRestControllerTest {
    private static final String REQUEST_TEMPLATE = "/api/reports/grouping?summary=%s&types=%s&interval=%s";

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestUtil testUtil;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @After
    public void tearDown() {
        testUtil.clear();
    }

    @Test
    @Transactional
    public void getReport() throws Exception {
        final PagesWithHighFailureRateReport pagesWithHighFailureRateReport = ReportCreatorUtil.pagesWithHighFailureRateReport();
        final Report report = testUtil.addReportToFirstSummary(pagesWithHighFailureRateReport);

        this.mockMvc.perform(get("/api/reports/" + report.getId()).accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.samplingInterval", is(pagesWithHighFailureRateReport.getSampling().getSignature())))
                .andExpect(jsonPath("$.startDate", is(pagesWithHighFailureRateReport.getSampling().getStartDate().toString())))
                .andExpect(jsonPath("$.endDate", is(pagesWithHighFailureRateReport.getSampling().getEndDate().toString())));
    }

    @Test
    @Transactional
    public void getReportWithGrouping() throws Exception {
        final PagesWithHighFailureRateReport pagesWithHighFailureRateReport = ReportCreatorUtil.pagesWithHighFailureRateReport();
        final Report report = testUtil.addReportToFirstSummary(pagesWithHighFailureRateReport);

        final String request = String.format(
                REQUEST_TEMPLATE,
                report.getSummaryId(),
                report.getType(),
                pagesWithHighFailureRateReport.getSampling().getInterval()
        );

        this.mockMvc.perform(
                get(request).accept(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].samplingInterval", is(pagesWithHighFailureRateReport.getSampling().getSignature())))
                .andExpect(jsonPath("$[0].startDate", is(pagesWithHighFailureRateReport.getSampling().getStartDate().toString())))
                .andExpect(jsonPath("$[0].endDate", is(pagesWithHighFailureRateReport.getSampling().getEndDate().toString())));
    }
}