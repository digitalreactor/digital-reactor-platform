package io.digitalreactor.platform;

import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DefaultConfiguration {
    private User user;
    private List<Project> projects;
}
