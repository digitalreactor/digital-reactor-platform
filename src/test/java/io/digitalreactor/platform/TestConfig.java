package io.digitalreactor.platform;

import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.service.notification.NotificationService;
import io.digitalreactor.vendor.yandex.oauth.emulator.AuthorizeEmulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_DEV;
import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {
    @Autowired
    private TestUtil testUtil;

    @Bean
    public FlywayMigrationStrategy cleanMigrationStrategy() {
        return flyway -> {
            flyway.clean();
            flyway.migrate();

            final User user = testUtil.makeUser();
            if (user != null) {
                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, user.getAuthorities()));
            }
        };
    }

    @Bean
    @Primary
    public NotificationService notificationService() {
        return mock(NotificationService.class);
    }

    @Bean
    @Primary
    @Profile(SPRING_PROFILE_DEV)
    public TestUtil testUtil() {
        return mock(TestUtil.class);
    }

    @Bean
    public AuthorizeEmulator authorizeEmulator() {
        return new AuthorizeEmulator();
    }
}
