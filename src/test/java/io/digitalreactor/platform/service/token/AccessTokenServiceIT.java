package io.digitalreactor.platform.service.token;

import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.service.user.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class AccessTokenServiceIT {
    @Autowired
    private UserService userService;
    @Autowired
    private AccessTokenService accessTokenService;

    @Test
    public void getDecryptedToken() {
        final String token = "myYandexToken";
        final User user = userService.register("email@email.ru", "password");
        accessTokenService.save(user.getId(), token);

        final List<MetrikaToken> userTokens = accessTokenService.findByUserId(user.getId());
        assertThat(userTokens).hasSize(1);
        assertThat(userTokens.get(0).getToken()).isEqualTo(token);
    }

}