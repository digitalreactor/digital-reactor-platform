package io.digitalreactor.platform.service.counter;

import io.digitalreactor.platform.dao.CounterDao;
import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.metrika.MetrikaCounter;
import io.digitalreactor.platform.model.metrika.MetrikaToken;
import io.digitalreactor.platform.service.token.AccessTokenService;
import io.digitalreactor.vendor.yandex.metrika.model.Counter;
import io.digitalreactor.vendor.yandex.metrika.serivce.CounterApiService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * digital-reactor
 * Created by igor on 23.03.17.
 */

public class CounterServiceTest {
    private CounterService counterService;

    private CounterDao counterDao;
    private CounterApiService counterApiService;
    private AccessTokenService accessTokenService;

    private List<Counter> allUserCounters;
    private List<MetrikaCounter> allUsedCounters;
    private List<MetrikaToken> metrikaTokens;

    private static final int USER_ID = 1;

    @Before
    public void createMocks() {
        counterApiService = mock(CounterApiService.class);
        accessTokenService = mock(AccessTokenService.class);
        counterDao = mock(CounterDao.class);

        counterService = new CounterServiceImpl(counterDao,accessTokenService,counterApiService);

        allUsedCounters = new ArrayList<>();
        allUserCounters = new ArrayList<>();
        metrikaTokens = new ArrayList<>();
    }

    private void initializeListsAndMocksFindEmptyUnusedByUserIdAndTokenIdTest() {
        when(counterDao.findAll(anyInt())).thenReturn(allUsedCounters);
        when(accessTokenService.findByUserId(anyInt())).thenReturn(metrikaTokens);
    }

    @Test
    public void findEmptyUnusedByUserIdAndTokenId() {
        initializeListsAndMocksFindEmptyUnusedByUserIdAndTokenIdTest();

        assertThat(counterService.findAllUnusedByUserIdAndTokenId(USER_ID,1))
                .isEmpty();
    }

    private void initializeListsAndMocksFindAllUnusedByUserIdTest() {
        allUsedCounters.add(MetrikaCounter.builder().id(1).projectId(1).tokenId(1).externalId(1L).build());
        allUsedCounters.add(MetrikaCounter.builder().id(2).projectId(2).tokenId(2).externalId(2L).build());

        allUserCounters.add(Counter.builder().id(1L).name("counter1").build());
        allUserCounters.add(Counter.builder().id(2L).name("counter2").build());
        allUserCounters.add(Counter.builder().id(3L).name("counter3").build());

        metrikaTokens.add(MetrikaToken.builder().id(1).token("token1").build());
        metrikaTokens.add(MetrikaToken.builder().id(2).token("token2").build());

        when(counterApiService.getCounters("token1")).thenReturn(allUserCounters.subList(0,2));
        when(counterApiService.getCounters("token2")).thenReturn(allUserCounters.subList(2,3));

        when(counterDao.findAll(anyInt())).thenReturn(allUsedCounters);
        when(accessTokenService.findByUserId(anyInt())).thenReturn(metrikaTokens);
    }

    @Test
    @Ignore
    public void findAllUnusedByUserId() {
        initializeListsAndMocksFindAllUnusedByUserIdTest();

        final List<CounterWithToken> expect = new ArrayList<>();
        expect.add(CounterWithToken.builder()
                .tokenName("token2")
                .tokenId(2)
                .externalId(3)
                .name("counter3")
                .build());

        assertThat(counterService.findAllUnusedByUserId(USER_ID)).contains(expect.get(0));
    }

    private void initializeListsAndMocksFindAllUnusedByUserIdAndUserIdTest() {
        allUsedCounters.add(MetrikaCounter.builder().id(1).projectId(1).tokenId(1).externalId(1L).build());
        allUsedCounters.add(MetrikaCounter.builder().id(2).projectId(1).tokenId(1).externalId(12L).build());
        allUsedCounters.add(MetrikaCounter.builder().id(5).projectId(3).tokenId(2).externalId(123L).build());

        allUserCounters.add(Counter.builder().id(1L).name("counter1").build());
        allUserCounters.add(Counter.builder().id(12L).name("counter2").build());
        allUserCounters.add(Counter.builder().id(123L).name("counter3").build());
        allUserCounters.add(Counter.builder().id(1234L).name("counter4").build());

        metrikaTokens.add(MetrikaToken.builder().id(1).token("token1").build());

        when(counterApiService.getCounters("token1")).thenReturn(allUserCounters);

        when(counterDao.findAll(anyInt())).thenReturn(allUsedCounters);
        when(accessTokenService.findByUserId(anyInt())).thenReturn(metrikaTokens);
    }

    @Test
    @Ignore
    public void findAllUnusedByUserIdAndTokenId() {
        initializeListsAndMocksFindAllUnusedByUserIdAndUserIdTest();

        final List<CounterWithToken> expect = new ArrayList<>();
        expect.add(CounterWithToken.builder()
                .tokenName("token1")
                .tokenId(1)
                .externalId(1234L)
                .name("counter4")
                .build());

        assertThat(counterService.findAllUnusedByUserIdAndTokenId(USER_ID,1))
            .contains(expect.get(0));
    }
}
