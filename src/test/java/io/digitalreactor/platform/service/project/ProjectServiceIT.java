package io.digitalreactor.platform.service.project;

import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.model.CounterWithToken;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.model.project.Project;
import io.digitalreactor.platform.service.counter.CounterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

/**
 * digital-reactor
 * Created by igor on 10.05.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ProjectServiceIT {
    @Autowired
    ProjectService projectService;

    @Autowired
    TestUtil testUtil;

    @MockBean
    CounterService counterService;

    @Test
    @Transactional
    public void addProject() throws Exception {
        final User user = testUtil.getFirstUser();
        final String newProjectName = "newProject";

        final Project newProject = projectService.addProject(user.getId(), newProjectName);

        assertThat(newProject)
                .hasFieldOrPropertyWithValue("userId", user.getId())
                .hasFieldOrPropertyWithValue("name", newProjectName)
                .matches(project -> project.getId() > 0);

        assertThat(projectService.findAllByUserId(user.getId()))
                .contains(newProject);
    }

    @Test
    @Transactional
    public void findAllByUserId() throws Exception {
        final Project project1 = testUtil.addProjectToFirstUser("project1"),
                project2 = testUtil.addProjectToFirstUser("project2");

        final User user = testUtil.getFirstUser();

        assertThat(projectService.findAllByUserId(user.getId()))
                .containsOnlyOnce(
                        project1, project2
                );
    }

    @Test
    @Transactional
    public void findByProjectIdNotExists() throws Exception {
        assertThat(projectService.findByProjectId(-1))
                .isEmpty();
    }

    @Test
    @Transactional
    public void findByProjectId() throws Exception {
        final Project project = testUtil.addProjectToFirstUser("newProject");

        assertThat(projectService.findByProjectId(project.getId()))
                .contains(project);
    }

    @Test
    @Transactional
    public void counterIdBelongsUser() throws Exception {
        final User user = testUtil.getFirstUser();

        when(counterService.findAllUnusedByUserId(anyLong()))
                .thenReturn(
                        Collections.singletonList(
                                CounterWithToken.builder().tokenName("token1").externalId(1).tokenId(1).name("name1").build()
                        )
                );

        assertThat(projectService.counterIdBelongsUser(user.getId(), 1))
                .isTrue();

        assertThat(projectService.counterIdBelongsUser(user.getId(), 2))
                .isFalse();
    }
}