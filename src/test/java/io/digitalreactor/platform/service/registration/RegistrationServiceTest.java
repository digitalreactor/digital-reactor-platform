package io.digitalreactor.platform.service.registration;

import io.digitalreactor.platform.dao.UserDao;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.service.notification.NotificationService;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * digital-reactor
 * Created by igor on 14.04.17.
 */

public class RegistrationServiceTest {
    private static final String EXISTING_EMAIL = "existingEmail";
    private static final String NOT_EXISTING_EMAIL = "notExistingEmail";

    private RegistrationService registrationService;
    private UserDao userDao;
    private NotificationService notificationService;

    @Before
    public void setUp() {
        userDao = mock(UserDao.class);
        notificationService = mock(NotificationService.class);
        registrationService = new RegistrationServiceImpl(
                userDao,
                null,
                null,
                null,
                notificationService,
                null
        );

        when(userDao.findByEmail(EXISTING_EMAIL)).thenReturn(
                Optional.of(
                        User.builder()
                                .email(EXISTING_EMAIL)
                                .build()
                )
        );

        when(userDao.findByEmail(NOT_EXISTING_EMAIL)).thenReturn(Optional.empty());

        when(userDao.resetPassword(anyString(),anyString())).thenReturn(true);
    }

    @Test
    public void resetPasswordForExistingUser() {
        assertThat(registrationService.resetPassword(EXISTING_EMAIL)).isTrue();
    }

    @Test
    public void resetPasswordForNotExistingUser() {
        assertThat(registrationService.resetPassword(NOT_EXISTING_EMAIL)).isFalse();
    }
}
