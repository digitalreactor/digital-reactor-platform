package io.digitalreactor.platform.service.registration;

import io.digitalreactor.platform.TestUtil;
import io.digitalreactor.platform.dao.UserDao;
import io.digitalreactor.platform.model.User;
import io.digitalreactor.platform.service.notification.NotificationService;
import io.digitalreactor.platform.service.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static io.digitalreactor.platform.Constants.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * digital-reactor
 * Created by igor on 14.04.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class RegistrationServiceTestIT {
    @Autowired
    RegistrationService registrationService;

    @Autowired
    UserService userService;

    @Autowired
    private TestUtil testUtil;

    @Test
    public void resettingPassword() {
        final User userBeforeResettingPass = testUtil.makeUser();

        registrationService.resetPassword(userBeforeResettingPass.getEmail());

        final Optional<User> userAfterResettingPass = userService.findByEmail(userBeforeResettingPass.getEmail());

        assertThat(userAfterResettingPass.get())
                .isEqualToComparingOnlyGivenFields(userBeforeResettingPass,"id","email","registrationDate");
    }
}
