package io.digitalreactor.platform.service.notification;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

import static io.digitalreactor.platform.service.notification.email.EmailNotification.NEW_USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class NotificationServiceTest {

    private NotificationService notificationService;
    private JavaMailSender javaMailSender;
    private MimeMessage mimeMessage;

    @Before
    public void setUp() {
        this.javaMailSender = mock(JavaMailSender.class);
        this.mimeMessage = mock(MimeMessage.class);
        when(this.javaMailSender.createMimeMessage()).thenReturn(this.mimeMessage);
        this.notificationService = new NotificationServiceImpl(this.javaMailSender);
    }

    @Test
    public void sentNewUserEmail() throws MessagingException {
        final String emailRecipient = "tester@digitalreactor.io";
        final String password = "samePassword";
        final Map<String, String> params = new HashMap<>();
        params.put("email", emailRecipient);
        params.put("password", password);

        notificationService.sendEmailNotification(NEW_USER, emailRecipient, params);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(mimeMessage).setContent(captor.capture(), any());
        assertThat(captor.getValue()).contains(emailRecipient, password);
    }
}